module.exports = function(grunt) {

	var globalConfig = {
	  resourceDir: 'resources',
	  publicDir: 'public',
	  appDir: 'app'
	};


    grunt.initConfig({
    	globalConfig: globalConfig,
        watch: {
            sass: {
                files: [
                	'<%= globalConfig.resourceDir  %>/assets/scss/*.{scss,sass}',
                	'<%= globalConfig.resourceDir  %>/assets/scss/*/*.*'
            	],
                tasks: ['sass']
            },
            all: {
                files: [
	                '<%= globalConfig.publicDir  %>/*.html',
	                '<%= globalConfig.publicDir  %>/*.php',
	                '<%= globalConfig.publicDir  %>/js/**/*.{js,json}',
	                '<%= globalConfig.publicDir  %>/**/*.php',
	                '<%= globalConfig.publicDir  %>/**/*.{png,jpg,jpeg,gif,webp,svg}'
                ],
                options: {
                    livereload: true
                }
            },
            jshint: {
                files: '<%= globalConfig.publicDir  %>/js/*.js',
                tasks: 'jshint'
            }
        },
        jshint: {
            files: ['<%= globalConfig.publicDir  %>/js/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        sass: {
            options: {
                sourceMap: true,
                //outputStyle: 'compressed'
            },
            dev: {
                files: {
                    '<%= globalConfig.publicDir  %>/css/style.css': '<%= globalConfig.resourceDir  %>/assets/scss/style.scss'
                }
            },
            dist: {
                files: {
                    '<%= globalConfig.publicDir  %>/css/style.css': '<%= globalConfig.resourceDir  %>/assets/scss/style.scss'
                }
            }
        },
        uglify: {
            js: {
                files: {
                    '<%= globalConfig.publicDir  %>/js/scripts.min.js': '<%= globalConfig.publicDir  %>/js/*.js'
                },
                options: {
                    preserveComments: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-watch');

    grunt.registerTask('default', ['sass:dev', 'watch']);
    grunt.registerTask('dist', ['makepot', 'uglify', 'sass:dist']);


};
