<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class ChecklistItem extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'description' => 'required',
        'checklist_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }
    
    /**
    * Product relationship
    */
    public function checklist()
    {
        return $this->belongsTo('PPDevPortal\Checklist');
    }

}
