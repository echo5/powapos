<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

	/**
    * Users relationship
    */
    public function users()
    {
        return $this->hasMany('PPDevPortal\User');
    }

}
