<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;
use URL;

class DemoApp extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'description' => 'required',
        'link' => 'required',
        'product_id' => 'required',
        'platform_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }
    
    /**
    * Product relationship
    */
    public function product()
    {
        return $this->belongsTo('PPDevPortal\Product');
    }

    /**
    * Product relationship
    */
    public function category()
    {
        return $this->belongsTo('PPDevPortal\ProductCategory');
    }

    /**
    * Platform relationship
    */
    public function platform()
    {
        return $this->belongsTo('PPDevPortal\Platform');
    }

    /**
     * Get link for download
     * 
     * @return str
     */
    public function getLink()
    {
        if ($this->plist)
            return 'itms-services://?action=download-manifest&url=' . urlencode(URL::to(config('assets.demoAppsDirectory') . $this->plist));
        elseif ($this->file)
            return URL::to(config('assets.demoAppsDirectory') . $this->file);
        else
            return $this->link;
    }

}
