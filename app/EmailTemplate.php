<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'view' => 'required',
        'subject' => 'required',
        'content' => 'required',
    );

}
