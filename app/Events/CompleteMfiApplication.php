<?php namespace PPDevPortal\Events;

use PPDevPortal\Events\Event;

use Illuminate\Queue\SerializesModels;

class CompleteMfiApplication extends Event {

	use SerializesModels;

    public $id;
    public $email;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($id)
	{
        $this->id = $id;
	}

}
