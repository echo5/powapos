<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    // @TODO Remove?  Why urlify anchor tag- should be opened through jQuery?
	// public function getAnchorName()
	// {
	// 	return urlify($this->question);
	// }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'question' => 'required',
        'answer' => 'required',
        'product_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('category_id', 'asc')->orderBy('order', 'asc');
    }

    /**
    * Product relationship
    */
    public function category()
    {
        return $this->belongsTo('PPDevPortal\FaqCategory');
    }

}
