<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model {

    /**
     * Table
     *
     * @var str
     */
    protected $table = 'faq_categories';

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    /**
    * Pages relationship
    */
	public function faqs()
	{
		return $this->hasMany('PPDevPortal\Faq', 'category_id');
	}


}
