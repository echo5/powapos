<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'image' => 'required',
        'description' => 'required',
        'product_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }
    
    /**
    * Product relationship
    */
    public function product()
    {
        return $this->belongsTo('PPDevPortal\Product');
    }

}
