<?php namespace PPDevPortal\Handlers\Events;

use PPDevPortal\Events\CompleteMfiApplication;
use PPDevPortal\MfiApplication;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class MarkMfiComplete {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  CompleteMfiApplication  $event
	 * @return void
	 */
	public function handle(CompleteMfiApplication $event)
	{
        $application = MfiApplication::find($event->id);
        $application->complete = true;
        $application->save();
	}

}
