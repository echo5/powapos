<?php namespace PPDevPortal\Handlers\Events;

use PPDevPortal\Events\CompleteMfiApplication;
use PPDevPortal\MfiApplication;
use PPDevPortal\User;
use PPDevPortal\EmailTemplate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendMfiCompleteEmail {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  CompleteMfiApplication  $event
	 * @return void
	 */
	public function handle(CompleteMfiApplication $event)
	{
        $application = MfiApplication::find($event->id);
        $user = User::find($application->user_id);
        $emailTemplate = EmailTemplate::whereView('emails.mficomplete')->first();

        \Mail::send($emailTemplate->view, [
            'user' => $user
        ],
        function($message) use ($user, $emailTemplate)
        {
            $message->to($user->email, $user->getFullName())
                ->subject($emailTemplate->subject);
        });
	}

}
