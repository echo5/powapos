<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\Checklist;
use Route;

class ChecklistsComposer {

    /**
     * ProductCategory instance.
     *
     * @var Faq
     */
    protected $checklists;

    /**
     * Create a new SdkComposer instance.
     */
    public function __construct(Checklist $checklist)
    {
        $this->checklists = $checklist->with([
            'items' => function($query) {
                $query->orderBy('order', 'asc');
            },])
            ->ordered()
            ->get();
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'checklists' => $this->checklists,
        ));
    }

}