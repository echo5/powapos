<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\EmailTemplate;
use PPDevPortal\Services\Shortcodes;
use Route;
use Shortcode;

class EmailTemplatesComposer {

    /**
     * EmailTemplate instance.
     *
     * @var EmailTemplate
     */
    protected $emailTemplate;

    /**
     * Create a new EmailTemplate instance.
     */
	public function __construct(EmailTemplate $emailTemplate, Shortcodes $shortcodes)
    {
        $this->shortcodes = $shortcodes;
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $data = $view->getData();
        $view_name = $view->getName();
        
        if ($view_name != 'emails.base' || $view_name != 'emails.password'){
            $this->shortcodes->setUser($data['user']);
        }

        switch($view_name) {
            case 'emails.password':
                $this->shortcodes->setUser(Auth::user());
                $this->shortcodes->setToken($data['token']);
                break;
            case 'emails.confirm':
                $this->shortcodes->setConfirmationCode($data['confirmation_code']);
        }

        $this->emailTemplate = EmailTemplate::whereView($view->getName())->first();
        $this->emailTemplate['content'] = Shortcode::compile($this->emailTemplate['content']);
        $view->with(array('email' => $this->emailTemplate));
    }

}