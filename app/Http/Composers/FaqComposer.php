<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\FaqCategory;

class FaqComposer {

    /**
     * FAQ instance.
     *
     * @var Faq
     */
	protected $faqs;

    /**
     * Create a new FaqComposer instance.
     */
	public function __construct(FaqCategory $faqCategory)
    {
        $this->faqCategories = $faqCategory->with('faqs')->ordered()->get();
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with('faqCategories', $this->faqCategories);
    }

}