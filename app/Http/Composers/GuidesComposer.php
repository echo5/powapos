<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\Guide;
use Route;

class GuidesComposer {

    /**
     * ProductCategory instance.
     *
     * @var Faq
     */
    protected $guides;

    /**
     * Create a new SdkComposer instance.
     */
    public function __construct(Guide $guide)
    {
        $this->guides = $guide->ordered()->get();
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'guides' => $this->guides,
        ));
    }

}