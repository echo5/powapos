<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\PageCategory;

class MenubarComposer {

    /**
     * @var $menu_items
     * @var $slug
     */
	protected $menu_items;
    protected $category;

    /**
     * Create a new PageCategory instance.
     */
	public function __construct(PageCategory $pageCategory)
    {
        $this->category = $pageCategory;
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view_data = $view->getData();
        $slug = $view_data['page']['slug'];
        $parent_slug = $view_data['page']['category']['slug'];

        $category = $this->category->getBySlug($slug);
        // @TODO enable for sibling pages
        // if (!$category) {
        //     $category = $this->category->getBySlug($parent_slug);
        // }

        if($category)
            $this->menu_items = $category->pages;

        $view->with('menu_items', $this->menu_items)->with('category', $category);
    }

}