<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\Product;
use Route;

class MfiComposer {

    /**
     * Product instance.
     *
     * @var Product
     */
    protected $products;

    /**
     * Create a new SdkComposer instance.
     */
	public function __construct(Product $product)
    {
        $this->products = $product->all();
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with('products', $this->products);
    }

}