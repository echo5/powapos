<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\Sdk;
use PPDevPortal\SdkDownload;
use PPDevPortal\ProductCategory;
use Route;

class SdkDownloadComposer {

    /**
     * SDK instance.
     *
     * @var Faq
     */
	protected $parentSdk;
    protected $productCategory;
    protected $sdkDownloads;

    /**
     * Create a new SdkComposer instance.
     */
	public function __construct(Sdk $parentSdk, ProductCategory $productCategory, SdkDownload $sdkDownload)
    {
        $sdk = Route::getCurrentRoute()->getParameter('sdk');
        $category = Route::getCurrentRoute()->getParameter('category');
        
        $this->productCategory = $productCategory->whereSlug($category)
            ->with([
                'products.sdks' => function($query) {
                    $query->where('sdks.slug', Route::getCurrentRoute()->getParameter('sdk'));
                    $query->orderBy('order', 'asc');
                },
                'products.sdks.downloads' => function($query) {
                    $query->ordered();
                },
            ])
            ->first();

        if (!$this->productCategory) \App::abort(404);
        
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'sdk' => $this->productCategory->products->first()->sdks->first(),
            'downloads' => $this->productCategory->products->first()->sdks->first()->downloads,
        ));
    }

}