<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\Tutorial;
use PPDevPortal\ProductCategory;
use Route;

class TutorialComposer {

    /**
     * Tutorial instance.
     *
     * @var Tutorial
     */
	protected $tutorial;
    protected $productCategory;

    /**
     * Create a new SdkComposer instance.
     */
	public function __construct(Tutorial $tutorial, ProductCategory $productCategory)
    {
        $tutorial = Route::getCurrentRoute()->getParameter('tutorial');
        $category = Route::getCurrentRoute()->getParameter('category');
        
        $this->productCategory = $productCategory->whereSlug($category)
            ->with([
                'products.tutorials' => function($query) {
                    $query->where('tutorials.slug', Route::getCurrentRoute()->getParameter('tutorial'));
                },
            ])
            ->first();

        if (!$this->productCategory) \App::abort(404);
        
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'tutorial' => $this->productCategory->products->first()->tutorials->first(),
        ));
    }

}