<?php namespace PPDevPortal\Http\Composers;

use PPDevPortal\ProductCategory;
use PPDevPortal\Platform;
use Route;

class TutorialsComposer {

    /**
     * ProductCategory instance.
     *
     * @var Faq
     */
    protected $productCategory;

    /**
     * Create a new SdkComposer instance.
     */
	public function __construct(ProductCategory $productCategory, Platform $platform)
    {
        $this->currentCategory = Route::getCurrentRoute()->getParameter('category');
        $platform_slug = Route::getCurrentRoute()->getParameter('platform');
        $this->currentPlatform = $platform->whereSlug($platform_slug)->first();

        if(!empty($platform_slug) && $platform_slug != 'all' && !$this->currentPlatform) \App::abort(404);

        $this->productCategories = $productCategory->all();
        $this->platforms = $platform->all();

        $this->productCategory = $productCategory->whereSlug($this->currentCategory)
            ->with([
                'products.tutorials' => function($query) {
                    $query->orderBy('order', 'asc');
                    $query->orderBy('product_id', 'asc');
                    $platform_slug = Route::getCurrentRoute()->getParameter('platform');
                    if(!empty($platform_slug) && $platform_slug != 'all')
                        $query->where('platform_id', $this->currentPlatform->id);
                },
                'products.tutorials.platform'
            ])
            ->first();

        if (!$this->productCategory) \App::abort(404);
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'products' => $this->productCategory->products,
            'productCategory' => $this->productCategory,
            'productCategories' => $this->productCategories,
            'platforms' => $this->platforms,
            'currentCategory' => $this->currentCategory,
            'currentPlatform' => $this->currentPlatform,
        ));
    }

}