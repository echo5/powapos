<?php namespace PPDevPortal\Http\Controllers\Auth;

use PPDevPortal\Http\Controllers\Controller;
use PPDevPortal\Http\Requests\UpdateUserRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class AccountController extends Controller {

	protected $user;

	public function __construct(Guard $auth)
	{
		$this->user = $auth->user();
		$this->middleware('auth');
	}
	
	public function view()
	{
		return view('auth.edit', ['user' => $this->user]);
	}
	
	public function save(UpdateUserRequest $request)
	{
		$this->user->first_name = $request->get('first_name');
		$this->user->last_name	= $request->get('last_name');
		if ($request->get('password')) {
			$this->user->password	= bcrypt($request->get('password'));
		}
		$this->user->save();
		return redirect('account')->with('message', 'Updated successfully');
	}

}
