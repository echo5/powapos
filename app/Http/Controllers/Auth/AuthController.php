<?php namespace PPDevPortal\Http\Controllers\Auth;

use PPDevPortal\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use PPDevPortal\Http\Requests\LoginUserRequest;
use PPDevPortal\User;
use PPDevPortal\Country;
use PPDevPortal\EmailTemplate;

class AuthController extends Controller {

	use AuthenticatesAndRegistersUsers;
	
	protected $redirectTo = '/';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister()
	{
		$countries = Country::orderBy('name', 'asc')->get();
		$countries_dropdown = array();
		foreach($countries as $country)
		{
			$countries_dropdown[$country->id] = $country->name;
		}
		
		return view('auth.register', ['countries' => $countries_dropdown]);
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$user = $this->registrar->create($request->all());
		
		$this->sendConfirmationEmail($user);

		return redirect('auth/login')
				->withInput($request->only('email'))
				->with('extra', 'sent.confirmation');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(LoginUserRequest $request)
	{
		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt(array_merge($credentials, ['confirmed' => true]), $request->has('remember')))
		{
			return redirect()->intended($this->redirectPath());
		}

		if($this->auth->validate($credentials))
		{
			return redirect($this->loginPath())
				->withInput($request->only('email', 'remember'))
				->with(['extra' => 'need.confirmation']);
		}

		return redirect($this->loginPath())
			->withInput($request->only('email', 'remember'))
			->withErrors([
				'email' => $this->getFailedLoginMessage(),
			]);
	}

	/**
     * Get and send confirmation where email
     * 
     * @param  string $email 
     * @return response
     */
	public function getConfirmation($email)
	{
		$user = User::where('email', $email)->first();
		if(!$user) \App::abort(404);
		$this->sendConfirmationEmail($user);
		
		return $this->sentConfirmationRedirect($email);
	}
	
    /**
     * Verify user confirmation code
     * 
     * @param  string $confirmation_code
     * @return response
     */
	public function verify($confirmation_code)
	{
		$user = User::where('confirmation_code', $confirmation_code)->first();
		
		if(!$user)
			return redirect($this->loginPath())
				->with('message', 'Confirmation code not found, please try logging in below.');
		
		$user->confirmation_code 	= NULL;
		$user->confirmed			= true;
		$user->save();
		
		return redirect($this->loginPath())
			->with('message', 'Your account is now confirmed. You can log in below.');
	}
	
    /**
     * Send confirmation email to user
     * 
     * @param  User $user
     */
	protected function sendConfirmationEmail($user)
	{
        $emailTemplate = EmailTemplate::whereView('emails.confirm')->first();

		\Mail::send($emailTemplate->view, [
            'confirmation_code' => $user->confirmation_code,
            'user' => $user
        ],
		function($message) use ($user, $emailTemplate)
		{
		    $message->to($user->email, $user->getFullName())
		    	->subject($emailTemplate->subject);
		});
	}
	
    /**
     * Redirect for after email sent
     * 
     * @param  string $email
     * @return response
     */
	protected function sentConfirmationRedirect($email)
	{
		return redirect('auth/login')
			->withInput(['email' => $email])
			->with('extra', 'sent.confirmation');
	}

}
