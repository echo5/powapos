<?php namespace PPDevPortal\Http\Controllers\Auth;

use PPDevPortal\Http\Controllers\Controller;
use PPDevPortal\EmailTemplate;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Request;

class PasswordController extends Controller {

	use ResetsPasswords;
	
	protected $redirectTo = '/';

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}


    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        $emailTemplate = EmailTemplate::whereView('emails.password')->first();
        return $emailTemplate->subject;
    }

}
