<?php namespace PPDevPortal\Http\Controllers;

use PPDevPortal\Http\Requests;
use PPDevPortal\Http\Controllers\Controller;
use PPDevPortal\Events\CompleteMfiApplication;
use Illuminate\Http\Request;
use PPDevPortal\MfiApplication;
use PPDevPortal\EmailTemplate;
use Input;
use Validator;
use Redirect;
use Session;
use Mail;
use Auth;
use View;

class MfiController extends Controller {

    /**
     * Handle MFI form submission
     * @return mixed
     */
    public function submitMfiForm(){

        $data = Input::all();

        $rules = array (
            'name' => 'required',
            'version' => 'required',
            'release_date' => 'required|date',
            'app_category' => 'required',
            'bundle_id' => 'required',
            'developer_name' => 'required',
            'existing' => 'required',
            'dependencies' => 'required',
            'description' => 'required|min:5',
        );

        $validator = Validator::make ($data, $rules);
        if ($validator -> passes()){
            if (Auth::check()) {
                $user = Auth::user();
                $data['user'] = $user;
                $data['user_id'] = $user->id;
                $data['products'] = (isset($data['products']) ? implode($data['products'], ', ') : '');
                $mfiApplication = new MfiApplication($data);
                $mfiApplication->save();

                $emailTemplate = EmailTemplate::whereView('emails.mfisubmission')->first();
                return View::make($emailTemplate->view)->with($data);
                Mail::send($emailTemplate->view, $data, function($message) use ($data, $emailTemplate, $user)
                {
                    // @TODO change these emails
                    $message->from('info@powaposdeveloper.powa.com', 'PowaPOS Developer Portal');               
                    $message->to($user->email, $data['developer_name'])->subject($emailTemplate->subject);

                });

                Session::flash('message', 'Your submission has been received!'); 
                Session::flash('alert-class', 'alert-success'); 
                return Redirect::back();
            }
            else {
                Session::flash('message', 'Please login before submitting your MFi application.'); 
                Session::flash('alert-class', 'alert-warning'); 
            }

        }
        else {
            // Session::flash('message', 'Sorry, there was an error with your submission.'); 
            // Session::flash('alert-class', 'alert-danger'); 
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

}
