<?php namespace PPDevPortal\Http\Controllers;

use PPDevPortal\Http\Requests;
use PPDevPortal\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PPDevPortal\Page;
use PPDevPortal\PageCategory;
use PPDevPortal\ProductCategory;
use PPDevPortal\User;
use View;
use Route;
use Config;
use Response;
use URL;
use Activity;
use Auth;
use PPDevPortal\Services\Shortcodes;
use Shortcode;

class PageController extends Controller {

    /**
     * @var Page
     */
    protected $page;

    /**
     * @param Page $page
     */
    public function __construct(Page $page, Shortcodes $shortcodes)
    {
        $this->page = $page;
        $this->shortcodes = $shortcodes;
    }

    /**
     * Download a file and record action
     * 
     * @return response
     */
    public function downloadFile()
    {
        $folder = Route::getCurrentRoute()->getParameter('folder');
        $filename = Route::getCurrentRoute()->getParameter('filename') . Route::getCurrentRoute()->getParameter('extension');
        $file_path = Config::get('assets.' . $folder . 'Directory');
        $full_path = public_path() . $file_path . $filename;
        if (file_exists($full_path)) {
            Activity::log([
                'contentType' => $folder,
                'action'      => 'Download',
                'description' => 'Downloaded a file',
                'details'     => $filename,
            ]);
            $response = Response::download(public_path() . $file_path . $filename, $filename);             
            ob_end_clean();
            return $response;
        }
        return 'File does not exist.';
    }

    /**
     * Display the specified page by full slug.
     *
     * @param  str  $hierarchy
     * @return mixed
     */
    public function showPageBySlug($slug)
    {
        return $this->getPageBySlug($slug);       
    }

    /**
     * Display the specified page by getting route as slug.
     *
     * @param  str  $hierarchy
     * @return mixed
     */
    public function showPageByRoute()
    {
        $slug = Route::getCurrentRoute()->getPath();
        return $this->getPageBySlug($slug);       
    }

    /**
     * Show page to user.
     *
     * @return mixed
     */
    public function showSupportPage()
    {
        return view('support.index');
    }

    /**
     * Show Demo Apps page to user.
     *
     * @return mixed
     */
    public function showDemoAppsPage($category)
    {
        return $this->getPageBySlug('demo-apps/' . $category, false);
    }

    /**
     * Show Demo Apps page to user.
     *
     * @return mixed
     */
    public function showPowaPOSToolsPage($category)
    {
        return $this->getPageBySlug('powapos-tools/' . $category, false);
    }

    /**
     * Show Tutorials page to user.
     *
     * @return mixed
     */
    public function showTutorialsPage($category)
    {
        return $this->getPageBySlug('tutorials/' . $category, false);
    }

    /**
     * Show individual Tutorial page to user.
     *
     * @return mixed
     */
    public function showTutorialPage()
    {
        return View::make('content.develop.tutorials.tutorial');
    }

    /**
     * Show Guides Index page to user.
     *
     * @return mixed
     */
    public function showGuidesIndex()
    {
        return $this->getPageBySlug('develop/guides-and-checklists', false);
    }

    /**
     * Show Guides page to user.
     *
     * @return mixed
     */
    public function showGuidesPage($category)
    {
        return $this->getPageBySlug('develop/guides-and-checklists/' . $category, false);
    }

    /**
     * Show SDK page to user.
     *
     * @return mixed
     */
    public function showSdkPage($category)
    {
        return $this->getPageBySlug('sdk-downloads/' . $category, false);
    }

    /**
     * Show SDK downloads page to user.
     *
     * @return mixed
     */
    public function showSdkDownloadsPage($category, $sdk)
    {
        return View::make('content.develop.sdk.sdk_downloads');            
    }


    /**
     * Display the specified page by category and slug.
     *
     * @param  str  $hierarchy
     * @return mixed
     */
    public function getPageBySlug($slug, $use_default = true)
    {
        $page = $this->page->where('slug', $slug)
            ->with('template')
            ->first();  
        if (!$page) \App::abort(404);
        $page['content'] = Shortcode::compile($page->content);
        $page['cssClasses'] = $this->getPageClasses($page);

        if ($page->template) {
            if (view()->exists($page->template->view)) {
                return View::make($page->template->view, compact('page'));
            }
        }
        if ($use_default)
            return View::make('content.default', compact('page'));            
        else 
            \App::abort(404);
    }

    /**
     * Generate CSS Classes for page
     * 
     * @param  Page   $page
     * @return str $cssClasses
     */
    public function getPageClasses(Page $page)
    {
        if ($page->slug == '/') {
            $pageClasses = ' page-home';
        }
        else {
            $classes = explode('/', $page->slug);
            $pageClasses = '';
            foreach ($classes as $class) {
                $pageClasses .= ' page-' . $class;
            }            
        }

        $templateClass = str_replace('.', '-', $page->template->view);
        $templateClass = str_replace('content-', '', $templateClass);
        $templateClass = 'template-' . $templateClass;

        $cssClasses = $templateClass . $pageClasses;
        return $cssClasses;
    }

    /**
     * Display the specified page by category and slug.
     * @TODO remove if not using categories
     *
     * @param  str  $hierarchy
     * @return mixed
     */
    public function showPageBySlugCategory($hierarchy)
    {
        $categories = explode('/', $hierarchy);
        $page_slug = array_pop($categories);
        $category = PageCategory::getBySlug(end($categories));
        $valid = true;

        // if there's a category in the slug
        if ($category) {
            $slugs[] = $category->slug;
            $valid = false;
            if($category->parent) {
                do {
                    $parent = $category->parent;
                    array_unshift($slugs, $parent->slug);
                } 
                while($parent->parent);
            }
            $slugs[] = $page_slug;
            $joined_slugs = join($slugs,"/");
            $valid = ($joined_slugs == $hierarchy);
        }   

        // category slug is valid, get page
        if ($valid) {
            if ($category) {
                $page = $this->page->where('slug', $page_slug)
                    ->where('category_id', $category->id)
                    ->with('template')
                    ->first();                
            }
            else {
                $page = $this->page->where('slug', $page_slug)
                    ->with('template')
                    ->first();  
            }
            if (!$page || ($page->category && empty($category))) \App::abort(404);
            if ($page->template) {
                if (view()->exists($page->template->view)) {
                    return View::make($page->template->view, compact('page'));
                }
            }
            return View::make('content.default', compact('page'));            
        }

        return \App::abort(404);

    }
}
