<?php namespace PPDevPortal\Http\Controllers\Support;

use PPDevPortal\Http\Controllers\Controller;
use Redirect;
use Config;
use Auth;
use Session;

class SupportController extends Controller {

    /**
     * Get URL for Freshdesk SSO
     * 
     * @return str
     */
    function getSsoUrl($name, $email) {
        $timestamp = time();
        $to_be_hashed = $name . $email . $timestamp;
        $hash = hash_hmac('md5', $to_be_hashed, Config::get('freshdesk.secret'));
        return Config::get('freshdesk.base_url') 
        . "login/sso/?name=" . urlencode($name)
        . "&email=" . urlencode($email)
        . "&timestamp=" . $timestamp 
        . "&hash=" . $hash;
    }

    /**
     * Redirect user to Freshdesk
     *
     * @return response
     */
    public function redirectToSupport() {
        if (Auth::check()) {
            $user = Auth::user();
            $url = $this->getSsoUrl($user->first_name, $user->email);
            return Redirect::to($url);  
        }
        Session::flash('message', 'There was an error getting your credentials.  Please contact us.'); 
        Session::flash('alert-class', 'alert-danger'); 
        return Redirect::back();
    }

}
