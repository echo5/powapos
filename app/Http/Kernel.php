<?php namespace PPDevPortal\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'PPDevPortal\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' 			=> 'PPDevPortal\Http\Middleware\Authenticate',
		'auth.basic' 	=> 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' 		=> 'PPDevPortal\Http\Middleware\RedirectIfAuthenticated',
		'admin'			=> 'PPDevPortal\Http\Middleware\CheckAdmin',
        'role'          => 'Bican\Roles\Middleware\VerifyRole',
        'permission'    => 'Bican\Roles\Middleware\VerifyPermission',
        'level'         => 'Bican\Roles\Middleware\VerifyLevel',
	];

}
