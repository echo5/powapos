<?php namespace PPDevPortal\Http\Requests;

use PPDevPortal\Http\Requests\Request;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'password'      => 'confirmed|min:6',
            'first_name'    => 'required',
            'last_name'     => 'required'
		];
	}

}
