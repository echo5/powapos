<?php namespace PPDevPortal\Http;

use View;

View::composer('modules.faq', 'PPDevPortal\Http\Composers\FaqComposer');
View::composer('modules.demo_apps', 'PPDevPortal\Http\Composers\DemoAppsComposer');
View::composer('modules.powapos_tools', 'PPDevPortal\Http\Composers\PowaPOSToolsComposer');
View::composer('modules.tutorials', 'PPDevPortal\Http\Composers\TutorialsComposer');
View::composer('content.develop.tutorials.tutorial', 'PPDevPortal\Http\Composers\TutorialComposer');
View::composer('modules.guides', 'PPDevPortal\Http\Composers\GuidesComposer');
View::composer('modules.checklists', 'PPDevPortal\Http\Composers\ChecklistsComposer');
View::composer('modules.sdk', 'PPDevPortal\Http\Composers\SdkComposer');
View::composer('content.develop.tseries.mfi', 'PPDevPortal\Http\Composers\MfiComposer');
View::composer('content.develop.sdk.sdk_downloads', 'PPDevPortal\Http\Composers\SdkDownloadComposer');
View::composer('partials.menu.category_menubar_side', 'PPDevPortal\Http\Composers\MenubarComposer');
View::composer('emails.*', 'PPDevPortal\Http\Composers\EmailTemplatesComposer');