
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Account management
Route::get('account', 'Auth\AccountController@view');
Route::put('account', 'Auth\AccountController@save');
Route::controllers([
	'auth' 			=> 'Auth\AuthController',
	'password' 		=> 'Auth\PasswordController',
]);

// Override and add admin package scripts/styles
View::composer(array('administrator::layouts.default'), function($view)
{
    $view->js['ckeditor'] = asset('js/ckeditor/ckeditor.js');
});
Route::get('{model}/a/file', array(
    'as' => 'admin_display_file',
    'uses' => 'AdminController@displayFile'
));

// File manager
Route::get('sample-ckeditor-integration', function () {
    return \Illuminate\Support\Facades\View::make('editor');
});
Route::get('/laravel-filemanager', '\Tsawler\Laravelfilemanager\controllers\LfmController@show');
Route::any('/laravel-filemanager/upload', '\Tsawler\Laravelfilemanager\controllers\UploadController@upload');
Route::get('/laravel-filemanager/jsonimages', '\Tsawler\Laravelfilemanager\controllers\ItemsController@getImages');
Route::get('/laravel-filemanager/jsonfiles', '\Tsawler\Laravelfilemanager\controllers\ItemsController@getFiles');
Route::get('/laravel-filemanager/newfolder', '\Tsawler\Laravelfilemanager\controllers\FolderController@getAddfolder');
Route::get('/laravel-filemanager/deletefolder', '\Tsawler\Laravelfilemanager\controllers\FolderController@getDeletefolder');
Route::get('/laravel-filemanager/folders', '\Tsawler\Laravelfilemanager\controllers\FolderController@getFolders');
Route::get('/laravel-filemanager/crop', '\Tsawler\Laravelfilemanager\controllers\CropController@getCrop');
Route::get('/laravel-filemanager/cropimage', '\Tsawler\Laravelfilemanager\controllers\CropController@getCropimage');
Route::get('/laravel-filemanager/rename', '\Tsawler\Laravelfilemanager\controllers\RenameController@getRename');
Route::get('/laravel-filemanager/resize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@getResize');
Route::get('/laravel-filemanager/doresize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@performResize');
Route::get('/laravel-filemanager/download', '\Tsawler\Laravelfilemanager\controllers\DownloadController@getDownload');
Route::get('/laravel-filemanager/delete', '\Tsawler\Laravelfilemanager\controllers\DeleteController@getDelete');

// Limited to developers and up
Route::get('develop/t-series/mfi', array('as' => 'mfi', 'middleware' => 'level:1', 'uses' => 'PageController@showPageByRoute'));
Route::post('develop/t-series/mfi', array('as' => 'mfi.submit', 'middleware' => 'level:1', 'uses' => 'MfiController@submitMfiForm'));
Route::get('demo-apps/{category}', array('as' => 'demoapps', 'middleware' => 'level:1', 'uses' => 'PageController@showDemoAppsPage'));
Route::get('demo-apps/{category}/{platform}', array('as' => 'demoapps', 'middleware' => 'level:1', 'uses' => 'PageController@showDemoAppsPage'));
Route::get('powapos-tools/{category}', array('as' => 'powapostools', 'middleware' => 'level:1', 'uses' => 'PageController@showPowaPOSToolsPage'));
Route::get('powapos-tools/{category}/{platform}', array('as' => 'powapostools', 'middleware' => 'level:1', 'uses' => 'PageController@showPowaPOSToolsPage'));
Route::get('support/tickets', array('as' => 'freshdesk', 'middleware' => 'level:1', 'uses' => 'Support\SupportController@redirectToSupport'));
Route::get('download/{folder}/{filename}{extension?}', array('as' => 'download', 'middleware' => 'level:1', 'uses' => 'PageController@downloadFile'))->where(array('filename' => '[a-zA-Z0-9-_]+', 'extension' => '\..+'));

// Page slugs
// Route::get('support', array('as' => 'pages.support', 'uses' => 'PageController@showSupportPage'));
Route::get('develop/guides-and-checklists', array('as' => 'guides', 'uses' => 'PageController@showGuidesIndex'));
Route::get('develop/guides-and-checklists/{category}', array('as' => 'guides', 'uses' => 'PageController@showGuidesPage'));
Route::get('tutorials/{category}', array('as' => 'tutorials', 'uses' => 'PageController@showTutorialsPage'));
Route::get('tutorials/{category}/{platform}', array('as' => 'tutorials', 'uses' => 'PageController@showTutorialsPage'));
Route::get('tutorials/{category}/{platform}/{tutorial}', array('as' => 'tutorial', 'uses' => 'PageController@showTutorialPage'));
Route::get('sdk-downloads/{category}', array('as' => 'sdks', 'uses' => 'PageController@showSdkPage'));
Route::get('sdk-downloads/{category}/{platform}', array('as' => 'sdks', 'uses' => 'PageController@showSdkPage'));
Route::get('sdk-downloads/{category}/{platform}/{sdk}', array('as' => 'sdkdownloads', 'uses' => 'PageController@showSdkDownloadsPage'));

// Catch all for page slugs MUST BE LAST
Route::get('{hierarchy}', array('as' => 'pages.show', 'uses' => 'PageController@showPageBySlug'))->where('hierarchy', '(.*)?');