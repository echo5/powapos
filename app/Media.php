<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'image' => 'required',
        'category_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    /**
    * Category relationship
    */
    public function category()
    {
        return $this->belongsTo('PPDevPortal\MediaCategory');
    }

    /**
     * Get download link
     * 
     * @return str
     */
    public function getDownloadLink()
    {
        if ($this->file)
            return route('download', array('folder' => 'mediaFiles', 'filename' => $this->file));
        else
            return route('download', array('folder' => 'media', 'filename' => $this->image));
    }

}
