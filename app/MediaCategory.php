<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class MediaCategory extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
    );

    /**
    * Category relationship
    */
    public function media()
    {
        return $this->hasMany('PPDevPortal\Media');
    }

}
