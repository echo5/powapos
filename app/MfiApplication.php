<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class MfiApplication extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
    	'email' => 'required|email',
    	'name' => 'required',
    	'version' => 'required',
    	'release_date' => 'required|date',
    	'app_category' => 'required',
    	'bundle_id' => 'required',
    	'developer_name' => 'required',
    	'existing' => 'required',
    	'dependencies' => 'required',
    	'description' => 'required|min:5',
    );

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'version',
        'release_date',
        'app_category',
        'bundle_id',
        'developer_name',
        'products',
        'existing_product',
        'dependencies',
        'description', 
    ];

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('PPDevPortal\User');
    }


}
