<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'slug' => 'required',
    );

    /**
    * Category relationship
    */
    public function category()
    {
        return $this->belongsTo('PPDevPortal\PageCategory');
    }

    /**
    * Template relationship
    */
    public function template()
    {
        return $this->belongsTo('PPDevPortal\PageTemplate');
    }

}
