<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model {

    /**
     * Table
     *
     * @var str
     */
    protected $table = 'page_categories';

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
    );

    /**
    * Pages relationship
    */
	public function pages()
	{
		return $this->hasMany('PPDevPortal\Page', 'category_id');
	}

    /**
    * Parent relationship
    */
    public function parent()
    {
        return $this->belongsTo('PPDevPortal\PageCategory', 'parent_id');
    }

    /**
    * Children relationship
    */
    public function children()
    {
        return $this->hasMany('PPDevPortal\PageCategory', 'parent_id');
    }

    /**
     * Returns a category based on slug string
     *
     * @param  str $slug
     * @return Category
     */
    public static function getBySlug($slug)
    {
        return PageCategory::where('slug', $slug)->first();
    }   
}
