<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class PageTemplate extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'view', 
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'view' => 'required',
    );

    /**
    * Pages relationship
    */
    public function pages()
    {
        return $this->hasMany('PPDevPortal\Pages');
    }

}
