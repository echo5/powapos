<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model {

	protected $fillable = [
        'name',
        'icon',
        'slug'
    ];

    /**
    * DemoApp relationship
    */
    public function demoApps()
    {
        return $this->hasMany('PPDevPortal\DemoApp');
    }

    /**
    * Tutorials relationship
    */
    public function tutorials()
    {
        return $this->hasMany('PPDevPortal\Tutorial');
    }

    /**
    * SDKs relationship
    */
    public function sdks()
    {
        return $this->hasMany('PPDevPortal\Sdk');
    }

}
