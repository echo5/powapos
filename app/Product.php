<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	
	protected $fillable = [
        'name',
        'category_id',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'category_id' => 'required',
    );
	
    /**
    * FAQ relationship
    */
    public function faqs()
    {
        return $this->hasMany('PPDevPortal\Faq');
    }

    /**
    * DemoApp relationship
    */
    public function demoApps()
    {
        return $this->hasMany('PPDevPortal\DemoApp');
    }
	
    /**
    * SDK relationship
    */
    public function sdks()
    {
        return $this->hasMany('PPDevPortal\Sdk');
    }
    
    /**
    * Product relationship
    */
    public function category()
    {
        return $this->belongsTo('PPDevPortal\ProductCategory');
    }

    /**
    * Query latest FAQs
    *
    * @param int
    * @return Faq
    */
    public function latestFaqs($qty = 5)
    {
        return $this->faqs()->orderBy('created_at')->take($qty)->get();
    }

    /**
    * Tutorials relationship
    */
    public function tutorials()
    {
        return $this->hasMany('PPDevPortal\Tutorial');
    }

}
