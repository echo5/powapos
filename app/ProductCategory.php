<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model {
	
	protected $fillable = ['name'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
    );

    /**
    * Product relationship
    */
	public function products()
	{
		return $this->hasMany('PPDevPortal\Product', 'category_id');
	}

    /**
    * DemoApps relationship
    */
    public function demoApps()
    {
        return $this->hasMany('PPDevPortal\DemoApp');
    }

}
