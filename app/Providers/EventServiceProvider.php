<?php namespace PPDevPortal\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use PPDevPortal\Events\CompleteMfiApplication;
use PPDevPortal\Handlers\Events\MarkMfiComplete;
use PPDevPortal\Handlers\Events\SendMfiCompleteEmail;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
			'EventListener',
		],
        CompleteMfiApplication::class => [
            MarkMfiComplete::class,
            SendMfiCompleteEmail::class,
        ]
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
