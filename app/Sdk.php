<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;
use PPDevPortal\Platform;

class Sdk extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'label' => 'required',
        'name' => 'required',
        'description' => 'required',
        'platform_id' => 'required',
        'product_id' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }
    
    /**
    * Product relationship
    */
    public function product()
    {
        return $this->belongsTo('PPDevPortal\Product');
    }

    /**
    * Platform relationship
    */
    public function platform()
    {
        return $this->belongsTo('PPDevPortal\Platform');
    }

    /**
    * SDK Download relationship
    */
    public function downloads()
    {
        return $this->hasMany('PPDevPortal\SdkDownload');
    }

}
