<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class SdkDownload extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
    );

    /**
     * Default query order
     * @param  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    /**
    * Product relationship
    */
    public function sdk()
    {
        return $this->belongsTo('PPDevPortal\Sdk');
    }

}
