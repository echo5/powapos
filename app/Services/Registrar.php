<?php namespace PPDevPortal\Services;

use PPDevPortal\User;
use Validator;
use Bican\Roles\Models\Role;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name'	=> 'required|max:255',
			'last_name'		=> 'required|max:255',
			'email'			=> 'required|email|max:255|unique:users',
			'password'		=> 'required|confirmed|min:6',
			'company'		=> 'required|max:255',
			'phone'			=> 'numeric',
			'country'		=> 'required|exists:countries,id',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		// create the user
		
		$user = new User([
			'first_name'		=> $data['first_name'],
			'last_name'			=> $data['last_name'],
			'email' 			=> $data['email'],
			'password' 			=> bcrypt($data['password']),
            'company'           => $data['company'],
			'job_title'			=> $data['job_title'],
			'phone'				=> $data['phone'],
			'country_id'		=> $data['country'],
			'confirmation_code'	=> str_random(30),
		]);
		
		$user->save();
		
		if(User::all()->count() > 1)
		{		
			// there are more than 1 users
			// give this user the 'user' role
			
			$userRole = Role::where('slug', 'user')->first();
			
			if(!$userRole)
			{
				$userRole = Role::create([
					'name'			=> 'User',
					'slug'			=> 'user',
					'description'	=> 'Standard user',
				]);
			}
			
			$user->attachRole($userRole);
		}
		else 
		{
			// there is only 1 user (this one)
			// lets give it the 'admin' role
			
			$adminRole = Role::where('slug', 'admin')->first();
			
			if(!$adminRole)
			{
				$adminRole = Role::create([
					'name'			=> 'Admin',
					'slug'			=> 'admin',
					'description'	=> 'Global Admin',
				]);
			}
			
			$user->attachRole($adminRole);
		}
		
		return $user;
	}

}
