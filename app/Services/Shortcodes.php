<?php namespace PPDevPortal\Services;

use PPDevPortal\User;
use PPDevPortal\Media;
use PPDevPortal\MediaCategory;
use Shortcode;
use HTML;
use Config;
use View;

class Shortcodes {

    /**
     * Init shortcodes
     * 
     * @return null
     */
    public function __construct() {
        $this->registerShortcodes();
    }

    /**
     * Register sitewide shortcodes
     * 
     * @return mixed
     */
    public function registerShortcodes()
    {

        Shortcode::register('confirmation_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            if(!empty($text))
                return '<a href="'.url('auth/verify/'.$this->confirmation_code).'">'. $text .'</a>';
            else
                return url('auth/verify/'.$this->confirmation_code);
        });  

        Shortcode::register('password_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            if(!empty($text))
                return '<a href="'.url('password/reset/'.$this->token).'">'. $text .'</a>';
            else
                return url('password/reset/'.$this->token);
        });   

        Shortcode::register('media', function($attr, $content = null, $name = null)
        {
            $cat = array_get($attr, 'cat');
            $class = array_get($attr, 'class');
            $col = array_get($attr, 'col');
            $col_span = $this->formatColumn($col);
            $image_size = array_get($attr, 'size');
            $image_size = ($image_size ? $image_size : 'small');

            $mediaCategory = MediaCategory::where('slug', $cat)->first();
            if (!$mediaCategory)
                return '<div class="alert alert-info">Sorry, ' . $cat .' isn\'t a valid category.</div>';
            $media = Media::where('category_id', $mediaCategory->id)->ordered()->get();
            if (!count($media))
                return '<div class="alert alert-info">Sorry, there is no media in this category.</div>';

            $data = array(
                'media' => $media,
                'col_span' => $col_span,
                'class' => $class,
                'col' => $col,
                'image_size' => $image_size
            );
            return View::make('modules.media_items')->with($data);
        });    

        Shortcode::register('section', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $background_image = array_get($attr, 'background');
            $padding = array_get($attr, 'padding');
            $doc = new \DomDocument;
            $doc->validateOnParse = true;
            $doc->loadHtml($text);
            $background_image = '';
            if ($doc->getElementByID('section-bg-img'))
                $background_image = $doc->getElementByID('section-bg-img')->getAttribute('src');
            return '</div></div></div></div><div class="section-container" style="background-image:url('.$background_image.');padding:'.$padding.'px 0px;"><div class="container">'. $text .'</div></div><div class="col-xs-12"><div class="container"><div class="row"><div class="col-md-12">';
        }); 

        Shortcode::register('button', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $url = array_get($attr, 'url');
            return '<a class="btn btn-primary" href="'.$url.'">'. $text .'</a>';
        });   

        Shortcode::register('user', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->user->{$param};
        });


        Shortcode::register('button_alt', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $url = array_get($attr, 'url');
            return '<a class="btn btn-alt" href="'.$url.'">'. $text .'</a>';
        });      

        Shortcode::register('icon', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $url = array_get($attr, 'url');
            $color = array_get($attr, 'color');
            $shadow = array_get($attr, 'shadow');

            $shadow_style = 'text-shadow: 1px 1px 0px ' . $shadow . ',';
            $shadow_style .= '2px 2px 0px ' . $shadow . ',';
            $shadow_style .= '3px 3px 0px ' . $shadow . ',';
            $shadow_style .= '4px 4px 0px ' . $shadow . ',';
            $shadow_style .= '5px 5px 0px ' . $shadow . ',';
            $shadow_style .= '6px 6px 0px ' . $shadow . ',';
            $shadow_style .= '7px 7px 0px ' . $shadow . ',';
            $shadow_style .= '8px 8px 0px ' . $shadow . ';';

            return '<span class="fa-stack icon-huge"><i class="fa fa-circle fa-stack-2x" style="color:'.$color.';"></i><i class="fa fa-'.$text.' fa-stack-1x fa-inverse icon-white" style="'.$shadow_style.'"></i></span>';
        });      
        // Code Parser
        Shortcode::register('code', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $height = array_get($attr, 'height');
            $height = (isset($height) ? $height : '300');
            $div_id = uniqid();

            ob_start();
            ?>
            <div id="code-wrap-<?php echo $div_id ?>" class="code-wrap">
                <textarea id="code-<?php echo $div_id ?>" name="code" class="code" style="height:<?php echo $height; ?>px;width:100%;"><?php echo $text ?></textarea>
                <script>
                    var delay;
                    var editor = CodeMirror.fromTextArea(document.getElementById('code-<?php echo $div_id ?>'), {
                        lineNumbers: true,
                        mode: "text/x-java",
                        matchBrackets: true
                    });
                    editor.setSize("100%", <?php echo $height; ?>);
                </script>
            </div>
            <?php
            $output = ob_get_clean();
            return $output;
        });
        // Shortcode::register('one_fourth', function($attr, $content = null, $name = null)
        // {
        //     $text = Shortcode::compile($content);
        //     return '<div class="col-md-3">'. $text .'</div>';
        // });      

        // Shortcode::register('one_third', function($attr, $content = null, $name = null)
        // {
        //     $text = Shortcode::compile($content);
        //     return '<div class="col-md-4">'. $text .'</div>';
        // });     

        // Shortcode::register('one_half', function($attr, $content = null, $name = null)
        // {
        //     $text = Shortcode::compile($content);
        //     return '<div class="col-md-6">'. $text .'</div>';
        // });       
    }

    /**
     * Format columns for bootstrap output
     * 
     * @param  str $col column span
     * @return str $col_span
     */
    public function formatColumn($col)
    {
        $col_span = 'col-sm-';

        switch ($col) {
            case '1':
                $col_span .= '12';
                break;
            case '2':
                $col_span .= '6';
                break;
            case '3':
                $col_span .= '4';
                break;
            case '4':
                $col_span .= '3';
                break;
            case '5':
                $col_span .= '15';
                break;
            case '6':
                $col_span .= '2';
                break;
            default:
                $col_span .= '15';
        }
        return $col_span;
    }

    /**
     * Set a user confirmation code
     * 
     * @param str $confirmation_code
     */
    public function setConfirmationCode($confirmation_code)
    {
        $this->confirmation_code = $confirmation_code;
    }

    /**
     * Set a token
     * 
     * @param $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Set user
     * 
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}