<?php namespace PPDevPortal;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model {

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'content' => 'required',
    );

    /**
    * Product relationship
    */
    public function product()
    {
        return $this->belongsTo('PPDevPortal\Product');
    }

    /**
    * Platform relationship
    */
    public function platform()
    {
        return $this->belongsTo('PPDevPortal\Platform');
    }

}
