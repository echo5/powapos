<?php namespace PPDevPortal;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Hash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract {

	use Authenticatable, CanResetPassword, HasRoleAndPermission;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 
		'last_name', 
		'email', 
		'password',
        'company',
		'job_title',
		'phone',
		'country_id',
		'confirmation_code',
        'confirmed' //@TODO remove after import
	];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email',
        'password' => 'required',
        'company' => 'required',
    );

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
    /**
     * Method for getting the full name of the user
     *
     * @return the user's full name
     */
    public function getFullName(){
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
    * Country relationship
    */
    public function country()
    {
        return $this->belongsTo('PPDevPortal\Country');
    }

    /**
    * MFi Application relationship
    */
    public function mfiApplications()
    {
        return $this->hasMany('PPDevPortal\MfiApplication');
    }

    /**
    * MFi Application relationship
    */
    public function activity()
    {
        return $this->hasMany('Regulus\ActivityLog\Models\Activity');
    }

    /**
     * Mutator for password
     *
     * @param $password
     */
    public function setPasswordAttribute($password){
        if (!empty($password)) {
            if(Hash::needsRehash($password)) {
                $this->attributes['password'] = Hash::make($password);
            }
            else {
                $this->attributes['password'] = $password;
            }
        }
    }

}
