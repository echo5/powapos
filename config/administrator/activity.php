<?php

/**
 * MFi model config
 */

return array(

    'title' => 'Activity',

    'single' => 'Action',

    'model' => 'Regulus\ActivityLog\Models\Activity',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'content_type' => array(
            'title' => 'Content Type',
            'select' => "content_type",
        ),
        'action' => array(
            'title' => 'Action',
            'select' => "action",
        ),
        'details' => array(
            'title' => 'Details',
            'select' => "details",
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => "CONCAT((:table).first_name, ' ', (:table).last_name)",
        ),
        'ip_address' => array(
            'title' => 'IP Address',
            'select' => "ip_address",
        ),
        'created_at' => array(
            'title' => 'Date',
            'select' => "created_at",
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'content_type' => array(
            'title' => 'Content Type',
            'type' => 'text',
        ),
        'action' => array(
            'title' => 'Action',
            'type' => 'text',
        ),
        'details' => array(
            'title' => 'Details',
            'text' => "text",
        ),
        'user' => array(
            'title' => 'User Email',
            'type' => 'relationship',
            'name_field' => 'email',
        ),
        'created_at' => array(
            'type' => 'date',
            'title' => 'Date',
            'date_format' => 'yy-mm-dd',
        ),
        'ip_address' => array(
            'title' => 'IP Address',
            'type' => 'text',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'action' => array(
            'title' => 'Action',
            'type' => 'text',
            'editable' => false,
        ),
        'user' => array(
            'title' => 'User Email',
            'type' => 'relationship',
            'name_field' => 'email',
            'editable' => false,
        ),
        'ip_address' => array(
            'title' => 'IP Address',
            'type' => 'text',
            'editable' => false,
        ),
        'content_type' => array(
            'title' => 'Content Type',
            'type' => 'text',
            'editable' => false,
        ),
        'details' => array(
            'title' => 'Details',
            'type' => 'text',
            'editable' => false,
        ),
        'created_at' => array(
            'title' => 'Date',
            'type' => "text",
            'editable' => false,

        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.activity');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.activity');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.activity');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.activity');
        }
    ),

);