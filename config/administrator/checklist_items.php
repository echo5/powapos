<?php

/**
 * Checklist items model config
 */

return array(

    'title' => 'Checklist Items',

    'single' => 'Checklist Item',

    'model' => 'PPDevPortal\ChecklistItem',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'checklist' => array(
            'title' => 'Checklist',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'checklist' => array(
            'title' => 'Checklist',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.checklists');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.checklists');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.checklists');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.checklists');
        }
    ),
);