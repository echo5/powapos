<?php

/**
 * Checlists model config
 */

return array(

    'title' => 'Installation Checklists',

    'single' => 'Checklist',

    'model' => 'PPDevPortal\Checklist',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'pdf' => array(
            'title' => 'File',
            'type' => 'file',
            'location' => public_path() . config('assets.checklistsDirectory'),
            'naming' => 'keep',
            'length' => 20,
            'size_limit' => 2,
            'mimes' => 'pdf,psd,doc,docx',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.checklists');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.checklists');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.checklists');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.checklists');
        }
    ),

);