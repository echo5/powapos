<?php

/**
 * Pages model config
 */

return array(

    'title' => 'Email Templates',

    'single' => 'Email Template',

    'model' => 'PPDevPortal\EmailTemplate',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'subject' => array(
            'title' => 'Subject',
            'select' => "subject",
        ),
        'view' => array(
            'title' => 'View',
            'select' => "view",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'subject' => array(
            'title' => 'Subject',
            'type' => 'text',
        ),
        'view' => array(
            'title' => 'View',
            'type' => 'text',
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'wysiwyg',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.emailtemplates');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.emailtemplates');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.emailtemplates');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.emailtemplates');
        }
    ),

);