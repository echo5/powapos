<?php

/**
 * FAQ model config
 */

return array(

    'title' => 'FAQ',

    'single' => 'FAQ',

    'model' => 'PPDevPortal\Faq',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'question' => array(
            'title' => 'Question',
            'select' => "question",
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'question' => array(
            'title' => 'Question',
            'type' => 'text',
        ),
        'answer' => array(
            'title' => 'Answer',
            'type' => 'wysiwyg',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),


    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.faqs');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.faqs');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.faqs');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.faqs');
        }
    ),

);