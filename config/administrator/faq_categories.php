<?php

/**
 * FAQ Categories model config
 */

return array(

    'title' => 'FAQ Categories',

    'single' => 'FAQ Category',

    'model' => 'PPDevPortal\FaqCategory',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.pagecategories');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.pagecategories');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.pagecategories');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.pagecategories');
        }
    ),

);