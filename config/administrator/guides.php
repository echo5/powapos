<?php

/**
 * Guides model config
 */

return array(

    'title' => 'Product Guides',

    'single' => 'Product Guide',

    'model' => 'PPDevPortal\Guide',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => 'name',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => '<img src="'.config('assets.guidesImageDirectory').'(:value)" height="100" />',
            'sortable' => false,
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.guidesImageDirectory'),
            'naming' => 'keep',
            'length' => 20,
            'size_limit' => 2,
        ),
        'file' => array(
            'title' => 'File / PDF',
            'type' => 'file',
            'location' => public_path() . config('assets.guidesDirectory'),
            'naming' => 'keep',
            'length' => 20,
            'size_limit' => 2,
            'mimes' => 'pdf,psd,doc,docx,rtf,txt',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),


    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.guides');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.guides');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.guides');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.guides');
        }
    ),

);