<?php

/**
 * Media model config
 */

return array(

    'title' => 'Media',

    'single' => 'Media',

    'model' => 'PPDevPortal\Media',

    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => 'name',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => '<img src="' . config('assets.mediaThumbsDirectory') . 'small/' . '(:value)" height="100" />',
            'sortable' => false,
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'image' => array(
            'title' => 'Thumbnail Image',
            'type' => 'image',
            'location' => public_path() . config('assets.mediaDirectory'),
            'naming' => 'keep',
            'length' => 20,
            'size_limit' => 2,
            'sizes' => array(
                array(150, 150, 'crop', public_path() . config('assets.mediaThumbsDirectory') . 'small/', 100),
                array(250, 80, 'crop', public_path() . config('assets.mediaThumbsDirectory') . 'medium/', 100),
            )
        ),
        'file' => array(
            'title' => 'File',
            'type' => 'file',
            'location' => public_path() . config('assets.mediaFilesDirectory'),
            'naming' => 'keep',
            'length' => 20,
            'size_limit' => 2,
            'mimes' => 'pdf,psd,doc,docx,ai,png,jpg,jpeg,gif,ai,tiff',
            'description' => 'The file field is the actual downloaded file.  If none is set, the image field\'s uncropped image will be used.  It can be a PSD, illustrator file, or any image file type.'
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.media');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.media');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.media');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.media');
        }
    ),

);