<?php

/**
 * Page Categories model config
 */

return array(

    'title' => 'Media Categories',

    'single' => 'Media Category',

    'model' => 'PPDevPortal\MediaCategory',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'slug' => array(
            'title' => 'Slug',
            'select' => "slug",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.mediacategories');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.mediacategories');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.mediacategories');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.mediacategories');
        }
    ),

);