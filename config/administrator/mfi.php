<?php

/**
 * MFi model config
 */

use PPDevPortal\Events\CompleteMfiApplication;

return array(

    'title' => 'MFi Applications',

    'single' => 'MFi Application',

    'model' => 'PPDevPortal\MfiApplication',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Application Name',
            'select' => "name",
        ),
        'developer_name' => array(
            'title' => 'Developer Name',
            'select' => "developer_name",
        ),
        'app_category' => array(
            'title' => 'Category',
            'select' => "app_category",
        ),
        'complete' => array(
            'title' => 'Complete',
            'select' => "IF((:table).complete, 'Yes', 'No')",
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'complete' => array(
            'title' => 'Complete',
            'type' => 'bool',
            'editable' => false,
        ),
        'user' => array(
            'title' => 'User Email',
            'type' => 'relationship',
            'name_field' => 'email',
            'editable' => false,
        ),
        'name' => array(
            'title' => 'Application Name',
            'type' => 'text',
            'editable' => false,
        ),
        'version' => array(
            'title' => 'Version',
            'type' => 'text',
            'editable' => false,
        ),
        'app_category' => array(
            'title' => 'Category',
            'type' => 'text',
            'editable' => false,
        ),
        'bundle_id' => array(
            'title' => 'Bundle ID',
            'type' => 'text',
            'editable' => false,
        ),
        'developer_name' => array(
            'title' => 'Developer Name',
            'type' => 'text',
            'editable' => false,
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'complete' => array(
            'title' => 'Complete',
            'type' => 'bool',
            'editable' => false,
        ),
        'user' => array(
            'title' => 'User Email',
            'type' => 'relationship',
            'name_field' => 'email',
            'editable' => false,
        ),
        'name' => array(
            'title' => 'Application Name',
            'type' => 'text',
            'editable' => false,
        ),
        'version' => array(
            'title' => 'Version',
            'type' => 'text',
            'editable' => false,
        ),
        'release_date' => array(
            'title' => 'Release Date',
            'type' => 'text',
            'editable' => false,
        ),
        'app_category' => array(
            'title' => 'Category',
            'type' => 'text',
            'editable' => false,
        ),
        'bundle_id' => array(
            'title' => 'Bundle ID',
            'type' => 'text',
            'editable' => false,
        ),
        'developer_name' => array(
            'title' => 'Developer Name',
            'type' => 'text',
            'editable' => false,
        ),
        'existing_product' => array(
            'title' => 'Existing Product',
            'type' => 'text',
            'editable' => false,
        ),
        'dependencies' => array(
            'title' => 'Dependencies',
            'type' => 'text',
            'editable' => false,
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'text',
            'editable' => false,
        ),
    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'mark_complete' => array(
            'title' => 'Mark complete & email user',
            'title' => function($model)
            {
                return 'Mark complete & email user';
                // return ($model->complete ? 'Mark incomplete' : 'Mark complete & email user');
            },
            'messages' => array(
                'active' => 'Sending email...',
                'success' => 'Email sent!',
                'error' => 'There was an error sending this email.',
            ),
            'action' => function(&$data)
            {
                \Event::fire(new CompleteMfiApplication($data->id));
                return true;
            }
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        // 'create' => function($model)
        // {
        //     return Auth::user()->can('create.mfiapplications');
        // },
        // 'update' => function($model)
        // {
        //     return Auth::user()->can('update.mfiapplications');
        // },
        // 'delete' => function($model)
        // {
        //     return Auth::user()->can('delete.mfiapplications');
        // },
        // 'view' => function($model)
        // {
        //     return Auth::user()->can('view.mfiapplications');
        // }
    ),

);