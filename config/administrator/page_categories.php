<?php

/**
 * Page Categories model config
 */

return array(

    'title' => 'Page Categories',

    'single' => 'Page Category',

    'model' => 'PPDevPortal\PageCategory',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'parent_name' => array(
            'title' => 'Parent Category',
            'relationship' => 'parent',
            'select' => '(:table).name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'parent' => array(
            'title' => 'Parent Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.pagecategories');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.pagecategories');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.pagecategories');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.pagecategories');
        }
    ),

);