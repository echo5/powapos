<?php

/**
 * Pages model config
 */

return array(

    'title' => 'Pages',

    'single' => 'Page',

    'model' => 'PPDevPortal\Page',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'title' => array(
            'title' => 'Title',
            'select' => "title",
        ),
        'slug' => array(
            'title' => 'Slug',
            'select' => "slug",
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'title' => array(
            'title' => 'Title',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'title' => array(
            'title' => 'Title',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'relationship',
            'name_field' => 'view',
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'wysiwyg',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.pages');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.pages');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.pages');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.pages');
        }
    ),

);