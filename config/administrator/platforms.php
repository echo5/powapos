<?php

/**
 * Platforms model config
 */

return array(

    'title' => 'Platforms',

    'single' => 'Platform',

    'model' => 'PPDevPortal\Platform',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'slug' => array(
            'title' => 'Slug',
            'select' => "slug",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.platforms');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.platforms');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.platforms');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.platforms');
        }
    ),

);