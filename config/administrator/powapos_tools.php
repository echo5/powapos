<?php

/**
 * PowaPOS Tools model config
 */

return array(

    'title' => 'PowaPOS Tools',

    'single' => 'PowaPOS Tool',

    'model' => 'PPDevPortal\DemoApp',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * Limit to type tools
     */
    'query_filter'=> function($query)
    {
        $query->where('type_id', '2');
    },

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'link' => array(
            'title' => 'Link',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        ),
        'type_id' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 2,
            'visible' => function($model)
            {
                return false; 
            },
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.powapostools');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.powapostools');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.powapostools');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.powapostools');
        }
    ),

);