<?php

/**
 * Product Categories model config
 */

return array(

    'title' => 'Product Categories',

    'single' => 'Product Category',

    'model' => 'PPDevPortal\ProductCategory',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.productcategories');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.productcategories');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.productcategories');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.productcategories');
        }
    ),

);