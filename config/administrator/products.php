<?php

/**
 * Products model config
 */

return array(

    'title' => 'Products',

    'single' => 'Product',

    'model' => 'PPDevPortal\Product',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.products');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.products');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.products');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.products');
        }
    ),

);