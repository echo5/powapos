<?php

/**
 * Roles model config
 */

return array(

    'title' => 'User Roles',

    'single' => 'User Role',

    'model' => 'Bican\Roles\Models\Role',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'description' => array(
            'title' => 'Description',
            'select' => "description",
        ),
        'level' => array(
            'title' => 'Level',
            'select' => "level",
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'text',
        ),
        'level' => array(
            'title' => 'Level',
            'type' => 'text',
            'description' => 'Levels 1-9 are members of the front-end.  Levels 10-20 are admin roles, with 20 being a super admin.'
        ),
        'permissions' => array(
            'title' => 'Role Permissions',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.roles');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.roles');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.roles');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.roles');
        }
    ),

);