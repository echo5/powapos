<?php

/**
 * SDK model config
 */

return array(

    'title' => 'SDKs',

    'single' => 'SDK',

    'model' => 'PPDevPortal\Sdk',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'label' => array(
            'title' => 'Label',
            'select' => "label",
        ),
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'product_name' => array(
            'title' => 'Product',
            'relationship' => 'product',
            'select' => '(:table).name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'label' => array(
            'title' => 'Label',
            'type' => 'text',
        ),
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'label' => array(
            'title' => 'Label',
            'type' => 'text',
        ),
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.sdks');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.sdks');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.sdks');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.sdks');
        }
    ),

);