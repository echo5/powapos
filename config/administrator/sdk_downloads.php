<?php

/**
 * SDK Downloads model config
 */

return array(

    'title' => 'SDK Downloads',

    'single' => 'SDK Download',

    'model' => 'PPDevPortal\SdkDownload',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'order',
        'direction' => 'asc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'sdk_name' => array(
            'title' => 'Parent SDK',
            'relationship' => 'sdk',
            'select' => '(:table).name',
        ),
        'order' => array(
            'title' => 'Order',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'sdk' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),
    

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'sdk' => array(
            'title' => 'Parent SDK',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'file' => array(
            'title' => 'File',
            'type' => 'file',
            'location' => public_path() . config('assets.sdksDirectory'),
            'naming' => 'keep',
            'length' => 100,
            'size_limit' => 100,
            'mimes' => 'pdf,zip,doc,docx',
        ),
        'order' => array(
            'type' => 'number',
            'title' => 'Sort Order',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.sdkdownloads');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.sdkdownloads');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.sdkdownloads');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.sdkdownloads');
        }
    ),

);