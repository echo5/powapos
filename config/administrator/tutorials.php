<?php

/**
 * SDK model config
 */

return array(

    'title' => 'Tutorials',

    'single' => 'Tutorial',

    'model' => 'PPDevPortal\Tutorial',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'product_name' => array(
            'title' => 'Product',
            'relationship' => 'product',
            'select' => '(:table).name',
        ),
        'platform_name' => array(
            'title' => 'Platform Name',
            'relationship' => 'platform',
            'select' => '(:table).name',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'wysiwyg',
        ),
        'product' => array(
            'title' => 'Product',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'platform' => array(
            'title' => 'Platform',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'order' => array(
            'title' => 'Order',
            'type' => 'number',
        )
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.tutorials');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.tutorials');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.tutorials');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.tutorials');
        }
    ),

);