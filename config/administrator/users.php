<?php

/**
 * Users model config
 */


return array(

    'title' => 'Users',

    'single' => 'User',

    'model' => 'PPDevPortal\User',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'full_name' => array(
            'title' => 'Name',
            'select' => "CONCAT((:table).first_name, ' ', (:table).last_name)",
        ),
        'email' => array(
            'title' => 'Email',
            'select' => "email",
        ),
        'country_name' => array(
            'title' => 'Country',
            'relationship' => 'country',
            'select' => '(:table).name',
        ),
        'activity' => array(
            'title' => 'Activity',
            'output' => function($activity)
            {
                $output = '';
                $n = 1;
                foreach ($activity as $action) {
                    if ($n++ <= 5) {
                        $output .= $action->action. ' / ';
                        $output .= $action->content_type. ' / ';
                        $output .= $action->details. '<br/>';
                    }
                }
                return '<div>' . $output . '</div>';
            },
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'first_name' => array(
            'title' => 'First Name',
        ),
        'last_name' => array(
            'title' => 'Last Name',
        ),
        'roles' => array(
            'title' => 'User Role',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'country' => array(
            'title' => 'Country',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'first_name' => array(
            'title' => 'First Name',
            'type' => 'text',
        ),
        'last_name' => array(
            'title' => 'Last Name',
            'type' => 'text',
        ),
        'email' => array(
            'title' => 'Email',
            'type' => 'text',
        ),
        'password' => array(
            'title' => 'Password',
            'type' => 'password',
        ),
        'company' => array(
            'title' => 'Company',
            'type' => 'text',
        ),
        'job_title' => array(
            'title' => 'Job Title',
            'type' => 'text',
        ),
        'country' => array(
            'title' => 'Country',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'roles' => array(
            'title' => 'User Role',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'confirmed' => array(
            'title' => 'Email Confirmed',
            'type' => 'bool',
        ),
    ),

    /**
     * Permissions
     */
    'action_permissions'=> array(
        'create' => function($model)
        {
            return Auth::user()->can('create.users');
        },
        'update' => function($model)
        {
            return Auth::user()->can('update.users');
        },
        'delete' => function($model)
        {
            return Auth::user()->can('delete.users');
        },
        'view' => function($model)
        {
            return Auth::user()->can('view.users');
        }
    ),

);