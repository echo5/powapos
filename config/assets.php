<?php

return [

    /*
     * Path to SDKs upload directory
     */
    'sdksDirectory' => '/uploads/sdks/',

    /*
     * Path to Demo Apps upload directory
     */
    'demoAppsDirectory' => '/uploads/demoapps/',

    /*
     * Path to Media upload directory
     */
    'mediaDirectory' => '/uploads/media/originals/',

    /*
     * Path to Media upload directory
     */
    'mediaFilesDirectory' => '/uploads/media/files/',

    /*
     * Path to Media thumbs upload directory
     */
    'mediaThumbsDirectory' => '/uploads/media/thumbs/',

    /*
     * Path to Guides upload directory
     */
    'guidesDirectory' => '/uploads/guides/pdfs/',   

    /*
     * Path to Guides upload directory
     */
    'guidesImageDirectory' => '/uploads/guides/images/',   

    /*
     * Path to Checklists upload directory
     */
    'checklistsDirectory' => '/uploads/checklists/',   

];
