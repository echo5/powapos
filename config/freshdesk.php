<?php

return [

	/*
	 * API Secret Key
	 */
	'secret' => env('FRESHDESK_SECRET'),

	/*
	 * Base URL for support site http://youraccount.freshdesk.com
	 */
	'base_url' => env('FRESHDESK_URL'),

];
