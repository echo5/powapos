<?php

return [
	
	/**
	 * Roles-based restrictions
	 * 
	 * Restrictions for routes based on the user's role
	 *
	 * Format is:
	 *     'route name' => 'required role slug'
	 */
	
	'roles'	=> [
		
		'admin'		=> ['admin', 'staff'],
		'admin.*'	=> ['admin', 'staff'],
		
	],
	
	'permissions' => [
	
		
	
	]

];
