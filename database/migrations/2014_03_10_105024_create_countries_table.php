<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('country_code');
			$table->string('region');			
			$table->timestamps();
		});
		
		DB::table('countries')->insert([
			['name'=>'Andorra', 'country_code'=>'AD', 'region'=>'EMEA'],
			['name'=>'United Arab Emirates', 'country_code'=>'AE', 'region'=>'EMEA'],
			['name'=>'Afghanistan', 'country_code'=>'AF', 'region'=>'EMEA'],
			['name'=>'Antigua and Barbuda', 'country_code'=>'AG', 'region'=>'LAC'],
			['name'=>'Anguilla', 'country_code'=>'AI', 'region'=>'LAC'],
			['name'=>'Albania', 'country_code'=>'AL', 'region'=>'EMEA'],
			['name'=>'Armenia', 'country_code'=>'AM', 'region'=>'EMEA'],
			['name'=>'Angola', 'country_code'=>'AO', 'region'=>'EMEA'],
			['name'=>'Antarctica', 'country_code'=>'AQ', 'region'=>'NA'],
			['name'=>'Argentina', 'country_code'=>'AR', 'region'=>'LAC'],
			['name'=>'American Samoa', 'country_code'=>'AS', 'region'=>'NA'],
			['name'=>'Austria', 'country_code'=>'AT', 'region'=>'EMEA'],
			['name'=>'Australia', 'country_code'=>'AU', 'region'=>'ASPAC'],
			['name'=>'Aruba', 'country_code'=>'AW', 'region'=>'LAC'],
			['name'=>'Åland Islands', 'country_code'=>'AX', 'region'=>'EMEA'],
			['name'=>'Azerbaijan', 'country_code'=>'AZ', 'region'=>'EMEA'],
			['name'=>'Bosnia and Herzegovina', 'country_code'=>'BA', 'region'=>'EMEA'],
			['name'=>'Barbados', 'country_code'=>'BB', 'region'=>'LAC'],
			['name'=>'Bangladesh', 'country_code'=>'BD', 'region'=>'ASPAC'],
			['name'=>'Belgium', 'country_code'=>'BE', 'region'=>'EMEA'],
			['name'=>'Burkina Faso', 'country_code'=>'BF', 'region'=>'EMEA'],
			['name'=>'Bulgaria', 'country_code'=>'BG', 'region'=>'EMEA'],
			['name'=>'Bahrain', 'country_code'=>'BH', 'region'=>'EMEA'],
			['name'=>'Burundi', 'country_code'=>'BI', 'region'=>'EMEA'],
			['name'=>'Benin', 'country_code'=>'BJ', 'region'=>'EMEA'],
			['name'=>'Saint Barthélemy', 'country_code'=>'BL', 'region'=>'LAC'],
			['name'=>'Bermuda', 'country_code'=>'BM', 'region'=>'LAC'],
			['name'=>'Brunei Darussalam', 'country_code'=>'BN', 'region'=>'ASPAC'],
			['name'=>'Bolivia', 'country_code'=>'BO', 'region'=>'LAC'],
			['name'=>'Caribbean Netherlands ', 'country_code'=>'BQ', 'region'=>'LAC'],
			['name'=>'Brazil', 'country_code'=>'BR', 'region'=>'LAC'],
			['name'=>'Bahamas', 'country_code'=>'BS', 'region'=>'LAC'],
			['name'=>'Bhutan', 'country_code'=>'BT', 'region'=>'ASPAC'],
			['name'=>'Bouvet Island', 'country_code'=>'BV', 'region'=>'EMEA'],
			['name'=>'Botswana', 'country_code'=>'BW', 'region'=>'EMEA'],
			['name'=>'Belarus', 'country_code'=>'BY', 'region'=>'EMEA'],
			['name'=>'Belize', 'country_code'=>'BZ', 'region'=>'LAC'],
			['name'=>'Canada', 'country_code'=>'CA', 'region'=>'NA'],
			['name'=>'Cocos (Keeling) Islands', 'country_code'=>'CC', 'region'=>'ASPAC'],
			['name'=>'Congo, Democratic Republic of', 'country_code'=>'CD', 'region'=>'EMEA'],
			['name'=>'Central African Republic', 'country_code'=>'CF', 'region'=>'EMEA'],
			['name'=>'Congo', 'country_code'=>'CG', 'region'=>'EMEA'],
			['name'=>'Switzerland', 'country_code'=>'CH', 'region'=>'EMEA'],
			['name'=>'Cóte d\'Ivoire', 'country_code'=>'CI', 'region'=>'EMEA'],
			['name'=>'Cook Islands', 'country_code'=>'CK', 'region'=>'ASPAC'],
			['name'=>'Chile', 'country_code'=>'CL', 'region'=>'LAC'],
			['name'=>'Cameroon', 'country_code'=>'CM', 'region'=>'EMEA'],
			['name'=>'China', 'country_code'=>'CN', 'region'=>'ASPAC'],
			['name'=>'Colombia', 'country_code'=>'CO', 'region'=>'LAC'],
			['name'=>'Costa Rica', 'country_code'=>'CR', 'region'=>'LAC'],
			['name'=>'Cuba', 'country_code'=>'CU', 'region'=>'LAC'],
			['name'=>'Cape Verde', 'country_code'=>'CV', 'region'=>'EMEA'],
			['name'=>'Curaçao', 'country_code'=>'CW', 'region'=>'LAC'],
			['name'=>'Christmas Island', 'country_code'=>'CX', 'region'=>'ASPAC'],
			['name'=>'Cyprus', 'country_code'=>'CY', 'region'=>'EMEA'],
			['name'=>'Czech Republic', 'country_code'=>'CZ', 'region'=>'EMEA'],
			['name'=>'Germany', 'country_code'=>'DE', 'region'=>'EMEA'],
			['name'=>'Djibouti', 'country_code'=>'DJ', 'region'=>'EMEA'],
			['name'=>'Denmark', 'country_code'=>'DK', 'region'=>'EMEA'],
			['name'=>'Dominica', 'country_code'=>'DM', 'region'=>'LAC'],
			['name'=>'Dominican Republic', 'country_code'=>'DO', 'region'=>'LAC'],
			['name'=>'Algeria', 'country_code'=>'DZ', 'region'=>'EMEA'],
			['name'=>'Ecuador', 'country_code'=>'EC', 'region'=>'LAC'],
			['name'=>'Estonia', 'country_code'=>'EE', 'region'=>'EMEA'],
			['name'=>'Egypt', 'country_code'=>'EG', 'region'=>'EMEA'],
			['name'=>'Western Sahara', 'country_code'=>'EH', 'region'=>'EMEA'],
			['name'=>'Eritrea', 'country_code'=>'ER', 'region'=>'EMEA'],
			['name'=>'Spain', 'country_code'=>'ES', 'region'=>'EMEA'],
			['name'=>'Ethiopia', 'country_code'=>'ET', 'region'=>'EMEA'],
			['name'=>'Finland', 'country_code'=>'FI', 'region'=>'EMEA'],
			['name'=>'Fiji', 'country_code'=>'FJ', 'region'=>'ASPAC'],
			['name'=>'Falkland Islands', 'country_code'=>'FK', 'region'=>'LAC'],
			['name'=>'Micronesia, Federated States of', 'country_code'=>'FM', 'region'=>'ASPAC'],
			['name'=>'Faroe Islands', 'country_code'=>'FO', 'region'=>'EMEA'],
			['name'=>'France', 'country_code'=>'FR', 'region'=>'EMEA'],
			['name'=>'Gabon', 'country_code'=>'GA', 'region'=>'EMEA'],
			['name'=>'United Kingdom', 'country_code'=>'GB', 'region'=>'EMEA'],
			['name'=>'Grenada', 'country_code'=>'GD', 'region'=>'LAC'],
			['name'=>'Georgia', 'country_code'=>'GE', 'region'=>'EMEA'],
			['name'=>'French Guiana', 'country_code'=>'GF', 'region'=>'LAC'],
			['name'=>'Guernsey', 'country_code'=>'GG', 'region'=>'EMEA'],
			['name'=>'Ghana', 'country_code'=>'GH', 'region'=>'EMEA'],
			['name'=>'Gibraltar', 'country_code'=>'GI', 'region'=>'EMEA'],
			['name'=>'Greenland', 'country_code'=>'GL', 'region'=>'EMEA'],
			['name'=>'Gambia', 'country_code'=>'GM', 'region'=>'EMEA'],
			['name'=>'Guinea', 'country_code'=>'GN', 'region'=>'EMEA'],
			['name'=>'Guadeloupe', 'country_code'=>'GP', 'region'=>'LAC'],
			['name'=>'Equatorial Guinea', 'country_code'=>'GQ', 'region'=>'EMEA'],
			['name'=>'Greece', 'country_code'=>'GR', 'region'=>'EMEA'],
			['name'=>'South Georgia and the South Sandwich Islands', 'country_code'=>'GS', 'region'=>'LAC'],
			['name'=>'Guatemala', 'country_code'=>'GT', 'region'=>'LAC'],
			['name'=>'Guam', 'country_code'=>'GU', 'region'=>'ASPAC'],
			['name'=>'Guinea-Bissau', 'country_code'=>'GW', 'region'=>'EMEA'],
			['name'=>'Guyana', 'country_code'=>'GY', 'region'=>'LAC'],
			['name'=>'Hong Kong', 'country_code'=>'HK', 'region'=>'ASPAC'],
			['name'=>'Heard and McDonald Islands', 'country_code'=>'HM', 'region'=>'EMEA'],
			['name'=>'Honduras', 'country_code'=>'HN', 'region'=>'LAC'],
			['name'=>'Croatia', 'country_code'=>'HR', 'region'=>'EMEA'],
			['name'=>'Haiti', 'country_code'=>'HT', 'region'=>'LAC'],
			['name'=>'Hungary', 'country_code'=>'HU', 'region'=>'EMEA'],
			['name'=>'Indonesia', 'country_code'=>'ID', 'region'=>'ASPAC'],
			['name'=>'Ireland', 'country_code'=>'IE', 'region'=>'EMEA'],
			['name'=>'Israel', 'country_code'=>'IL', 'region'=>'EMEA'],
			['name'=>'Isle of Man', 'country_code'=>'IM', 'region'=>'EMEA'],
			['name'=>'India', 'country_code'=>'IN', 'region'=>'ASPAC'],
			['name'=>'British Indian Ocean Territory', 'country_code'=>'IO', 'region'=>'ASPAC'],
			['name'=>'Iraq', 'country_code'=>'IQ', 'region'=>'EMEA'],
			['name'=>'Iran', 'country_code'=>'IR', 'region'=>'EMEA'],
			['name'=>'Iceland', 'country_code'=>'IS', 'region'=>'EMEA'],
			['name'=>'Italy', 'country_code'=>'IT', 'region'=>'EMEA'],
			['name'=>'Jersey', 'country_code'=>'JE', 'region'=>'EMEA'],
			['name'=>'Jamaica', 'country_code'=>'JM', 'region'=>'LAC'],
			['name'=>'Jordan', 'country_code'=>'JO', 'region'=>'EMEA'],
			['name'=>'Japan', 'country_code'=>'JP', 'region'=>'ASPAC'],
			['name'=>'Kenya', 'country_code'=>'KE', 'region'=>'EMEA'],
			['name'=>'Kyrgyzstan', 'country_code'=>'KG', 'region'=>'EMEA'],
			['name'=>'Cambodia', 'country_code'=>'KH', 'region'=>'LAC'],
			['name'=>'Kiribati', 'country_code'=>'KI', 'region'=>'LAC'],
			['name'=>'Comoros', 'country_code'=>'KM', 'region'=>'EMEA'],
			['name'=>'Saint Kitts and Nevis', 'country_code'=>'KN', 'region'=>'LAC'],
			['name'=>'North Korea', 'country_code'=>'KP', 'region'=>'ASPAC'],
			['name'=>'South Korea', 'country_code'=>'KR', 'region'=>'ASPAC'],
			['name'=>'Kuwait', 'country_code'=>'KW', 'region'=>'EMEA'],
			['name'=>'Cayman Islands', 'country_code'=>'KY', 'region'=>'LAC'],
			['name'=>'Kazakhstan', 'country_code'=>'KZ', 'region'=>'EMEA'],
			['name'=>'Lao People\'s Democratic Republic', 'country_code'=>'LA', 'region'=>'ASPAC'],
			['name'=>'Lebanon', 'country_code'=>'LB', 'region'=>'EMEA'],
			['name'=>'Saint Lucia', 'country_code'=>'LC', 'region'=>'LAC'],
			['name'=>'Liechtenstein', 'country_code'=>'LI', 'region'=>'EMEA'],
			['name'=>'Sri Lanka', 'country_code'=>'LK', 'region'=>'ASPAC'],
			['name'=>'Liberia', 'country_code'=>'LR', 'region'=>'EMEA'],
			['name'=>'Lesotho', 'country_code'=>'LS', 'region'=>'EMEA'],
			['name'=>'Lithuania', 'country_code'=>'LT', 'region'=>'EMEA'],
			['name'=>'Luxembourg', 'country_code'=>'LU', 'region'=>'EMEA'],
			['name'=>'Latvia', 'country_code'=>'LV', 'region'=>'EMEA'],
			['name'=>'Libya', 'country_code'=>'LY', 'region'=>'EMEA'],
			['name'=>'Morocco', 'country_code'=>'MA', 'region'=>'EMEA'],
			['name'=>'Monaco', 'country_code'=>'MC', 'region'=>'EMEA'],
			['name'=>'Moldova', 'country_code'=>'MD', 'region'=>'EMEA'],
			['name'=>'Montenegro', 'country_code'=>'ME', 'region'=>'EMEA'],
			['name'=>'Saint-Martin (France)', 'country_code'=>'MF', 'region'=>'LAC'],
			['name'=>'Madagascar', 'country_code'=>'MG', 'region'=>'EMEA'],
			['name'=>'Marshall Islands', 'country_code'=>'MH', 'region'=>'ASPAC'],
			['name'=>'Macedonia', 'country_code'=>'MK', 'region'=>'EMEA'],
			['name'=>'Mali', 'country_code'=>'ML', 'region'=>'EMEA'],
			['name'=>'Myanmar', 'country_code'=>'MM', 'region'=>'ASPAC'],
			['name'=>'Mongolia', 'country_code'=>'MN', 'region'=>'ASPAC'],
			['name'=>'Macau', 'country_code'=>'MO', 'region'=>'ASPAC'],
			['name'=>'Northern Mariana Islands', 'country_code'=>'MP', 'region'=>'ASPAC'],
			['name'=>'Martinique', 'country_code'=>'MQ', 'region'=>'LAC'],
			['name'=>'Mauritania', 'country_code'=>'MR', 'region'=>'EMEA'],
			['name'=>'Montserrat', 'country_code'=>'MS', 'region'=>'LAC'],
			['name'=>'Malta', 'country_code'=>'MT', 'region'=>'EMEA'],
			['name'=>'Mauritius', 'country_code'=>'MU', 'region'=>'EMEA'],
			['name'=>'Maldives', 'country_code'=>'MV', 'region'=>'ASPAC'],
			['name'=>'Malawi', 'country_code'=>'MW', 'region'=>'EMEA'],
			['name'=>'Mexico', 'country_code'=>'MX', 'region'=>'LAC'],
			['name'=>'Malaysia', 'country_code'=>'MY', 'region'=>'ASPAC'],
			['name'=>'Mozambique', 'country_code'=>'MZ', 'region'=>'EMEA'],
			['name'=>'Namibia', 'country_code'=>'NA', 'region'=>'EMEA'],
			['name'=>'New Caledonia', 'country_code'=>'NC', 'region'=>'ASPAC'],
			['name'=>'Niger', 'country_code'=>'NE', 'region'=>'EMEA'],
			['name'=>'Norfolk Island', 'country_code'=>'NF', 'region'=>'ASPAC'],
			['name'=>'Nigeria', 'country_code'=>'NG', 'region'=>'EMEA'],
			['name'=>'Nicaragua', 'country_code'=>'NI', 'region'=>'EMEA'],
			['name'=>'The Netherlands', 'country_code'=>'NL', 'region'=>'EMEA'],
			['name'=>'Norway', 'country_code'=>'NO', 'region'=>'EMEA'],
			['name'=>'Nepal', 'country_code'=>'NP', 'region'=>'ASPAC'],
			['name'=>'Nauru', 'country_code'=>'NR', 'region'=>'ASPAC'],
			['name'=>'Niue', 'country_code'=>'NU', 'region'=>'ASPAC'],
			['name'=>'New Zealand', 'country_code'=>'NZ', 'region'=>'ASPAC'],
			['name'=>'Oman', 'country_code'=>'OM', 'region'=>'EMEA'],
			['name'=>'Panama', 'country_code'=>'PA', 'region'=>'LAC'],
			['name'=>'Peru', 'country_code'=>'PE', 'region'=>'LAC'],
			['name'=>'French Polynesia', 'country_code'=>'PF', 'region'=>'ASPAC'],
			['name'=>'Papua New Guinea', 'country_code'=>'PG', 'region'=>'ASPAC'],
			['name'=>'Philippines', 'country_code'=>'PH', 'region'=>'ASPAC'],
			['name'=>'Pakistan', 'country_code'=>'PK', 'region'=>'EMEA'],
			['name'=>'Poland', 'country_code'=>'PL', 'region'=>'EMEA'],
			['name'=>'St. Pierre and Miquelon', 'country_code'=>'PM', 'region'=>'NA'],
			['name'=>'Pitcairn', 'country_code'=>'PN', 'region'=>'ASPAC'],
			['name'=>'Puerto Rico', 'country_code'=>'PR', 'region'=>'LAC'],
			['name'=>'Palestine, State of', 'country_code'=>'PS', 'region'=>'EMEA'],
			['name'=>'Portugal', 'country_code'=>'PT', 'region'=>'EMEA'],
			['name'=>'Palau', 'country_code'=>'PW', 'region'=>'ASPAC'],
			['name'=>'Paraguay', 'country_code'=>'PY', 'region'=>'LAC'],
			['name'=>'Qatar', 'country_code'=>'QA', 'region'=>'EMEA'],
			['name'=>'Réunion', 'country_code'=>'RE', 'region'=>'EMEA'],
			['name'=>'Romania', 'country_code'=>'RO', 'region'=>'EMEA'],
			['name'=>'Serbia', 'country_code'=>'RS', 'region'=>'EMEA'],
			['name'=>'Russian Federation', 'country_code'=>'RU', 'region'=>'EMEA'],
			['name'=>'Rwanda', 'country_code'=>'RW', 'region'=>'EMEA'],
			['name'=>'Saudi Arabia', 'country_code'=>'SA', 'region'=>'EMEA'],
			['name'=>'Solomon Islands', 'country_code'=>'SB', 'region'=>'ASPAC'],
			['name'=>'Seychelles', 'country_code'=>'SC', 'region'=>'EMEA'],
			['name'=>'Sudan', 'country_code'=>'SD', 'region'=>'EMEA'],
			['name'=>'Sweden', 'country_code'=>'SE', 'region'=>'EMEA'],
			['name'=>'Singapore', 'country_code'=>'SG', 'region'=>'ASPAC'],
			['name'=>'Saint Helena', 'country_code'=>'SH', 'region'=>'EMEA'],
			['name'=>'Slovenia', 'country_code'=>'SI', 'region'=>'EMEA'],
			['name'=>'Svalbard and Jan Mayen Islands', 'country_code'=>'SJ', 'region'=>'EMEA'],
			['name'=>'Slovakia', 'country_code'=>'SK', 'region'=>'EMEA'],
			['name'=>'Sierra Leone', 'country_code'=>'SL', 'region'=>'EMEA'],
			['name'=>'San Marino', 'country_code'=>'SM', 'region'=>'EMEA'],
			['name'=>'Senegal', 'country_code'=>'SN', 'region'=>'EMEA'],
			['name'=>'Somalia', 'country_code'=>'SO', 'region'=>'EMEA'],
			['name'=>'Suriname', 'country_code'=>'SR', 'region'=>'EMEA'],
			['name'=>'South Sudan', 'country_code'=>'SS', 'region'=>'EMEA'],
			['name'=>'Sao Tome and Principe', 'country_code'=>'ST', 'region'=>'EMEA'],
			['name'=>'El Salvador', 'country_code'=>'SV', 'region'=>'LAC'],
			['name'=>'Sint Maarten (Dutch part)', 'country_code'=>'SX', 'region'=>'LAC'],
			['name'=>'Syria', 'country_code'=>'SY', 'region'=>'EMEA'],
			['name'=>'Swaziland', 'country_code'=>'SZ', 'region'=>'EMEA'],
			['name'=>'Turks and Caicos Islands', 'country_code'=>'TC', 'region'=>'LAC'],
			['name'=>'Chad', 'country_code'=>'TD', 'region'=>'EMEA'],
			['name'=>'French Southern Territories', 'country_code'=>'TF', 'region'=>'EMEA'],
			['name'=>'Togo', 'country_code'=>'TG', 'region'=>'EMEA'],
			['name'=>'Thailand', 'country_code'=>'TH', 'region'=>'ASPAC'],
			['name'=>'Tajikistan', 'country_code'=>'TJ', 'region'=>'EMEA'],
			['name'=>'Tokelau', 'country_code'=>'TK', 'region'=>'ASPAC'],
			['name'=>'Timor-Leste', 'country_code'=>'TL', 'region'=>'ASPAC'],
			['name'=>'Turkmenistan', 'country_code'=>'TM', 'region'=>'EMEA'],
			['name'=>'Tunisia', 'country_code'=>'TN', 'region'=>'EMEA'],
			['name'=>'Tonga', 'country_code'=>'TO', 'region'=>'ASPAC'],
			['name'=>'Turkey', 'country_code'=>'TR', 'region'=>'EMEA'],
			['name'=>'Trinidad and Tobago', 'country_code'=>'TT', 'region'=>'LAC'],
			['name'=>'Tuvalu', 'country_code'=>'TV', 'region'=>'ASPAC'],
			['name'=>'Taiwan', 'country_code'=>'TW', 'region'=>'ASPAC'],
			['name'=>'Tanzania', 'country_code'=>'TZ', 'region'=>'EMEA'],
			['name'=>'Ukraine', 'country_code'=>'UA', 'region'=>'EMEA'],
			['name'=>'Uganda', 'country_code'=>'UG', 'region'=>'EMEA'],
			['name'=>'United States Minor Outlying Islands', 'country_code'=>'UM', 'region'=>'ASPAC'],
			['name'=>'United States', 'country_code'=>'US', 'region'=>'NA'],
			['name'=>'Uruguay', 'country_code'=>'UY', 'region'=>'LAC'],
			['name'=>'Uzbekistan', 'country_code'=>'UZ', 'region'=>'EMEA'],
			['name'=>'Vatican', 'country_code'=>'VA', 'region'=>'EMEA'],
			['name'=>'Saint Vincent and the Grenadines', 'country_code'=>'VC', 'region'=>'LAC'],
			['name'=>'Venezuela', 'country_code'=>'VE', 'region'=>'LAC'],
			['name'=>'Virgin Islands (British)', 'country_code'=>'VG', 'region'=>'LAC'],
			['name'=>'Virgin Islands (U.S.)', 'country_code'=>'VI', 'region'=>'LAC'],
			['name'=>'Vietnam', 'country_code'=>'VN', 'region'=>'ASPAC'],
			['name'=>'Vanuatu', 'country_code'=>'VU', 'region'=>'ASPAC'],
			['name'=>'Wallis and Futuna Islands', 'country_code'=>'WF', 'region'=>'ASPAC'],
			['name'=>'Samoa', 'country_code'=>'WS', 'region'=>'EMEA'],
			['name'=>'Yemen', 'country_code'=>'YE', 'region'=>'EMEA'],
			['name'=>'Mayotte', 'country_code'=>'YT', 'region'=>'EMEA'],
			['name'=>'South Africa', 'country_code'=>'ZA', 'region'=>'EMEA'],
			['name'=>'Zambia', 'country_code'=>'ZM', 'region'=>'EMEA'],
			['name'=>'Zimbabwe', 'country_code'=>'ZW', 'region'=>'EMEA'],
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
