<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use PPDevPortal\Platform;

class CreatePlatformsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('platforms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('slug');
			$table->timestamps();
		});
		
		$android = Platform::create([
            'id'   => '1',
			'name' => 'Android',
			'slug' => 'android',
		]);
		
		$ios = Platform::create([
            'id'   => '2',
			'name' => 'iOS',
			'slug' => 'apple',
		]);
		
		$windows = Platform::create([
            'id'   => '3',
			'name' => 'Windows',
			'slug' => 'windows',
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('platforms');
	}

}
