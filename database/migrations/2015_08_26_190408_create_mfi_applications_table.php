<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfiApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('mfi_applications', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('version');
            $table->date('release_date');
            $table->string('app_category');
            $table->string('bundle_id');
            $table->string('developer_name');
            $table->string('products');
            $table->boolean('existing_product');
            $table->string('dependencies');
            $table->text('description');
            $table->boolean('complete');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mfi_applications');
	}

}
