<?php

use PPDevPortal\ChecklistItem;
use Illuminate\Database\Seeder;

class ChecklistItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $checklist_items = [
            [
                'id' => 1,
                'name' => 'Do you have the latest version of the iOS/Android/Windows SDK?',
                'description' => 'Make sure you or your software developers are coding your payment application using the latest version of the PowaPOS SDK (available in the Developer Portal). PowaPOS performs infrequent updates to the SDK to include product updates and feature enhancements.',
                'checklist_id' => '1'
            ],
            [
                'id' => 2,
                'name' => 'Check T25 firmware',
                'description' => 'Using the T25 Hardware Test App (available at the Developer Portal), check to make sure you have the latest (or required) version of the T25 firmware. If not, download the Firmware Update utility and update to the required version of the firmware.',
                'checklist_id' => '1',
            ],
        ];

        foreach ($checklist_items as $item)
            ChecklistItem::create($item);

    }

}