<?php

use PPDevPortal\Checklist;
use Illuminate\Database\Seeder;

class ChecklistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $checklists = [
            [
                'id' => 1,
                'name' => 'ISV / VAR Best Practices Checklist',
                'description' => '<p>The items below serve as a checklist for any party developing and/or distributing a tablet-based payment application integrated with the PowaPOS T25 to its merchant base. These are best practices in the development of the payment application and installation of the full POS solution at merchant location(s).</p><p>If you are completing the installation on behalf of your merchant customer, we also recommend reviewing the Merchant Checklist Installation Best Practices.</p>',
                'pdf' => 'T25-Install-Best-Practices-ISV_VAR_5.29.pdf',
                'product_id' => '1',
            ],
            [
                'id' => 2,
                'name' => 'Merchant Best Practices Checklist',
                'description' => 'The items below serve as a checklist for merchants and/or VARs who will be installing the PowaPOS T25 onsite. These are best practices to ensure the full POS solution (software, hardware, and processing) is functioning as expected and will not interrupt the flow of business.',
                'pdf' => 'T25-Install-Best-Practices-Merchant_5.29-1.pdf',
                'product_id' => '1',
            ],
            [
                'id' => 3,
                'name' => 'Distributor Best Practices Checklist',
                'description' => 'The items below serve as a checklist for certified distribution partners selling the PowaPOS T25.',
                'pdf' => 'T25-Install-Best-Practices-Distributor_5.29-1.pdf',
                'product_id' => '1',
            ],
        ];

        foreach ($checklists as $checklist)
            Checklist::create($checklist);

    }

}