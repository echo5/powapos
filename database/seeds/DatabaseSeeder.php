<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        DB::table('roles')->delete();
        DB::table('users')->delete();

        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('ProductCategoriesTableSeeder');
        $this->call('ProductsTableSeeder');
        $this->call('PermissionsTableSeeder');
        $this->call('PageTemplatesTableSeeder');
        $this->call('PageCategoriesTableSeeder');
        $this->call('PagesTableSeeder');
        $this->call('FaqCategoriesTableSeeder');
        $this->call('FaqsTableSeeder');
        $this->call('SdksTableSeeder');
        $this->call('SdkDownloadsTableSeeder');
        $this->call('DemoAppsTableSeeder');
        $this->call('TutorialsTableSeeder');
        $this->call('EmailTemplatesTableSeeder');
        $this->call('ChecklistsTableSeeder');
        $this->call('ChecklistItemsTableSeeder');
	}

    /**
     * Loads data from $source
     *
     * @param $source   string  Source data file
     * @param $closure  Closure Closure to execute on each line
     */
    public function loadCsvData($source, Closure $closure)
    {
        $handle = fopen(__DIR__ . '/source/' . $source, 'r');
        $headers = fgetcsv($handle);

        while (($row = fgetcsv($handle)) !== false) {
            call_user_func($closure, array_combine($headers, $row));
        }

        fclose($handle);

    }

}
