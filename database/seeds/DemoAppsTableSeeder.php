<?php

use Illuminate\Database\Seeder;
use PPDevPortal\DemoApp;

class DemoAppsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demo_apps = [
            [
                'name' => 'PowaPOS T25 Android Tablet Register (Yomani)',
                'description' => 'This SDK release includes support for the production hardware and firmware. The supported T-Series peripherals are: Cash Drawer, Printer, Rotation Sensor, USB Ports and Serial Scanner.',
                'product_id' => '1',
                'platform_id' => '1',
                'type_id' => '1',
                'link' => 'https://miadev.warp68.net:10443/AppStore/TabletRegisterYomani.apk'
            ],
            [
                'name' => 'PowaPOS T25 iOS Tablet Register (PowaPIN)',
                'description' => '<p>A mock tablet register demo app which will allow you to demonstrate the functionality of the T-Series in a typical POS software environment.</p><p>Please be aware this download works for iOS versions 8.1.2 and below. Contact us for an IPA file for iOS 8.1.3 and newer.</p>',
                'product_id' => '1',
                'platform_id' => '2',
                'type_id' => '1',
                'link' => 'http://tinyurl.com/qa3xzzb'
            ],
            [
                'name' => 'PowaPOS T25 iOS Tablet Register (MX925)',
                'description' => '',
                'product_id' => '1',
                'platform_id' => '2',
                'type_id' => '1',
                'link' => 'http://tinyurl.com/pc3tjka'
            ],
            [
                'name' => 'PowaPOS T25 Windows Cinema Demo (Vx805)',
                'description' => 'A mock tablet register demo app which will allow you to demonstrate the functionality of the T-Series in a typical POS software environment.',
                'product_id' => '1',
                'platform_id' => '3',
                'type_id' => '1',
                'link' => '/uploads/files/T25-CINEMAS.zip'
            ],
            [
                'name' => 'PowaTools 1.0 for Android (APK)',
                'description' => 'PowaTools allows you to get get system status and information, perform hardware tests, upgrade firmware, and view help content from the Developer Portal.',
                'product_id' => '1',
                'platform_id' => '1',
                'type_id' => '2',
                'link' => 'https://powapos.freshdesk.com/helpdesk/attachments/1027253394'
            ],
            [
                'name' => 'PowaTools 1.0 for iOS',
                'description' => '<p>PowaTools allows you to get get system status and information, perform hardware tests, upgrade firmware, and view help content from the Developer Portal.</p>
<p><b>Please note:</b> This .zip file contains an IPA for download to Mac or PC, to allow manual installation through iTunes. </p>',
                'product_id' => '1',
                'platform_id' => '2',
                'type_id' => '2',
                'link' => '/uploads/files/PowaTools.ipa_.zip'
            ],
            [
                'name' => 'PowaPIN Sample Application',
                'description' => 'A mock mobile payment demo app which will allow you to demonstrate the functionality of the PowaPIN.',
                'product_id' => '3',
                'platform_id' => '1',
                'type_id' => '1',
                'link' => 'https://powapos.freshdesk.com/helpdesk/attachments/1022556555'
            ],
            [
                'name' => 'PowaPIN Sample Application Source Code',
                'description' => 'Source code for a mock mobile payments demo app which will allow you to demonstrate the functionality of the PowaPIN software environment.',
                'product_id' => '3',
                'platform_id' => '2',
                'type_id' => '1',
                'link' => '/uploads/files/PowaPINSDKSampleApp.zip'
            ],
        ];

        foreach ($demo_apps as $demo_app) {
            DemoApp::create($demo_app);
        }

    }

}