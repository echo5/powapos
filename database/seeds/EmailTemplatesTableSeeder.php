<?php

use Illuminate\Database\Seeder;
use PPDevPortal\EmailTemplate;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = [
            [
                'view' => 'emails.mfisubmission',
                'subject' => 'MFi Application Received',
                'content' => '<p>Thank you for your MFi submission with the PowaPOS Developer Portal.</p>
                                <p>We\'ll be in touch with you after approval.</p>
                                <p>Kind regards,<br />The PowaPOS Team</p>',
            ],
            [
                'view' => 'emails.confirm',
                'subject' => 'Confirm Your Email',
                'content' => '<p>Thank you for registering with the PowaPOS Developer Portal.</p>
                        <p>[confirmation_link]Click here to confirm your email address[/confirmation_link] or copy and paste the following URL into your browser:</p>
                        <p>[confirmation_link]</p>
                        <p>Kind regards,<br />
                        The PowaPOS Team</p>',
            ],
            [
                'view' => 'emails.password',
                'subject' => 'Reset Your Password',
                'content' => '<p>We\'re sorry to hear you\'re having trouble logging into your PowaPOS Developer account.</p>
                            <p>[password_link]Click here to reset your password[/password_link] or copy and paste the following URL into your browser:</p>
                            <p>[password_link]</p>
                            <p>Kind regards,<br />The PowaPOS Team</p>',
            ],
            [
                'view' => 'emails.mficomplete',
                'subject' => 'MFi Application Complete',
                'content' => '<p>You\'re MFi application was completed successfully.</p>
                            <p>Kind regards,<br />The PowaPOS Team</p>',
            ],
        ];

        foreach ($templates as $template)
            EmailTemplate::create($template);

    }

}