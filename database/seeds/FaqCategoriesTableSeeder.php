<?php

use Illuminate\Database\Seeder;
use PPDevPortal\FaqCategory;

class FaqCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'T25 Hardware',
            ],
            [
                'id' => 2,
                'name' => 'PowaPOS SDK',
            ],
            [
                'id' => 3,
                'name' => 'PowaPIN 100',
            ],
        ];

        foreach ($categories as $category)
            FaqCategory::create($category);

    }

}