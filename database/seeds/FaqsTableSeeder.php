<?php

use Illuminate\Database\Seeder;
use PPDevPortal\Faq;
use PPDevPortal\Product;

class FaqsTableSeeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->loadCsvData('faq.csv', function ($row) {
            DB::table('faqs')->insert($row);
        });

    }

}