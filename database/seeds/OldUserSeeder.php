<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use PPDevPortal\User;
use PPDevPortal\Country;
use Bican\Roles\Models\Role;

class OldUsersSeeder extends DatabaseSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
	{
        $this->loadCsvData('old_users.csv', function ($row) {
            if ($row['country_id']) {
                $row['country_id'] = Country::where('name', $row['country_id'])->first()->id;
            }
            else {
                $row['country_id'] = '233';
            }

            switch ($row['role']) {
                case 'administrator':
                    $role = 3;
                    break;
                case 'powa_administrator':
                    $role = 2;
                    break;
                case 'developer':
                    $role = 1;
                    break;
            }
            // $role = Role::where('name', $row['role']);
            $user = User::create($row);
            $user->attachRole($role);
        });
        
	}

}