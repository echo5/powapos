<?php

use Illuminate\Database\Seeder;
use PPDevPortal\PageCategory;

class PageCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'Develop',
                'slug' => 'develop',
            ],
            [
                'id' => 2,
                'name' => 'Market',
                'slug' => 'market',
            ],
            [
                'id' => 3,
                'name' => 'Engage',
                'slug' => 'engage',
            ],
            [
                'id' => 4,
                'name' => 'Support',
                'slug' => 'support',
            ],
            [
                'id' => 5,
                'name' => 'T-Series',
                'slug' => 't-series',
                'parent_id' => 1,
            ],
            [
                'id' => 6,
                'name' => 'PowaPin',
                'slug' => 'powapin',
                'parent_id' => 1,
            ],
            [
                'id' => 7,
                'name' => 'Guides & Checklists',
                'slug' => 'guides-and-checklists',
                'parent_id' => 1,
            ],
        ];

        foreach ($categories as $category)
            PageCategory::create($category);

    }

}