<?php

use Illuminate\Database\Seeder;
use PPDevPortal\PageTemplate;

class PageTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = [
            [
                'view' => 'content.default',
            ],
            [
                'view' => 'content.menubar_left',
            ],
            [
                'view' => 'content.home',
            ],
            [
                'view' => 'content.develop',
            ],
            [
                'view' => 'content.develop.tseries',
            ],
            [
                'view' => 'content.develop.sdk.sdks',
            ],
            [
                'view' => 'content.develop.demo_apps',
            ],
            [
                'view' => 'content.develop.tutorials.tutorials',
            ],
            [
                'view' => 'content.powapos_tools',
            ],
            [
                'view' => 'content.develop.tseries.supported_tablets',
            ],
            [
                'view' => 'content.develop.tseries.mfi',
            ],
            [
                'view' => 'content.develop.powapin',
            ],
            [
                'view' => 'content.develop.guides',
            ],
            [
                'view' => 'content.market',
            ],
            [
                'view' => 'content.market.logos',
            ],
            [
                'view' => 'content.market.media',
            ],
            [
                'view' => 'content.market.collateral',
            ],
            [
                'view' => 'content.engage',
            ],
            [
                'view' => 'content.engage.partners',
            ],
            [
                'view' => 'content.support',
            ],
            [
                'view' => 'content.support.faq',
            ],
            [
                'view' => 'content.support.proviews',
            ],
        ];

        foreach ($templates as $template)
            PageTemplate::create($template);

    }

}