<?php

use Illuminate\Database\Seeder;
use PPDevPortal\Page;
use PPDevPortal\PageTemplate;
use PPDevPortal\PageCategory;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'title' => 'Home',
                'slug' => '/',
                'content' => '',
                'template' => 'content.home',
            ],
            [
                'title' => 'Develop',
                'slug' => 'develop',
                'content' => 'Develop page content here.',
                'template' => 'content.default',
            ],
                [
                    'title' => 'T-Series',
                    'slug' => 'develop/t-series',
                    'content' => 'The smallest, most elegant POS system available, PowaPOS T25 is a small revolution. But don’t be fooled by its size. PowaPOS T25 takes advantage of rapidly emerging tablet and mobile applications, yet brings all the power, sophistication and security of the most advanced integrated POS systems. It’s the ultimate solution for retailers large and small. For the first time a simple turnkey tablet solution can incorporate all POS peripherals and applications.',
                    'template' => 'content.develop.tseries',
                    'category' => 'Develop',
                ],
                    [
                        'title' => 'T-Series SDK Downloads',
                        'slug' => 'sdk-downloads/t-series',
                        'content' => '<h3 class="widget-title" style="margin:0px 0px 1em">Welcome to the T-Series SDK download portal</h3>

<div class="textwidget" style="margin:0px">
<p style="margin:0px 0px 1em">PowaPOS SDK works seamlessly with your existing smartphone and tablet applications, enabling them to become complete point-of-sale (POS) systems that will run your business more efficiently.</p>

<p style="margin:1em 0px">The PowaPOS SDK provides developers with a single easy to integrate API that works on iOS, Android and Windows and drives the PowaPIN and T-Series POS peripherals and payment devices. Currently supported tablet models can be found on the Latest SDK pages for each operating system.</p>

<p style="margin:1em 0px">Standard interfaces are provided for the integrated printer, barcode scanner, cash drawer and PowaPIN payment devices. In addition, PowaPOS SDK can provide support for a growing list of third party payment terminals, PINpads and other peripherals when required.</p>

<p style="margin:1em 0px 0px">PowaPOS SDK has also been implemented to address the ongoing costs associated with PCI-DSS compliance by establishing an out-of-scope environment for integrated payment applications. By providing a secure and encrypted interface to PCI and EMV compliant devices, PowaPOS SDK allows software providers to save thousands of dollars in ongoing assessment and certification fees.</p>
</div>',
                        'template' => 'content.develop.sdk.sdks',
                        'category' => 'T-Series',
                    ],
                    [
                        'title' => 'Demo Apps',
                        'slug' => 'demo-apps/t-series',
                        'content' => 'Demo Apps page content here.',
                        'template' => 'content.develop.demo_apps',
                        'category' => 'T-Series',
                    ],
                    [
                        'title' => 'Tutorials',
                        'slug' => 'tutorials/t-series',
                        'content' => '',
                        'template' => 'content.develop.tutorials.tutorials',
                        'category' => 'T-Series',
                    ],
                    [
                        'title' => 'PowaPOS Tools',
                        'slug' => 'powapos-tools/t-series',
                        'content' => 'Note: Unless otherwise stated, please open the above application downloads directly from your Android or iOS tablet.',
                        'template' => 'content.powapos_tools',
                        'category' => 'T-Series',
                    ],
                    [
                        'title' => 'Supported Tablets',
                        'slug' => 'develop/t-series/supported-tablets',
                        'content' => 'Supported tablets page content here.',
                        'template' => 'content.develop.tseries',
                        'category' => 'T-Series',
                    ],
                    [
                        'title' => 'MFi Registration',
                        'slug' => 'develop/t-series/mfi',
                        'content' => '',
                        'template' => 'content.develop.tseries.mfi',
                        'category' => 'T-Series',
                    ],
                [
                    'title' => 'PowaPIN',
                    'slug' => 'develop/powapin',
                    'content' => 'The smallest, smartest cardreader on the market. The market is on the move and as more and more customers buy from their smartphones and tablets, PowaPIN is the way for your business to keep one step ahead of them. Small, stylish and perfectly formed, the PowaPIN reader is the ideal companion to your smartphone or tablet.',
                    'template' => 'content.develop.powapin',
                    'category' => 'Develop',
                ],
                    [
                        'title' => 'PowaPIN SDK Downloads',
                        'slug' => 'sdk-downloads/powapin',
                        'content' => '<h3 class="widget-title" style="margin:0px 0px 1em">Welcome to the PowaPIN SDK download portal</h3>

<div class="textwidget" style="margin:0px">
<p style="margin:0px 0px 1em">PowaPOS SDK works seamlessly with your existing smartphone and tablet applications, enabling them to become complete point-of-sale (POS) systems that will run your business more efficiently.</p>

<p style="margin:1em 0px">The PowaPOS SDK provides developers with a single easy to integrate API that works on iOS, Android and Windows and drives the PowaPIN and T-Series POS peripherals and payment devices.</p>

<p style="margin:1em 0px">Standard interfaces are provided for the integrated printer, barcode scanner, cash drawer and PowaPIN payment devices. In addition, PowaPOS SDK can provide support for a growing list of third party payment terminals, PINpads and other peripherals when required.</p>

<p style="margin:1em 0px 0px">PowaPOS SDK has also been implemented to address the ongoing costs associated with PCI-DSS compliance by establishing an out-of-scope environment for integrated payment applications. By providing a secure and encrypted interface to PCI and EMV compliant devices, PowaPOS SDK allows software providers to save thousands of dollars in ongoing assessment and certification fees.</p>
</div>',
                        'template' => 'content.develop.sdk.sdks',
                        'category' => 'PowaPIN',
                    ],
                    [
                        'title' => 'Demo Apps',
                        'slug' => 'demo-apps/powapin',
                        'content' => 'Demo Apps page content here.',
                        'template' => 'content.develop.demo_apps',
                        'category' => 'PowaPIN',
                    ],
                    [
                        'title' => 'Tutorials',
                        'slug' => 'tutorials/powapin',
                        'content' => '',
                        'template' => 'content.develop.tutorials.tutorials',
                        'category' => 'PowaPIN',
                    ],
                [
                    'title' => 'Guides & Checklists',
                    'slug' => 'develop/guides-and-checklists',
                    'content' => '',
                    'template' => 'content.develop.guides',
                    'category' => 'Develop',
                ],
            [
                'title' => 'Market',
                'slug' => 'market',
                'content' => '<p>PowaPOS Marketing is here to support all of your marketing needs. Please feel free to download and use the product images, collateral, logos and other materials in your marketing efforts, according to our brand guidelines.</p><p>If you have a marketing resource need not currently available here, please contact your PowaPOS sales representative or Candace McCaffery, SVP Marketing at <a href="mailto:candacemccaffery@powa.com">candacemccaffery@powa.com</a>.</p>',
                'template' => 'content.market',
            ],
                [
                    'title' => 'Logos and Templates',
                    'slug' => 'market/logos-and-templates',
                    'content' => '[media cat=logos-and-templates col=3]',
                    'template' => 'content.market.logos',
                    'category' => 'Market',
                ],
                [
                    'title' => 'Photography & Video',
                    'slug' => 'market/photography-and-video',
                    'content' => '<h2>T25 Features</h2>[media cat=t25-features col=4]<br/><br/>
                    <h2>T25 Accessories</h2>[media cat=t25-accessories col=4]<br/><br/>
                    <h2>T25 UM Android</h2>[media cat=t25-um-android col=4]<br/><br/>
                    <h2>T25 UM Windows</h2>[media cat=t25-um-windows col=4]<br/><br/>
                    <h2>T25 UM iPad Air</h2>[media cat=t25-um-ipad-air col=4]<br/><br/>
                    <h2>T25 iPad Air</h2>[media cat=t25-ipad-air col=4]<br/><br/>
                    <h2>T25 Tablet and PINPad Mounts</h2>[media cat=tablet-and-mounts col=4]<br/><br/>
                    <h2>PowaPIN</h2>[media cat=powapin col=4]<br/><br/>
                    <h2>Environmental</h2>[media cat=environmental col=4]<br/><br/>
                    <h2>Videos</h2>Paste video embeds here<br/><br/>',
                    'template' => 'content.market.media',
                    'category' => 'Market',
                ],
                [
                    'title' => 'Collateral',
                    'slug' => 'market/collateral',
                    'content' => '<div class="row two-col">
<div class="col-md-6 col-1">
<h3>English (US)</h3>

<p><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/PowaPOS-T25-Sales-Sheet_US-Letter-2pp_DIGITAL-2.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/powapos-sdk-sales-sheet_us-letter-2pp_digital-4.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/powapos-powapin-sales-sheet_us-letter-2pp_digital-2.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/powapos-accessories-sales-sheet_us-letter-2pp_digital-2.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>

<div class="col-md-6 col-2">
<h3>English (UK)</h3>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/PowaPOS-T25-sales-sheet_A4-2pp_DIGITAL.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/PowaPOS-SDK-sales-sheet_A4-2pp_DIGITAL.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/PowaPOS-PowaPIN-sales-sheet_A4-2pp_DIGITAL.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/Powapos-accessories-sales-sheet_a4-2pp_digital.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>
</div>

<p>&nbsp;</p>

<div class="row two-col">
<div class="col-md-6 col-1">
<h3>Spanish (Latin America)</h3>

<p style="margin:0px 0px 1em"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/T25-Inserts_ES_LA-DIGITAL-v2.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/SDK-Inserts_ES_LA-DIGITAL-v2.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/Powapin-Inserts_ES_LA-DIGITAL-v2.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/03/Powapos-accessories-inserts_ES_LA-DIGITAL-v2.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>

<div class="col-md-6 col-2">
<h3>Spanish (Europe)</h3>

<p style="margin:0px 0px 1em"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-T25-sales-sheet_A4-2pp_SP.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-SDK-sales-sheet_A4-2pp_SP.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-PowaPIN-sales-sheet_A4-2pp_SP.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-accessories-sales-sheet_A4-2pp_SP.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>
</div>

<p>&nbsp;</p>

<div class="row two-col">
<div class="col-md-6 col-1">
<h3>French</h3>

<p style="margin:0px 0px 1em"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-T25-sales-sheet_A4-2pp_DIGITAL_French.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-SDK-sales-sheet_A4-2pp_DIGITAL_French.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-PowaPIN-sales-sheet_A4-2pp_DIGITAL_French.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-accessories-sales-sheet_A4-2pp_DIGITAL_French.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>

<div class="col-md-6 col-2">
<h3>Portuguese</h3>

<p style="margin:0px 0px 1em"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-T25-sales-sheet_A4-2pp_DIGITAL_Portuguese.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-SDK-sales-sheet_A4-2pp_DIGITAL_Portuguese.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-PowaPIN-sales-sheet_A4-2pp_DIGITAL_Portuguese.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-accessories-sales-sheet_A4-2pp_DIGITAL_Portuguese.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>
</div>

<p>&nbsp;</p>

<div class="row two-col">
<div class="col-md-6 col-1">
<h3>Chinese (Simplified)</h3>

<p style="margin:0px 0px 1em"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-T25-sales-sheet_A4-2pp_DIGITAL_SIMP_CH.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-SDK-sales-sheet_A4-2pp_DIGITAL_SIMP_CH.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-PowaPIN-sales-sheet_A4-2pp_DIGITAL_SIMP_CH.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-accessories-sales-sheet_A4-2pp_DIGITAL_SIMP_CH.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>

<div class="col-md-6 col-2">
<h3>Chinese (Traditional)</h3>

<p style="margin:0px 0px 1em"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-T25-sales-sheet_A4-2pp_DIGITAL_TRAD_CH.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-SDK-sales-sheet_A4-2pp_DIGITAL_TRAD_CH.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-PowaPIN-sales-sheet_A4-2pp_DIGITAL_TRAD_CH.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2015/04/PowaPOS-accessories-sales-sheet_A4-2pp_DIGITAL_TRAD_CH.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>
</div>

<p>&nbsp;</p>

<div class="row two-col">
<div class="col-md-6 col-1">
<h3>Japanese</h3>

<p style="margin:0px 0px 1em"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/06/PowaPOS-T25-Sales-Sheet-JP.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/06/PowaPOS-SDK-Sales-Sheet-JP.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/06/PowaPOS-PowaPin-Sales-Sheet-JP.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/06/PowaPOS-Accessories-ales-Sheet-JP.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>

<div class="col-md-6 col-2">
<h3>Korean</h3>

<p style="margin:0px 0px 1em"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/08/T25-Inserts_v7-DIGITAL_KOR.pdf" style="margin:0px">PowaPOS T25 Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/08/SDK-Inserts_v7-DIGITAL_KOR.pdf" style="margin:0px">PowaPOS SDK Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/08/Powapin-Inserts_v7-DIGITAL_KOR.pdf" style="margin:0px">PowaPOS PowaPIN Sales Sheet</a></p>

<p style="margin:1em 0px"><a href="https://powaposdeveloper.powa.com/wp-content/uploads/2015/08/powapos-accessories-inserts_DIGITAL_KOR.pdf" style="margin:0px">PowaPOS Accessories Sales Sheet</a></p>
</div>
</div>

<p>&nbsp;</p>',
                    'template' => 'content.market.collateral',
                    'category' => 'Market',
                ],
            [
                'title' => 'Engage',
                'slug' => 'engage',
                'content' => 'Engage page content here.',
                'template' => 'content.engage',
            ],
                [
                    'title' => 'Value added Partner Listings',
                    'slug' => 'engage/partners',
                    'content' => 'Partners page content here.',
                    'template' => 'content.engage.partners',
                    'category' => 'Engage',
                ],
            [
                'title' => 'Support',
                'slug' => 'support',
                'content' => 'Support page content here.',
                'template' => 'content.support',
                'category' => 'Support',
            ],
                [
                    'title' => 'Frequently Asked Questions',
                    'slug' => 'support/faq',
                    'content' => 'If your question isn’t answered here please feel free to <a href="/support/tickets">submit a support ticket</a> and one of our support team will help you.',
                    'template' => 'content.support.faq',
                    'category' => 'Support'
                ],
                [
                    'title' => 'ProViews Videos',
                    'slug' => 'support/proviews-videos',
                    'content' => 'Proviews videos here',
                    'template' => 'content.support.proviews',
                    'category' => 'Support'
                ],
        ];

        foreach ($pages as $page) {
            $new_page = new Page;
            $new_page->title = $page['title'];
            $new_page->slug = $page['slug'];
            $new_page->content = $page['content'];

            if (isset($page['template'])) {
                echo $page['template'];
                $template = PageTemplate::where('view', $page['template'])->firstOrFail();
                $new_page->template()->associate($template);                
            }
            if (isset($page['category'])) {
                $category = PageCategory::where('name', $page['category'])->firstOrFail();
                $new_page->category()->associate($category);
            }
            
            $new_page->save();
        }

    }

}