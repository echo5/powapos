<?php

use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $models = [
            [
                'name' => 'Checklists',
                'slug' => 'checklists',
                'permissions' => true,
            ],
            [
                'name' => 'Demo Apps',
                'slug' => 'demoapps',
                'permissions' => true,
            ],
            [
                'name' => 'FAQs',
                'slug' => 'faqs',
                'permissions' => true,
            ],
            [
                'name' => 'Guides',
                'slug' => 'guides',
                'permissions' => true,
            ],
            [
                'name' => 'Media',
                'slug' => 'media',
                'permissions' => true,
            ],
            [
                'name' => 'Media Categories',
                'slug' => 'mediacategories',
                'permissions' => true,
            ],
            [
                'name' => 'Platforms',
                'slug' => 'platforms',
                'permissions' => true,
            ],
            [
                'name' => 'Product Categories',
                'slug' => 'productcategories',
                'permissions' => true,
            ],
            [
                'name' => 'Products',
                'slug' => 'products',
                'permissions' => true,
            ],
            [
                'name' => 'Roles',
                'slug' => 'roles',
                'permissions' => false,
            ],
            [
                'name' => 'SDKs',
                'slug' => 'sdks',
                'permissions' => true,
            ],
            [
                'name' => 'SDKs',
                'slug' => 'sdkdownloads',
                'permissions' => true,
            ],
            [
                'name' => 'Users',
                'slug' => 'users',
                'permissions' => false,
            ],
            [
                'name' => 'Pages',
                'slug' => 'pages',
                'permissions' => false,
            ],
            [
                'name' => 'Page Categories',
                'slug' => 'pagecategories',
                'permissions' => false,
            ],
            [
                'name' => 'Tutorials',
                'slug' => 'tutorials',
                'permissions' => false,
            ],
            [
                'name' => 'Email Templates',
                'slug' => 'emailtemplates',
                'permissions' => false,
            ],
            [
                'name' => 'User Activity',
                'slug' => 'activity',
                'permissions' => false,
            ],
            [
                'name' => 'PowaPOS Tools',
                'slug' => 'powapostools',
                'permissions' => false,
            ],
        ];

        $methods = [
            [
                'name' => 'Create',
                'slug' => 'create',
                'description' => 'Can create new',
            ],
            [
                'name' => 'Update',
                'slug' => 'update',
                'description' => 'Can update',
            ],
            [
                'name' => 'Delete',
                'slug' => 'delete',
                'description' => 'Can delete',
            ],
            [
                'name' => 'View',
                'slug' => 'view',
                'description' => 'Can view',
            ],
        ];

        foreach ($models as $model) {
            foreach ($methods as $method) {
                $data = [
                    'name' => $method['name'] . ' ' . $model['name'],
                    'slug' => $method['slug'] . '.' . $model['slug'],
                    'description' => $method['description'] . ' ' . $model['name'],
                ];

                $permission = Permission::create($data);

                $super_admin = Role::find(3);
                $super_admin->attachPermission($permission);
                $admin = Role::find(2);
                if ($model['permissions']) {
                    $admin->attachPermission($permission);
                }

            }
        }

    }

}