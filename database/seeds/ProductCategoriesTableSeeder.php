<?php

use Illuminate\Database\Seeder;
use PPDevPortal\ProductCategory;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'T-Series',
                'slug' => 't-series',
            ],
            [
                'id' => 2,
                'name' => 'PowaPIN',
                'slug' => 'powapin',
            ],
        ];

        foreach ($categories as $category)
            ProductCategory::create($category);

    }

}