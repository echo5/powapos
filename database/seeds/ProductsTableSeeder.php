<?php

use Illuminate\Database\Seeder;
use PPDevPortal\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'id' => 1,
                'name' => 'T25',
                'slug' => 't25',
                'category_id' => '1',
            ],
            [
                'id' => 2,
                'name' => 'S10',
                'slug' => 's10',
                'category_id' => '1',
            ],
            [
                'id' => 3,
                'name' => 'PowaPIN 100',
                'slug' => 'powapin-100',
                'category_id' => '2',
            ],
        ];

        foreach ($products as $product)
            Product::create($product);

    }

}