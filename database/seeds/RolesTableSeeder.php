<?php

use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'name' => 'Developer',
                'slug' => 'developer',
                'description' => 'Access to all front-end files',
                'level' => '1',
            ],
            [
                'id' => 2,
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'Access to set areas in admin panel',
                'level' => '10',
            ],
            [
                'id' => 3,
                'name' => 'Super Admin',
                'slug' => 'super_admin',
                'description' => 'Access to everything in admin panel',
                'level' => '20',
            ],
        ];

        foreach ($roles as $role)
            Role::create($role);

    }

}