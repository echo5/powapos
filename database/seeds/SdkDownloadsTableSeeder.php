<?php

use Illuminate\Database\Seeder;
use PPDevPortal\SdkDownload;

class SdkDownloadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sdk_downloads = [
            [
                'name' => 'T25 Android Release 1.4',
                'description' => 'Complete SDK Package.',
                'file' => 'PowaPOS-SDK-Android-1.4-Release.zip',
                'sdk_id' => '1',
            ],
            [
                'name' => 'T25 iOS Release 1.4',
                'description' => 'Android PowaPOS 1.4 Library',
                'file' => 'PowaPOS-SDK-Android-1.4-library',
                'sdk_id' => '1',
            ],
        ];

        foreach ($sdk_downloads as $download) {
            SdkDownload::create($download);
        }

    }

}