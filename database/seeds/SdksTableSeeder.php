<?php

use Illuminate\Database\Seeder;
use PPDevPortal\Sdk;

class SdksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sdks = [
            [
                'label' => 'Latest Android SDK',
                'name' => 'T25 Android Release 1.4',
                'description' => '<p><strong>Summary:</strong></p>
<p>This SDK release includes support for the production hardware and firmware. The supported T-Series peripherals are: Cash Drawer, Printer, Rotation Sensor, USB Ports and Serial Scanner.</p>
<p>The available development interfaces in this release are native Android and PhoneGap.</p>
<p><strong>Summary of SDK changes:</strong></p>
<ul>
<li>Included support for remote firmware update</li>
<li>Included control over the scanner “auto scan” feature</li>
<li>Include control over the scanner beep</li>
</ul>
<p>A simulation SDK is also included in this release. This simulation SDK offers the same API as the generic SDK but it emulates the use of a real T-Series hardware (i.e. it can be used by applications without an existing T-Series).</p>
<p><strong>This release includes:</strong></p>
<ul>
<li>PowaPOS SDK libraries</li>
<li>TSeries firmware 1008</li>
<li>TSeries bootloader 8021</li>
<li>Native Sample Application</li>
<li>PhoneGap plugin</li>
<li>PhoneGap Sample Application</li>
<li>Update Utility</li>
<li>Functional Documentation V1.15</li>
<li>SDK User Guide V1.5</li>
<li>Simulation SDK</li>
<li>Update Utility</li>
</ul>
<p><strong>Known Issues:</strong></p>
<ul>
<li>Issue with printing ‘€’ using&nbsp;printText .</li>
<li>When printing images with large dark areas at high speeds while the printer is cold, may result in a “voltage error” being reported by the SDK.</li>
<li>The print bar code feature supports the following modes: ODE_128_CODE_A, CODE_128_CODE_B, CODE_39JAN_EAN_13, JAN_EAN_8, UPC_A, UPC_E</li>
<li>Setting the bar code parameters has no effect on the bar widths (barCodeNarrowWidth and BarcodeWideWidth)</li>
<li>When turning on and off the bluetooth while using the Scanner, occasionally the Scanner connection may be lost and a re-pair may be required.</li>
<li>On some Android tablets, when the battery level is too low, the T25 may not be able to charge it.</li>
<li>Some Android tablets will not power on when plugged into the T25 and the battery level is too low.</li>
<li>Occasionally, when the Scanner runs out of battery the pairing information may be lost.</li>
<li>Printing QR code with single quotes (‘) my not work when using PhoneGap plugin.</li>
</ul>
<p><strong>Fixed Known Issues in this release:</strong></p>
<ul>
<li>Opening the paper lid while printing may cause garbage to be printed</li>
</ul>
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'label' => 'Latest iOS SDK',
                'name' => 'T25 iOS Release 1.4',
                'description' => '<p><strong>Summary</strong></p>
<p>This SDK release includes support for the production hardware and firmware. The supported T-Series peripherals are: Cash Drawer, Printer, Rotation Sensor, USB Ports and Serial Scanner.</p>
<p><strong>Changes</strong></p>
<ul>
<li>Support for remote firmware update</li>
<li>Control over the scanner “auto scan” feature</li>
<li>Control over the scanner beep</li>
<li>Additional signatures for the printBarCode and printQRCode methods to allow non-ASCII barcode types.</li>
</ul>
<p>A simulation SDK for the most used functionalities is also included in this release. This simulation SDK offers the same API as the generic SDK but it emulates the use of a real T-Series hardware (i.e. it can be used by applications without an existing T-Series).<br>
This release is specific for iOS mobile devices.</p>
<p><strong>Includes</strong></p>
<ul>
<li>PowaPOS SDK libraries</li>
<li>TSeries firmware 1008</li>
<li>TSeries bootloader 8021</li>
<li>Native Sample Application</li>
<li>PhoneGap Plug-in</li>
<li>PhoneGap Sample</li>
<li>Update Utility</li>
<li>Functional Documentation V1.15</li>
<li>SDK User Guide V1.3</li>
<li>Simulation SDK</li>
</ul>
<p><strong>Known Issues</strong></p>
<ul>
<li>Sending data packets to USB larger than 300 bytes may cause a print job to stop (PSIQAI-196)</li>
<li>Opening and closing the printer cover during printing may cause unexpected data to be printed.</li>
<li>Scanning barcodes type 128 C is not supported in this release.</li>
<li>HTML documentation for Simulation SDK lists&nbsp;‘id’ and ‘property’ entries.</li>
</ul>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'label' => 'Latest Windows SDK',
                'name' => 'T25 Windows .NET Release 1.4',
                'description' => '<p><strong>Summary</strong></p>
<p>This SDK release includes support for the production hardware and firmware. The supported T-Series peripherals are: Cash Drawer, Printer, Rotation Sensor, USB Ports and Serial Scanner.</p>
<p>This release is specific for developing desktop applications for .NET Windows PC and tablet devices.</p>
<ul>
<li>Added logic to check version numbers and update the T25 firmware remotely from a PowaPOS FTP site. (PSFW-10)</li>
<li>Added logic to include an RGB average threshold value to image printing functions. (PSFW-11)</li>
<li>Add logic to implement call back events for T25 peripheral states. (PSFW-9)</li>
<li>Add logic to provide MCU connection state information to the application. (PSFW-8)</li>
<li>Add logic to implement control over the scanner auto scan feature. (PSFW-12)</li>
<li>Add logic to implement control over the scanner beep functionality. (PSFW-14)</li>
</ul>
<p><strong>Full SDK Release Contents</strong></p>
<ul>
<li>PowaPOS SDK libraries</li>
<li>PowaPOS T25 firmware 1008</li>
<li>PowaPOS T25boot loader8021</li>
<li>Native Sample Application</li>
<li>Functional DocumentationV1.15</li>
<li>SDK User Guide V1.3</li>
<li>Simulation SDK</li>
</ul>
<p><strong>Fixed Known Issues</strong></p>
<ul>
<li>Fixed issue where the T25 would hang during iOS negotiation and fail to initialize on some windows devices. (PPSWQI-9)</li>
<li>Fixed an issue where the Euro symbol was not printing correctly on receipts. (PPS-34)</li>
<li>Scan Printed QR codes.&nbsp;(PPSWQI-10)</li>
<li>Corrected CheckMCUAvailableFirmware().&nbsp;(PPSWQI-11)</li>
<li>Fixed invalid port for USB attach/detach events.&nbsp;(PPSWQI-12)</li>
<li>Added Scanner and MCU connection state events to the native sample app.&nbsp;(PPSWQI-13)</li>
<li>Fixed issue where Latin_Japanese_8bit QR Codes were not printing correctly from ‘String’.&nbsp;(PPSWQI-15)</li>
<li>Removed Firmware update button and FTP url, user id and password from Native sample app.&nbsp;(PPSWQI-16)</li>
<li>Copied latest version of Native simulation app to the release.&nbsp;(PPSWQI-17)</li>
<li>Modified the application projects to copy image resource files to output directory.&nbsp;(PPSWQI-18)</li>
</ul>
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'label' => 'Latest Windows Store SDK',
                'name' => 'T25 .NET for Windows Store Release 1.0 (Alpha Release)',
                'description' => '<p><strong>Summary</strong></p>
<p>This SDK release includes support for the production hardware and firmware. The supported T-Series peripherals are: Cash Drawer, Printer, Rotation Sensor, USB Ports and Serial Scanner.</p>
<ul>
<li>This release is ported from the PowaPOS SDK Release 1.0 RC3 desktop version and includes all the functionality contained therein including image printing and receipt builder.</li>
<li>Modified the USB logic so that USB port numbers in the SDK will match the physical port numbers on the T25.</li>
</ul>
<p>A simulation SDK is also included in this release. This simulation SDK offers the same API as the generic SDK but it emulates the use of a real T-Series hardware (i.e. it can be used by applications without an existing T-Series).<br>
Assumptions • This release is specific for developing store applications for Windows PC and tablet devices.</p>
<p><strong>Full SDK Release Contents</strong></p>
<ul>
<li>PowaPOS SDK libraries for Store Apps</li>
<li>PowaPOS T25 firmware 1008</li>
<li>PowaPOS T25 Driver for Windows&nbsp;1.0.0.1</li>
<li>PowaPOS T25 bootloader 8021</li>
<li>PowaPOSSample Application</li>
<li>PowaPOS Simulation Application</li>
<li>Functional Documentation V1.12</li>
<li>SDK User Guide V1.2</li>
<li>SDK Windows Store App Supplement</li>
</ul>
<p><strong>Existing Known Issues</strong></p>
<ul>
<li>Firmware and boot loader update require the image files to be renamed to ‘.txt’ files. This release will not work with ‘.bin’ files.</li>
<li>The T25 must be connected to the PC or tablet during the first execution of an application. Otherwise the PC will not issue the prompt for USB device permission. As a workaround the sample application issues a connection prompt when this happens.</li>
<li>On the Toshiba tablet and using the sample application and powered USB cable the T25 intermittently fails to reactivate after cable disconnect.</li>
<li>The barcode CODABAR can’t be scanned. (PPSWQI-19)</li>
<li>The QR codes can’t be scanned. (PPSWQI-20)</li>
</ul>
<p><strong>Fixed Known Issues</strong></p>
<ul>
<li>Added logic to change the SDK USB port numbers to match the USB numbers on the T25 hardware.</li>
<li>Fixed an issue where the Euro symbol was not printing correctly on receipts. (PPS-34)</li>
</ul>
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'label' => 'Latest Firmware',
                'name' => 'T25 .NET for Windows Store Release 1.0 (Alpha Release)',
                'description' => 'Firmware Version 1.0.0.8',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'label' => 'Latest Android SDK',
                'name' => 'PowaPIN Android Release 1.0',
                'description' => '<p>This SDK release allows PowaPIN processing of Sale, Refund, Read Card and Cancellation. It also allows configuration updates.<span id="more-418"></span></p>
<p><span style="color: #333333; font-size: medium;">In this SDK release, the following features were improved:</span></p>
<ul>
<li>Recovery process</li>
<li>Data elements on the PowaPIN Payloads</li>
<li>Updated Sample App</li>
<li>Information provided by the SDK regarding the transaction outcome.</li>
</ul>
<p>Several minor bugs were corrected.</p>
<p><b>System Requirements</b></p>
<ul>
<li>Android OS 2.2 or higher</li>
</ul>
<p><b>Firmware Requirements&nbsp;</b></p>
<ul>
<li>PowaPIN – Firmware v.10.2</li>
</ul>
<p><b>Assumptions:</b></p>
<ul>
<li>This release is specific for Android mobile devices.</li>
<li>The transaction scenarios include exception handling “state machine” as of this release.</li>
<li>The messages sent by the PowaPIN are enciphered.</li>
</ul>
<p><b>Release deliverables:&nbsp;</b></p>
<ul>
<li>PowaPOS SDK Frameworks.</li>
<li>PowaPIN firmware V1.10.2</li>
<li>Sample Application</li>
<li>Functional Documentation</li>
</ul>
<p><b>Fixed Known Issues:</b></p>
<ul>
<li><span style="color: #333333; font-size: medium;">In some situations, the SDK will raise an unexpected timeout event when the card is not removed at the end of the transaction after a few seconds</span></li>
</ul>
<div><span style="color: #4d81c2; font-size: medium;">&nbsp;</span></div>
<div><b>Known Issues in this release:</b></div>
<ul>
<li>The EMV Application Selection functionality (when there is more than one common application to choose from) doesn’t make the correct use of the Cancel Key (treating it as “Next”).</li>
<li>While processing the Refund operation, if the ICC is removed before the authorisation response is received, the transaction is approved.</li>
<li>The SDK does not throw exceptions to the mobile Application for all the misuse situations.</li>
<li>When using the Initallise method without specifying an update version number, the SDK may become unresponsive.</li>
<li>Occasionally, a communication error can occur while the PowaPIN is receiving an authorisation response and the transaction is reverse.</li>
</ul>',
                'product_id' => '3',
                'platform_id' => '1',
            ],
            [
                'label' => 'Latest iOS SDK',
                'name' => 'PowaPIN iOS Release 1.1',
                'description' => '<p>This SDK release allows PowaPIN processing of Sale, Refund, Read Card and Cancellation. It also allows configuration updates.<br>
The available development interfaces in this release are native iOS .</p>
<p>In this SDK release, the following features were improved:</p>
<ul>
<li>SDK stability</li>
<li>Data elements on the PowaPIN Payloads</li>
<li>Updated Sample Apps</li>
<li>A fix was included in the SelectPED method.</li>
<li>A fix was included in the remote software update process.</li>
<li>A fix was included regarding PowaPIN re-connection</li>
<li>Several minor bugs were corrected both on the Firmware and SDK.</li>
</ul>
<p><strong>Included</strong></p>
<ul>
<li>PowaPIN SDK Frameworks</li>
<li>PowaPIN firmware V1.11.16 – using an iOS Software update App</li>
<li>Sample Applications</li>
<li>Functional Documentation V1.10</li>
<li>User Guide V1.1</li>
</ul>
<p><strong>Known Issues in this release</strong></p>
<ul>
<li>No exception is thrown by the SDK when starting 2 consecutive transactions</li>
<li>In some situations a PROCESS_NOT_ALLOWED exception may triggered after a call to InitialisePED</li>
<li>When the ICC is removed from the reader during the transaction, the prompt messages reported by the SDK and presented on the PowaPIN display guide the user to remove the card</li>
<li>When disconnecting the PowaPIN during a transaction, the SDK triggers a disconnect event but no transactionError</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>',
                'product_id' => '3',
                'platform_id' => '2',
            ],
            [
                'label' => 'Latest Windows SDK',
                'name' => 'PowaPIN Windows (.NET) Release 1.0',
                'description' => '<p><strong>Summary</strong></p>
<p>This SDK release allows PowaPIN processing of Sale, Refund, Read Card and Cancellation. It also allows configuration updates.</p>
<p>This release is specific for Windows devices.<br>
<strong>Included</strong></p>
<ul>
<li>PowaPIN SDK libraries</li>
<li>PowaPIN firmware V1.11.16 – using an iOS software update app</li>
<li>PowaPIN Sample App source code and executables.</li>
<li>PowaPIN Server abstraction demo library source and binary.</li>
<li>PowaPIN Server abstraction gatewaylibrary source and binary.</li>
<li>Functional documentation V1.10</li>
<li>HTML Documentation</li>
</ul>
<p><strong>Known Issues </strong></p>
<ul>
<li>The MAC address is not returned when the PED information is requested.</li>
</ul>
<p>&nbsp;</p>',
                'product_id' => '3',
                'platform_id' => '3',
            ],
            [
                'label' => 'Latest API Specifications',
                'name' => 'PowaPIN API Specifications',
                'description' => 'This API reference manual is the blueprint for the PowaPIN SDK API. It provides an outline of functional specifications and requirements detailing how the application API interacts with connecting PowaPIN Devices and payment platforms.',
                'product_id' => '3',
                'platform_id' => '1',
            ],
        ];

        foreach ($sdks as $sdk) {
            $sdk['slug'] = str_slug($sdk['label']);
            Sdk::create($sdk);
        }

    }

}