<?php

use Illuminate\Database\Seeder;
use PPDevPortal\Tutorial;
use Illuminate\Support\Str;

class TutorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tutorials = [
            [
                'name' => 'MFi Certification Process',
                'content' => '<p>1. Down load the PowaPOS iOS SDK from the Developer Portal.</p>
<p>&nbsp;</p>
<p>2. Enroll in Apple iOS Developer Program.</p>
<p>&nbsp;</p>
<p>3. After you develop your App, complete the&nbsp;<a href="http://powaposdeveloper.powa.com/?page_id=156" target="_blank"><u><b>Application Information Sheet</b></u></a>&nbsp;located in the Apple MFI section of the developer portal.</p>
<p>&nbsp;</p>
<p>4. PowaPOS sends your Application Information Sheet to Apple.</p>
<p>&nbsp;</p>
<p>5. After Apple notifies PowaPOS the Application Information Sheet has been reviewed, PowaPOS will notify you, usually within 5 business days.</p>
<p>&nbsp;</p>
<p>6. Submit your App to the Apple App Store and enter the MFI PPID in the app metadata review notes field.</p>
<p>Protocol name(s) that are supported by our devices:</p>
<p>T25: com.powa.mcu</p>
<p>S10: com.powa.s10</p>
<p>PPID for the approved MFI devices</p>
<p>T25 PPID: 146124-0006</p>
<p>S10 PPID: 146124-0009</p>
<p>&nbsp;</p>
<p>7. Apple reviews and posts the App to the App Store within 1 to 2 weeks if Apple has no other concerns regarding&nbsp;the&nbsp;App. &nbsp;Apple will notify you directly when this process is completed.</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Firmware check and update sample code',
                'content' => '<p>Below please find the windows code that can be used to check the current version of the firmware on a T25 and perform a firmware update if the T25 is not on the most recent version of the firmware.</p>
<p>Please contact your technical account manager if you have any questions or issues implementing the firmware update methods.</p>
<p>Update button click event:</p>
<p>private void btnUpdate_Click(object sender, RoutedEventArgs e)<br>
{<br>
String newVersion = “1.0.0.8”;<br>
List&lt;Powa.Win.Sdk.PowaPOS.Core.DataObjects.PowaDeviceInfo&gt; info = powaPOS.GetMCUInformation();<br>
foreach (var item in info)<br>
{<br>
if(item.DeviceType.Equals(PowaPOSEnums.DeviceInfoType.MCU))<br>
{<br>
int newVersionNumber = Convert.ToInt32(newVersion.Replace(“.”, “”));<br>
int currentVersionNumber = Convert.ToInt32(item.FirmwareVersion.Replace(“.”, “”));<br>
if(newVersionNumber &lt;= currentVersionNumber)</p>
<p>{ // report failure Console.WriteLine(“Update not required …”); return; }</p>
<p>try</p>
<p>{ var fileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @”Update\relapp1008.bin”); var bytes = File.ReadAllBytes(fileName); powaPOS.UpdateMCUFirmware(bytes); }</p>
<p>catch (Exception ex)</p>
<p>{ // report error Console.WriteLine(ex.ToString()); }</p>
<p>return;<br>
}<br>
}<br>
}</p>
<p>Update callback handlers:<br>
private class PowaPOSCallbackClient : PowaPOSCallback<br>
{<br>
private MainWindow parent;<br>
public PowaPOSCallbackClient(MainWindow parent)</p>
<p>{ this.parent = parent; }</p>
<p>public override void OnMCUFirmwareUpdateStarted()</p>
<p>{/* add code here for update start event */ }</p>
<p>public override void OnMCUFirmwareUpdateProgress(int progress)</p>
<p>{/* add code here for update progress event */ }</p>
<p>public override void OnMCUFirmwareUpdateFinished()</p>
<p>{/* add code here for update finish event */ }</p>
<p>}</p>',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'name' => 'Firmware check and update sample code',
                'content' => '<p>Below please find the Android&nbsp;code that can be used to check the current version of the firmware on a T25 and perform a firmware update if the T25 is not on the most recent version of the firmware.</p>
<p>Please contact your technical account manager if you have any questions or issues implementing the firmware update methods.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f097465a142642662580" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">public void mcuOnUpdate() {

    try {

        String newVersion = "1.0.0.8";&nbsp; // The firmware version that should be on the T25 1.0.0.8 or higher

        List &lt; PowaDeviceInfo &gt; info = powaPOS.getMCUInformation();&nbsp; // Request the current firmware version on the T25

        for (PowaDeviceInfo deviceInfo: info) {

            if (deviceInfo.deviceType.equals(PowaPOSEnums.DeviceInfoType.MCU)) {

                int newVersionNumber = Integer.parseInt(newVersion.replace(".", ""));

                int currentVersionNumber = Integer.parseInt(deviceInfo.firmwareVersion.replace(".", ""));

                if (newVersionNumber &lt;= currentVersionNumber)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; // Compares the current firmware version on the T25 with the desired firmware version.

                {
                    /* Report failure return; */
                }

                AssetManager assetManager = this.getAssets();

                try {
                    InputStream istr = assetManager.open("powapossdk/update/relapp1008.bin");&nbsp; // Gets the desired firmware version .bin file from local storage

                    byte[] data = IOUtils.toByteArray(istr);
                    powaPOS.updateMCUFirmware(data); // Performs the firmware update

                } catch (Exception e) { /* Report failure return; */ }

                //Report success

                return;

            }

        }
    }
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f097465a142642662580-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-2">2</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-4">4</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-6">6</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-8">8</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-10">10</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-12">12</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-13">13</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-14">14</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-15">15</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-16">16</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-17">17</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-18">18</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-19">19</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-20">20</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-21">21</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-22">22</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-23">23</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-24">24</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-25">25</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-26">26</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-27">27</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-28">28</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-29">29</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-30">30</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-31">31</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-32">32</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-33">33</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-34">34</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-35">35</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-36">36</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-37">37</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-38">38</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-39">39</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097465a142642662580-40">40</div><div class="crayon-num" data-line="crayon-55f097465a142642662580-41">41</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f097465a142642662580-1"><span class="crayon-m">public</span><span class="crayon-h"> </span><span class="crayon-t">void</span><span class="crayon-h"> </span><span class="crayon-e">mcuOnUpdate</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-2">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-3"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">try</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-4">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">String</span><span class="crayon-h"> </span><span class="crayon-v">newVersion</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-s">"1.0.0.8"</span><span class="crayon-sy">;</span>&nbsp;<span class="crayon-h"> </span><span class="crayon-c">// The firmware version that should be on the T25 1.0.0.8 or higher</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-6">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-7"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-v">List</span><span class="crayon-h"> </span><span class="crayon-o">&lt;</span><span class="crayon-h"> </span><span class="crayon-v">PowaDeviceInfo</span><span class="crayon-h"> </span><span class="crayon-o">&gt;</span><span class="crayon-h"> </span><span class="crayon-v">info</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">getMCUInformation</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span>&nbsp;<span class="crayon-h"> </span><span class="crayon-c">// Request the current firmware version on the T25</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-8">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-9"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">for</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-e">PowaDeviceInfo </span><span class="crayon-v">deviceInfo</span><span class="crayon-o">:</span><span class="crayon-h"> </span><span class="crayon-v">info</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-10">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-11"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">if</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-v">deviceInfo</span><span class="crayon-sy">.</span><span class="crayon-v">deviceType</span><span class="crayon-sy">.</span><span class="crayon-e">equals</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-v">DeviceInfoType</span><span class="crayon-sy">.</span><span class="crayon-v">MCU</span><span class="crayon-sy">)</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-12">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-13"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">int</span><span class="crayon-h"> </span><span class="crayon-v">newVersionNumber</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-t">Integer</span><span class="crayon-sy">.</span><span class="crayon-e">parseInt</span><span class="crayon-sy">(</span><span class="crayon-v">newVersion</span><span class="crayon-sy">.</span><span class="crayon-e">replace</span><span class="crayon-sy">(</span><span class="crayon-s">"."</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-s">""</span><span class="crayon-sy">)</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-14">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-15"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">int</span><span class="crayon-h"> </span><span class="crayon-v">currentVersionNumber</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-t">Integer</span><span class="crayon-sy">.</span><span class="crayon-e">parseInt</span><span class="crayon-sy">(</span><span class="crayon-v">deviceInfo</span><span class="crayon-sy">.</span><span class="crayon-v">firmwareVersion</span><span class="crayon-sy">.</span><span class="crayon-e">replace</span><span class="crayon-sy">(</span><span class="crayon-s">"."</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-s">""</span><span class="crayon-sy">)</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-16">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-17"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">if</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-v">newVersionNumber</span><span class="crayon-h"> </span><span class="crayon-o">&lt;=</span><span class="crayon-h"> </span><span class="crayon-v">currentVersionNumber</span><span class="crayon-sy">)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-h"> </span><span class="crayon-c">// Compares the current firmware version on the T25 with the desired firmware version.</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-18">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-19"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-20"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-c">/* Report failure return; */</span></div><div class="crayon-line" id="crayon-55f097465a142642662580-21"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-22">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-23"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-e">AssetManager </span><span class="crayon-v">assetManager</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">this</span><span class="crayon-sy">.</span><span class="crayon-e">getAssets</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-24">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-25"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">try</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-26"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-e">InputStream </span><span class="crayon-v">istr</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-v">assetManager</span><span class="crayon-sy">.</span><span class="crayon-e">open</span><span class="crayon-sy">(</span><span class="crayon-s">"powapossdk/update/relapp1008.bin"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span>&nbsp;<span class="crayon-h"> </span><span class="crayon-c">// Gets the desired firmware version .bin file from local storage</span></div><div class="crayon-line" id="crayon-55f097465a142642662580-27">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-28"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">byte</span><span class="crayon-sy">[</span><span class="crayon-sy">]</span><span class="crayon-h"> </span><span class="crayon-v">data</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-v">IOUtils</span><span class="crayon-sy">.</span><span class="crayon-e">toByteArray</span><span class="crayon-sy">(</span><span class="crayon-v">istr</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f097465a142642662580-29"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">updateMCUFirmware</span><span class="crayon-sy">(</span><span class="crayon-v">data</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">// Performs the firmware update</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-30">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-31"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span><span class="crayon-h"> </span><span class="crayon-st">catch</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-i">Exception</span><span class="crayon-h"> </span><span class="crayon-v">e</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span><span class="crayon-h"> </span><span class="crayon-c">/* Report failure return; */</span><span class="crayon-h"> </span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-32">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-33"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-c">//Report success</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-34">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-35"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">return</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-36">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-37"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-38">&nbsp;</div><div class="crayon-line" id="crayon-55f097465a142642662580-39"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097465a142642662580-40"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line" id="crayon-55f097465a142642662580-41"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0056 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'name' => 'Firmware check and update sample code',
                'content' => '<p>Below please find the iOS&nbsp;code that can be used to check the current version of the firmware on a T25 and perform a firmware update if the T25 is not on the most recent version of the firmware.</p>
<p>Please contact your technical account manager if you have any questions or issues implementing the firmware update methods.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f0977bca75a708089427" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">-(void)startFirmwareUpdate {
  NSString *fileName = [self.firmwareUpdate objectForKey:@"firmwareName"];
  NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"bin"]];
  [self.tseries startFirmwareUpdateWithData:data];
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f0977bca75a708089427-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f0977bca75a708089427-2">2</div><div class="crayon-num" data-line="crayon-55f0977bca75a708089427-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f0977bca75a708089427-4">4</div><div class="crayon-num" data-line="crayon-55f0977bca75a708089427-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f0977bca75a708089427-1"><span class="crayon-o">-</span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e">startFirmwareUpdate</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f0977bca75a708089427-2"><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-e ">NSString *</span><span class="crayon-v">fileName</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-e">firmwareUpdate </span><span class="crayon-v">objectForKey</span><span class="crayon-o">:</span><span class="crayon-sy">@</span><span class="crayon-s">"firmwareName"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f0977bca75a708089427-3"><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-e ">NSData *</span><span class="crayon-v">data</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-e">NSData </span><span class="crayon-v">dataWithContentsOfFile</span><span class="crayon-o">:</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-e">NSBundle </span><span class="crayon-v">mainBundle</span><span class="crayon-sy">]</span><span class="crayon-h"> </span><span class="crayon-v">pathForResource</span><span class="crayon-o">:</span><span class="crayon-e">fileName </span><span class="crayon-v">ofType</span><span class="crayon-o">:</span><span class="crayon-sy">@</span><span class="crayon-s">"bin"</span><span class="crayon-sy">]</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f0977bca75a708089427-4"><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-e">tseries </span><span class="crayon-v">startFirmwareUpdateWithData</span><span class="crayon-o">:</span><span class="crayon-v">data</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f0977bca75a708089427-5"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0015 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Windows Tablet Support',
                'content' => '<p>When using the PowaPOS SDK for Windows with the T-Series, we recommend and support the&nbsp;&nbsp;<a title="Toshiba Encore 2 WT10-A32 Tablet" href="http://www.toshiba.com/us/tablets/encore2/10/WT10-A32" target="_blank">Toshiba&nbsp;Encore 2 WT10-A32 Tablet</a>.</p>
<p>A Micro USB to USB OTG Host Adapter M/F is also required, such as <a title="this adapter offered by StarTech" href="http://www.startech.com/Cables/USB-2.0/USB-Adapters/5in-Micro-USB-to-USB-OTG-Host-Adapter-Male-to-Female~UUSBOTG" target="_blank">this adapter&nbsp;offered by&nbsp;StarTech</a>.</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Using the S10 Scanner',
                'content' => '<p>The following assumes that you have already set up your project with the PowaPOS SDK library and&nbsp;have already instantiated the&nbsp;PowaPOS object as described in the&nbsp;Initializing the PowaPOS T25 tutorial.</p>
<p><strong>Initializing the Scanner</strong></p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f097f9e8788962609198" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaS10Scanner scanner = new PowaS10Scanner();
powaPOS.AddPeripheral(scanner);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f097f9e8788962609198-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8788962609198-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f097f9e8788962609198-1"><span class="crayon-e">PowaS10Scanner </span><span class="crayon-v">scanner</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaS10Scanner</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8788962609198-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">AddPeripheral</span><span class="crayon-sy">(</span><span class="crayon-v">scanner</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0006 seconds] -->
<p><strong>&nbsp;Receiving the initialization events</strong></p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f097f9e8792254496158" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaS10Scanner &nbsp;scanner &nbsp;= &nbsp;new &nbsp;PowaS10Scanner();
powaPOS.AddPeripheral(scanner);

Receiving &nbsp;the &nbsp;initialization &nbsp;events

private &nbsp;PowaPOSCallbackClient &nbsp;powaPOSCallbacks;

class &nbsp;PowaPOSCallbackClient &nbsp;: &nbsp;PowaPOSCallback
{
&nbsp;&nbsp;&nbsp;private &nbsp;OwnerClass &nbsp;owner;
&nbsp;&nbsp;&nbsp;public &nbsp;PowaPOSCallbackClient &nbsp;(OwnerClass &nbsp;owner)
&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.owner &nbsp;= &nbsp;owner;
&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;public &nbsp;override &nbsp;void &nbsp;OnMCUInitialized(PowaPOSEnums.InitializedResult &nbsp;result)
&nbsp;&nbsp;&nbsp;{ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;//override &nbsp;all &nbsp;the &nbsp;callback &nbsp;methods
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-2">2</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-4">4</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-6">6</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-8">8</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-10">10</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-12">12</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-13">13</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-14">14</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-15">15</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-16">16</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-17">17</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-18">18</div><div class="crayon-num" data-line="crayon-55f097f9e8792254496158-19">19</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8792254496158-20">20</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f097f9e8792254496158-1"><span class="crayon-i">PowaS10Scanner</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">scanner</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-o">=</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-r">new</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-e">PowaS10Scanner</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">AddPeripheral</span><span class="crayon-sy">(</span><span class="crayon-v">scanner</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-3">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-4"><span class="crayon-i">Receiving</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">the</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">initialization</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-e">events</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-5">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-6"><span class="crayon-m">private</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">PowaPOSCallbackClient</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-v">powaPOSCallbacks</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-7">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-8"><span class="crayon-t">class</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">PowaPOSCallbackClient</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-o">:</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-e">PowaPOSCallback</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-9"><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-10">&nbsp;&nbsp;&nbsp;<span class="crayon-m">private</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">OwnerClass</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-v">owner</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-11">&nbsp;&nbsp;&nbsp;<span class="crayon-m">public</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-i">PowaPOSCallbackClient</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-sy">(</span><span class="crayon-i">OwnerClass</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-v">owner</span><span class="crayon-sy">)</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-12">&nbsp;&nbsp;&nbsp;<span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-13">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-r">this</span><span class="crayon-sy">.</span><span class="crayon-i">owner</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-o">=</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-v">owner</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-14">&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-15">&nbsp;&nbsp;&nbsp;<span class="crayon-m">public</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-m">override</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-t">void</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-e">OnMCUInitialized</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-i">InitializedResult</span><span class="crayon-h"> </span>&nbsp;<span class="crayon-v">result</span><span class="crayon-sy">)</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-16">&nbsp;&nbsp;&nbsp;<span class="crayon-sy">{</span><span class="crayon-h"> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="crayon-line" id="crayon-55f097f9e8792254496158-17">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-18">&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span></div><div class="crayon-line" id="crayon-55f097f9e8792254496158-19">&nbsp;&nbsp;&nbsp;<span class="crayon-c">//override &nbsp;all &nbsp;the &nbsp;callback &nbsp;methods</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8792254496158-20"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0025 seconds] -->
<p>OwnerClass : is a generic representation of the class which contains the callbacks instance.</p>
<p><strong>Write code to handle scanned barcodes</strong></p>
<p>The following code illustrates how to handle events regarding the scanner barcode reads:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f097f9e8798491015080" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">class PowaPOSCallbackClient : PowaPOSCallback
{


&nbsp; &nbsp; public override void OnScannerRead(string data) { 
&nbsp; &nbsp; //do something with the data


&nbsp; &nbsp; }
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f097f9e8798491015080-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8798491015080-2">2</div><div class="crayon-num" data-line="crayon-55f097f9e8798491015080-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8798491015080-4">4</div><div class="crayon-num" data-line="crayon-55f097f9e8798491015080-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8798491015080-6">6</div><div class="crayon-num" data-line="crayon-55f097f9e8798491015080-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8798491015080-8">8</div><div class="crayon-num" data-line="crayon-55f097f9e8798491015080-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f097f9e8798491015080-10">10</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f097f9e8798491015080-1"><span class="crayon-t">class</span><span class="crayon-h"> </span><span class="crayon-v">PowaPOSCallbackClient</span><span class="crayon-h"> </span><span class="crayon-o">:</span><span class="crayon-h"> </span><span class="crayon-e">PowaPOSCallback</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8798491015080-2"><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f097f9e8798491015080-3">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8798491015080-4">&nbsp;</div><div class="crayon-line" id="crayon-55f097f9e8798491015080-5">&nbsp;<span class="crayon-h"> </span>&nbsp;<span class="crayon-h"> </span><span class="crayon-m">public</span><span class="crayon-h"> </span><span class="crayon-m">override</span><span class="crayon-h"> </span><span class="crayon-t">void</span><span class="crayon-h"> </span><span class="crayon-e">OnScannerRead</span><span class="crayon-sy">(</span><span class="crayon-t">string</span><span class="crayon-h"> </span><span class="crayon-v">data</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span><span class="crayon-h"> </span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8798491015080-6">&nbsp;<span class="crayon-h"> </span>&nbsp;<span class="crayon-h"> </span><span class="crayon-c">//do something with the data</span></div><div class="crayon-line" id="crayon-55f097f9e8798491015080-7">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8798491015080-8">&nbsp;</div><div class="crayon-line" id="crayon-55f097f9e8798491015080-9">&nbsp;<span class="crayon-h"> </span>&nbsp;<span class="crayon-h"> </span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f097f9e8798491015080-10"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0009 seconds] -->
<p>&nbsp;</p>
<p>&nbsp;</p>',
                'product_id' => '2',
                'platform_id' => '1',
            ],
            [
                'name' => 'Write code to Print',
                'content' => '<p>This information assumes that you have already set up your project with the PowaPOS SDK library and&nbsp;have already instantiated the&nbsp;PowaPOS object as described in the&nbsp;Initializing the PowaPOS T25 tutorial.</p>
<p>The following code snippet allows printing a text string and an image to the T-Series Printer:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09863de210257281799" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">powaPOS.PrintText(“Hello POWA!”);
powaPOS.PrintImage(image); //The image binary data expressed in a Bitmap object</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09863de210257281799-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09863de210257281799-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09863de210257281799-1"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">PrintText</span><span class="crayon-sy">(</span>“<span class="crayon-e">Hello </span><span class="crayon-v">POWA</span><span class="crayon-o">!</span>”<span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09863de210257281799-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">PrintImage</span><span class="crayon-sy">(</span><span class="crayon-v">image</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">//The image binary data expressed in a Bitmap object</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'name' => 'Initializing the PowaPOS T25',
                'content' => '<p>The following sections describe the step-by-step procedures on how to create a simple Windows application to illustrate the SDK usage.</p>
<p><strong>Setting up the Project</strong></p>
<p>The initial tasks to get the SDK integrated are those required to create a project referencing the SDK libraries.</p>
<p><strong>Creating a Simple Application</strong></p>
<p>The following sections dive into the details of the Application design and how it makes use of the SDK functionality. The SDK requires some initial configuration to adapt it to the target project. The following sections show a step-by-step implementation using the PowaPOS SDK.</p>
<p><strong>Add external libraries</strong></p>
<p>These DLLs must be added as project references:</p>
<ul>
<li>Powa.Win.Sdk.Commons.dll</li>
<li>Powa.Win.Sdk.PowaPOS.dll</li>
</ul>
<p><strong>Initialise the PowaPOS</strong></p>
<p>The first step is to obtain a reference for the PowaPOS object. This object is used to invoke the actions on the device. The object can be created using the following code sample:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f0988052702039417532" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">powaPOSCallbacks = new PowaPOSCallbackClient(this);
PowaPOS powaPOS = new PowaPOS (powaPOSCallbacks);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f0988052702039417532-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f0988052702039417532-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f0988052702039417532-1"><span class="crayon-v">powaPOSCallbacks</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaPOSCallbackClient</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f0988052702039417532-2"><span class="crayon-e">PowaPOS </span><span class="crayon-v">powaPOS</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-v">powaPOSCallbacks</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0008 seconds] -->
<p>&nbsp;</p>
<p>powaPOSCallbacks : The callback to listen all the SDK events.</p>
<p><strong>Initializing the T-Series</strong></p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098805270c699531950" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">C#</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaTSeries tseries = new PowaTSeries(vid, pid);
powaPOS.AddPeripheral(tseries);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098805270c699531950-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098805270c699531950-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098805270c699531950-1"><span class="crayon-e">PowaTSeries </span><span class="crayon-v">tseries</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaTSeries</span><span class="crayon-sy">(</span><span class="crayon-v">vid</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">pid</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098805270c699531950-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">AddPeripheral</span><span class="crayon-sy">(</span><span class="crayon-v">tseries</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0006 seconds] -->
<p>vid: TSeries vendor Id.</p>
<p>pid: TSeries product Id.</p>',
                'product_id' => '1',
                'platform_id' => '3',
            ],
            [
                'name' => 'Using the S10 Scanner',
                'content' => '<h3>Setting up and initialising the S10 scanner.</h3>
<p>Setting up the S10 scanner is largely the same process as setting up the T-Series in your code. You can read the T-Series initialisation tutorial here: <a title="Initialising the PowaPOS T25" href="http://powaposdeveloper.powa.com/blog/tseriestutorials/initialising-the-powapos-t25/" target="_blank">Initialising the PowaPOS T25</a>.</p>
<p>This tutorial assumes you have already set up your project with the PowaPOS SDK frameworks and followed the above tutorial. To use the scanner you need to add the protocol string&nbsp;
            <span id="crayon-55f098cbefb5e854695826" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">com</span><span class="crayon-sy">.</span><span class="crayon-v">powa</span><span class="crayon-sy">.</span><span class="crayon-v">s10</span></span></span>&nbsp;to your info list under “Supported external accessory protocols”.</p>
<p>We will need to set the current view controller up as a scanner observer so we can listen for notifications from it. To do this we add the&nbsp;
            <span id="crayon-55f098cbefb69680655813" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">PowaScannerObserver</span></span></span>&nbsp;directive to our header file. Your&nbsp;<em>.h</em> file should look similar to the below code snippet.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098cbefb6e075051475" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.h

#import &lt;UIKit/UIKit.h&gt;
#import &lt;PowaPOSSDK/PowaPOSSDK.h&gt;

@interface ViewController : UIViewController &lt;PowaScannerObserver&gt;

@property (nonatomic, retain) PowaPOS *powaPOS;

- (void)accessoryDidConnect:(NSNotification *)notif;
- (void)accessoryDidDisconnect:(NSNotification *)notif;

@end</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-2">2</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-4">4</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-6">6</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-8">8</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-10">10</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb6e075051475-12">12</div><div class="crayon-num" data-line="crayon-55f098cbefb6e075051475-13">13</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098cbefb6e075051475-1"><span class="crayon-c">// ViewController.h</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-2">&nbsp;</div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-3"><span class="crayon-p">#import &lt;UIKit/UIKit.h&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-4"><span class="crayon-p">#import &lt;PowaPOSSDK/PowaPOSSDK.h&gt;</span></div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-5">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-6"><span class="crayon-st">@interface</span><span class="crayon-e "> ViewController </span><span class="crayon-v">: UIViewController</span><span class="crayon-h"> </span><span class="crayon-n">&lt;PowaScannerObserver&gt;</span></div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-7">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-8"><span class="crayon-st">@property</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-m">nonatomic</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-r">retain</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-t">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-v">*powaPOS</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-9">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-10"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">accessoryDidConnect</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSNotification</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">notif</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-11"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">accessoryDidDisconnect</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSNotification</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">notif</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb6e075051475-12">&nbsp;</div><div class="crayon-line" id="crayon-55f098cbefb6e075051475-13"><span class="crayon-st">@end</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0013 seconds] -->
<p>Using the code in&nbsp;
            <span id="crayon-55f098cbefb73757199825" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">viewDidLoad</span></span></span>&nbsp;from the T-Series initialisation tutorial we will listen for the external accessory connection/disconnection notifications again.&nbsp;Remember to&nbsp;
            <span id="crayon-55f098cbefb77018597761" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-p">#import &lt;ExternalAccessory/ExternalAccessory.h&gt;</span></span></span>&nbsp;.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098cbefb7c290094657" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.m

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object: nil];
    
    [[EAAccessoryManager sharedAccessoryManager]  registerForLocalNotifications];

    [self accessoryDidConnect:nil]; // We call this incase a scanner is already connected.
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-2">2</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-4">4</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-6">6</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-8">8</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-10">10</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb7c290094657-12">12</div><div class="crayon-num" data-line="crayon-55f098cbefb7c290094657-13">13</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098cbefb7c290094657-1"><span class="crayon-c">// ViewController.m</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-2">&nbsp;</div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-3"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-v">viewDidLoad</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-r">super</span><span class="crayon-h"> </span><span class="crayon-v">viewDidLoad</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">NSNotificationCenter</span><span class="crayon-h"> </span><span class="crayon-v">defaultCenter</span><span class="crayon-sy">]</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-e "> selector</span><span class="crayon-o">:</span><span class="crayon-st">@selector</span><span class="crayon-sy">(</span><span class="crayon-e ">accessoryDidConnect</span><span class="crayon-o">:</span><span class="crayon-sy">)</span><span class="crayon-e "> name</span><span class="crayon-v">:EAAccessoryDidConnectNotification</span><span class="crayon-e "> object</span><span class="crayon-v">: nil</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-7"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-8"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">NSNotificationCenter</span><span class="crayon-h"> </span><span class="crayon-v">defaultCenter</span><span class="crayon-sy">]</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-e "> selector</span><span class="crayon-o">:</span><span class="crayon-st">@selector</span><span class="crayon-sy">(</span><span class="crayon-e ">accessoryDidDisconnect</span><span class="crayon-o">:</span><span class="crayon-sy">)</span><span class="crayon-e "> name</span><span class="crayon-v">:EAAccessoryDidDisconnectNotification</span><span class="crayon-e "> object</span><span class="crayon-v">: nil</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-9"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-10"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">EAAccessoryManager</span><span class="crayon-h"> </span><span class="crayon-v">sharedAccessoryManager</span><span class="crayon-sy">]</span><span class="crayon-e ">&nbsp;&nbsp;registerForLocalNotifications</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-11">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb7c290094657-12"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-e "> accessoryDidConnect</span><span class="crayon-v">:nil</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">// We call this incase a scanner is already connected.</span></div><div class="crayon-line" id="crayon-55f098cbefb7c290094657-13"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0020 seconds] -->
<p>Now we can test to see if a scanner has connected when the notification is triggered. In your&nbsp;
            <span id="crayon-55f098cbefb81606879618" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">accessoryDidConnect</span><span class="crayon-o">:</span></span></span>&nbsp;method add the following code.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098cbefb85659401757" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">self.powaPOS = [[PowaPOS alloc] init];
    
NSArray *connectedScanners = [PowaS10Scanner connectedDevices];
if(connectedScanners.count &gt; 0){
    NSLog(@"Detected %lu S10 scanner(s)", (unsigned long)connectedScanners.count);
    PowaS10Scanner *scanner = [connectedScanners objectAtIndex:0];
    [scanner addObserver:self];
    [self.powaPOS addPeripheral:scanner];
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098cbefb85659401757-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb85659401757-2">2</div><div class="crayon-num" data-line="crayon-55f098cbefb85659401757-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb85659401757-4">4</div><div class="crayon-num" data-line="crayon-55f098cbefb85659401757-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb85659401757-6">6</div><div class="crayon-num" data-line="crayon-55f098cbefb85659401757-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb85659401757-8">8</div><div class="crayon-num" data-line="crayon-55f098cbefb85659401757-9">9</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098cbefb85659401757-1"><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-r">alloc</span><span class="crayon-sy">]</span><span class="crayon-e "> init</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb85659401757-2"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span></div><div class="crayon-line" id="crayon-55f098cbefb85659401757-3"><span class="crayon-t">NSArray</span><span class="crayon-h"> </span><span class="crayon-v">*connectedScanners</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">PowaS10Scanner</span><span class="crayon-h"> </span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb85659401757-4"><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">connectedScanners</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-h"> </span><span class="crayon-o">&gt;</span><span class="crayon-h"> </span><span class="crayon-cn">0</span><span class="crayon-sy">)</span><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f098cbefb85659401757-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"Detected %lu S10 scanner(s)"</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">unsigned</span><span class="crayon-h"> </span><span class="crayon-t">long</span><span class="crayon-sy">)</span><span class="crayon-v">connectedScanners</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb85659401757-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">PowaS10Scanner</span><span class="crayon-h"> </span><span class="crayon-v">*scanner</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-v">connectedScanners</span><span class="crayon-e "> objectAtIndex</span><span class="crayon-o">:</span><span class="crayon-cn">0</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb85659401757-7"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-v">scanner</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb85659401757-8"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-e "> addPeripheral</span><span class="crayon-v">:scanner</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb85659401757-9"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0020 seconds] -->
<p>As with the T-Series tutorial we first instantiate the PowaPOS object. Then we see how many connected scanners there are using&nbsp;
            <span id="crayon-55f098cbefb8a477786644" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-sy">[</span><span class="crayon-e">PowaS10Scanner </span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span></span></span>&nbsp;. This method returns an array of connected scanners. As the scanners connect via bluetooth there is a high&nbsp;chance that there will be more than one scanner connected at once. This would be the opportunity to display the scanners’ serial numbers in your app and allow the user to choose the one they are using. For brevity we are going to assume there is only one scanner connected and choose the first object in the array.</p>
<p>The line&nbsp;
            <span id="crayon-55f098cbefb8f363801074" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-sy">[</span><span class="crayon-e">scanner </span><span class="crayon-v">addObserver</span><span class="crayon-o">:</span><span class="crayon-v">self</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></span></span>&nbsp;adds the view controller as an observer and then we add the scanner as a peripheral to the PowaPOS object in the final line.</p>
<p>There are two observer methods that you will need to use for the scanner. Firstly&nbsp;
            <span id="crayon-55f098cbefb93965640438" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-v">scannerDidFinishInitializing</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-v">id</span><span class="crayon-o">&lt;</span><span class="crayon-v">PowaScanner</span><span class="crayon-o">&gt;</span><span class="crayon-sy">)</span><span class="crayon-v">scanner</span><span class="crayon-sy">;</span></span></span>&nbsp;is similar to the T-Series equivalent. This will notify you when the scanner is ready to be used. You can use this method like so:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098cbefb98361358622" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">- (void)scannerDidFinishInitializing:(id&lt;PowaScanner&gt;)scanner {
    NSLog(@"Scanner is ready");
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098cbefb98361358622-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefb98361358622-2">2</div><div class="crayon-num" data-line="crayon-55f098cbefb98361358622-3">3</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098cbefb98361358622-1"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">scannerDidFinishInitializing</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">id</span><span class="crayon-n">&lt;PowaScanner&gt;</span><span class="crayon-sy">)</span><span class="crayon-v">scanner</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefb98361358622-2"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"Scanner is ready"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefb98361358622-3"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0006 seconds] -->
<p>The other method is&nbsp;
            <span id="crayon-55f098cbefb9c090200773" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-v">scanner</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-v">id</span><span class="crayon-o">&lt;</span><span class="crayon-v">PowaScanner</span><span class="crayon-o">&gt;</span><span class="crayon-sy">)</span><span class="crayon-e">scanner </span><span class="crayon-v">scannedBarcode</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-e ">NSString *</span><span class="crayon-sy">)</span><span class="crayon-v">barcode</span><span class="crayon-sy">;</span></span></span>&nbsp;and calls back when a valid barcode is scanned by the S10 scanner.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098cbefba0164053884" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">- (void)scanner:(id&lt;PowaScanner&gt;)scanner scannedBarcode:(NSString *)barcode {
    NSLog(@"Scanned: %@", barcode);
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098cbefba0164053884-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098cbefba0164053884-2">2</div><div class="crayon-num" data-line="crayon-55f098cbefba0164053884-3">3</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098cbefba0164053884-1"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">scanner</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">id</span><span class="crayon-n">&lt;PowaScanner&gt;</span><span class="crayon-sy">)</span><span class="crayon-v">scanner</span><span class="crayon-e "> scannedBarcode</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSString</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">barcode</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098cbefba0164053884-2"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"Scanned: %@"</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">barcode</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f098cbefba0164053884-3"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0008 seconds] -->
<p>Running the code on an iPad will output valid barcodes to the console log. You can test this with any product barcode.</p>',
                'product_id' => '2',
                'platform_id' => '2',
            ],
            [
                'name' => 'Printing an Image',
                'content' => '<p>Printing an image onto a&nbsp;receipt can be achieved&nbsp;using the&nbsp;
            <span id="crayon-55f098f90b4d2088984886" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">printImage</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-e ">UIImage *</span><span class="crayon-sy">)</span><span class="crayon-v">image</span></span></span>&nbsp; method on the&nbsp;
            <span id="crayon-55f098f90b4de792821672" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-v">printer</span></span></span>&nbsp; object.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f098f90b4e3146692057" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">UIImage *myImage = [UIImage imageNamed:@"receipt_image"];
[powaPOS.printer printImage:myImage];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f098f90b4e3146692057-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f098f90b4e3146692057-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f098f90b4e3146692057-1"><span class="crayon-t">UIImage</span><span class="crayon-h"> </span><span class="crayon-v">*myImage</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">UIImage</span><span class="crayon-e "> imageNamed</span><span class="crayon-o">:</span><span class="crayon-s">@"receipt_image"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f098f90b4e3146692057-2"><span class="crayon-sy">[</span><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-v">printer</span><span class="crayon-e "> printImage</span><span class="crayon-v">:myImage</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Initialising the PowaPOS T25',
                'content' => '<h3>Set up your Xcode project.</h3>
<p>In this example I am using a Single View Application.</p>
<p><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-17-at-10.26.54.png"><img class="alignnone wp-image-393 size-full" src="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-17-at-10.26.54.png" alt="Screen Shot 2014-12-17 at 10.26.54" width="250" height="183"></a></p>
<p>Next add the protocol string to your info list. If you don’t already have the entry “Supported external accessory protocols” please add it.</p>
<p>The protocol string to use is 
            <span id="crayon-55f09917d288e007536410" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">com</span><span class="crayon-sy">.</span><span class="crayon-v">powa</span><span class="crayon-sy">.</span><span class="crayon-v">mcu</span></span></span> .</p>
<p><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.09.55.png"><img class="alignnone wp-image-394 size-medium" src="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.09.55-300x37.png" alt="Screen Shot 2014-12-18 at 12.09.55" width="300" height="37"></a></p>
<p>Finally add the PowaPOS framework to your project.</p>
<p>In the latest versions of Xcode all you have to do is drag and drop into your project.</p>
<p><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.16.52.png"><img class="alignnone size-full wp-image-396" src="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.16.52.png" alt="Screen Shot 2014-12-18 at 12.16.52" width="253" height="92"></a></p>
<p>It gets automatically added to your build settings.</p>
<p><a href="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.17.43.png"><img class="alignnone size-medium wp-image-397" src="http://powaposdeveloper.powa.com/wp-content/uploads/2014/12/Screen-Shot-2014-12-18-at-12.17.43-300x64.png" alt="Screen Shot 2014-12-18 at 12.17.43" width="300" height="64"></a></p>
<p>&nbsp;</p>
<h3>Connecting to the T-Series</h3>
<p>The 
            <span id="crayon-55f09917d289b617237714" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-t">PowaPOS</span></span></span> object controls and maintains everything for you. We will add it as an 
            <span id="crayon-55f09917d28a0254314185" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-st">@property</span></span></span> to our main view controller so that we can access it throughout the class.</p>
<p>We will also register this class as a 
            <span id="crayon-55f09917d28a5131221542" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">PowaTSeriesObserver</span></span></span> so that we can get the callback letting us know the T25 has initialised correctly.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28a9189019825" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title">ViewController.h</span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.h

#import &lt;UIKit/UIKit.h&gt;
#import &lt;PowaPOSSDK/PowaPOSSDK.h&gt;

@interface ViewController : UIViewController &lt;PowaTSeriesObserver&gt;

@property (nonatomic, retain) PowaPOS *powaPOS;

@end</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28a9189019825-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28a9189019825-2">2</div><div class="crayon-num" data-line="crayon-55f09917d28a9189019825-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28a9189019825-4">4</div><div class="crayon-num" data-line="crayon-55f09917d28a9189019825-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28a9189019825-6">6</div><div class="crayon-num" data-line="crayon-55f09917d28a9189019825-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28a9189019825-8">8</div><div class="crayon-num" data-line="crayon-55f09917d28a9189019825-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28a9189019825-10">10</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28a9189019825-1"><span class="crayon-c">// ViewController.h</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28a9189019825-2">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28a9189019825-3"><span class="crayon-p">#import &lt;UIKit/UIKit.h&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28a9189019825-4"><span class="crayon-p">#import &lt;PowaPOSSDK/PowaPOSSDK.h&gt;</span></div><div class="crayon-line" id="crayon-55f09917d28a9189019825-5">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28a9189019825-6"><span class="crayon-st">@interface</span><span class="crayon-e "> ViewController </span><span class="crayon-v">: UIViewController</span><span class="crayon-h"> </span><span class="crayon-n">&lt;PowaTSeriesObserver&gt;</span></div><div class="crayon-line" id="crayon-55f09917d28a9189019825-7">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28a9189019825-8"><span class="crayon-st">@property</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-m">nonatomic</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-r">retain</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-t">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-v">*powaPOS</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09917d28a9189019825-9">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28a9189019825-10"><span class="crayon-st">@end</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>We need to register for external accessory connected/disconnected notifications.</p>
<p>First import the External Accessory framework.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28ae620889205" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.m

#import &lt;ExternalAccessory/ExternalAccessory.h&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28ae620889205-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28ae620889205-2">2</div><div class="crayon-num" data-line="crayon-55f09917d28ae620889205-3">3</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28ae620889205-1"><span class="crayon-c">// ViewController.m</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28ae620889205-2">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28ae620889205-3"><span class="crayon-p">#import &lt;ExternalAccessory/ExternalAccessory.h&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0002 seconds] -->
<p>Next, in the 
            <span id="crayon-55f09917d28b2241839498" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">viewDidLoad</span></span></span> method of your view controller add this code to register for the notifications.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28b7498206036" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title">viewDidLoad</span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object: nil];

[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object: nil];

[[EAAccessoryManager sharedAccessoryManager]  registerForLocalNotifications];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28b7498206036-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28b7498206036-2">2</div><div class="crayon-num" data-line="crayon-55f09917d28b7498206036-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28b7498206036-4">4</div><div class="crayon-num" data-line="crayon-55f09917d28b7498206036-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28b7498206036-1"><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">NSNotificationCenter</span><span class="crayon-h"> </span><span class="crayon-v">defaultCenter</span><span class="crayon-sy">]</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-e "> selector</span><span class="crayon-o">:</span><span class="crayon-st">@selector</span><span class="crayon-sy">(</span><span class="crayon-e ">accessoryDidConnect</span><span class="crayon-o">:</span><span class="crayon-sy">)</span><span class="crayon-e "> name</span><span class="crayon-v">:EAAccessoryDidConnectNotification</span><span class="crayon-e "> object</span><span class="crayon-v">: nil</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28b7498206036-2">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28b7498206036-3"><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">NSNotificationCenter</span><span class="crayon-h"> </span><span class="crayon-v">defaultCenter</span><span class="crayon-sy">]</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-e "> selector</span><span class="crayon-o">:</span><span class="crayon-st">@selector</span><span class="crayon-sy">(</span><span class="crayon-e ">accessoryDidDisconnect</span><span class="crayon-o">:</span><span class="crayon-sy">)</span><span class="crayon-e "> name</span><span class="crayon-v">:EAAccessoryDidDisconnectNotification</span><span class="crayon-e "> object</span><span class="crayon-v">: nil</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28b7498206036-4">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28b7498206036-5"><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">EAAccessoryManager</span><span class="crayon-h"> </span><span class="crayon-v">sharedAccessoryManager</span><span class="crayon-sy">]</span><span class="crayon-e ">&nbsp;&nbsp;registerForLocalNotifications</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0013 seconds] -->
<p>This code will call the method 
            <span id="crayon-55f09917d28bc354801964" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">accessoryDidConnect</span><span class="crayon-o">:</span></span></span> when the T25 (or any other accessory) is connected and 
            <span id="crayon-55f09917d28c0120991889" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">accessoryDidDisconnect</span><span class="crayon-o">:</span></span></span> when it is disconnected.</p>
<p>So next we have to implement these methods to connect the T25.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28c4238083444" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.m

- (void)accessoryDidConnect:(NSNotification *)notif {
    self.powaPOS = [[PowaPOS alloc] init];
    NSArray *connectedDevices = [PowaTSeries connectedDevices];
    if(connectedDevices.count &gt; 0){
        NSLog(@"Detected T25");
        PowaTSeries *device = [connectedDevices objectAtIndex:0];
        [device addObserver:self];
        [self.powaPOS addPeripheral:device];
    }
}

- (void)accessoryDidDisconnect:(NSNotification *)notif {
    NSArray *connectedDevices = [PowaTSeries connectedDevices];
    if(connectedDevices.count == 0){
        NSLog(@"T25 Disconnected");
    }
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-2">2</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-4">4</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-6">6</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-8">8</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-10">10</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-12">12</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-13">13</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-14">14</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-15">15</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-16">16</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-17">17</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28c4238083444-18">18</div><div class="crayon-num" data-line="crayon-55f09917d28c4238083444-19">19</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28c4238083444-1"><span class="crayon-c">// ViewController.m</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-2">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28c4238083444-3"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">accessoryDidConnect</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSNotification</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">notif</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-r">alloc</span><span class="crayon-sy">]</span><span class="crayon-e "> init</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSArray</span><span class="crayon-h"> </span><span class="crayon-v">*connectedDevices</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-h"> </span><span class="crayon-o">&gt;</span><span class="crayon-h"> </span><span class="crayon-cn">0</span><span class="crayon-sy">)</span><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-7"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"Detected T25"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-8"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-v">*device</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-v">connectedDevices</span><span class="crayon-e "> objectAtIndex</span><span class="crayon-o">:</span><span class="crayon-cn">0</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-9"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-v">device</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-10"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-e "> addPeripheral</span><span class="crayon-v">:device</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-11"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-12"><span class="crayon-sy">}</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-13">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-14"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">accessoryDidDisconnect</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSNotification</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">notif</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-15"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSArray</span><span class="crayon-h"> </span><span class="crayon-v">*connectedDevices</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-16"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-h"> </span><span class="crayon-o">==</span><span class="crayon-h"> </span><span class="crayon-cn">0</span><span class="crayon-sy">)</span><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-17"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"T25 Disconnected"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28c4238083444-18"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div><div class="crayon-line" id="crayon-55f09917d28c4238083444-19"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0033 seconds] -->
<p>Line by line:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28c9015451970" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">self.powaPOS = [[PowaPOS alloc] init];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28c9015451970-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28c9015451970-1"><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">PowaPOS</span><span class="crayon-h"> </span><span class="crayon-r">alloc</span><span class="crayon-sy">]</span><span class="crayon-e "> init</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>This initialises the PowaPOS object which maintains the connection to the T25. We store it in the class variable we created earlier so that we can use it in the future to print receipts and perform other functions.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28ce001813070" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">NSArray *connectedDevices = [PowaTSeries connectedDevices];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28ce001813070-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28ce001813070-1"><span class="crayon-t">NSArray</span><span class="crayon-h"> </span><span class="crayon-v">*connectedDevices</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>This is an array of connected TSeries objects. At the time of writing this will only ever be one unit as only one can be connected to the USB port on an iPad at once.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28d2853864361" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">if(connectedDevices.count &gt; 0){</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28d2853864361-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28d2853864361-1"><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">connectedDevices</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-h"> </span><span class="crayon-o">&gt;</span><span class="crayon-h"> </span><span class="crayon-cn">0</span><span class="crayon-sy">)</span><span class="crayon-sy">{</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0003 seconds] -->
<p>The if statement tests to see if there are any TSeries connected. The array count should be 1 (or higher) if there are devices connected. If there are no devices in this array then a different accessory has been connected, rather than a TSeries.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28d7774906580" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaTSeries *device = [connectedDevices objectAtIndex:0];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28d7774906580-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28d7774906580-1"><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-v">*device</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-v">connectedDevices</span><span class="crayon-e "> objectAtIndex</span><span class="crayon-o">:</span><span class="crayon-cn">0</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0005 seconds] -->
<p>As stated above we should only have one T25 connected at any one time so we know that the T25 is the first object in the array.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28db909126487" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">[device addObserver:self];
[self.powaPOS addPeripheral:device];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28db909126487-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28db909126487-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28db909126487-1"><span class="crayon-sy">[</span><span class="crayon-v">device</span><span class="crayon-e "> addObserver</span><span class="crayon-v">:self</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28db909126487-2"><span class="crayon-sy">[</span><span class="crayon-r">self</span><span class="crayon-sy">.</span><span class="crayon-v">powaPOS</span><span class="crayon-e "> addPeripheral</span><span class="crayon-v">:device</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>Lastly add your view controller as an observer and add the TSeries devices as a peripheral to the PowaPOS object.</p>
<p>There is nothing that needs to be done at this point when the accessory disconnects. All we do is check to see if the TSeries has disconnected and NSLogging if it has.</p>
<p>&nbsp;</p>
<h3>Initialising the T-Series</h3>
<p>At this point we have identified that a T-Series has connected to the iPad, however we cannot use it until it has finished initialising. The 
            <span id="crayon-55f09917d28e0872713371" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">PowaPOS</span></span></span> object does this initialisation for us and the T-Series will call back to it’s registered observer when it is ready to be used.</p>
<p>As we have already set our view controller up as an observer all we need to do is implement the following observer method which gets called when the T-Series is ready to be used.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28e5672277665" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">- (void)tseriesDidFinishInitializing:(PowaTSeries *)tseries;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28e5672277665-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28e5672277665-1"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">tseriesDidFinishInitializing</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">tseries</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>In your view controller add the following code:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09917d28e9355049998" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// ViewController.m

- (void)tseriesDidFinishInitializing:(PowaTSeries *)tseries {
    NSLog(@"Finished Initialising");
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09917d28e9355049998-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28e9355049998-2">2</div><div class="crayon-num" data-line="crayon-55f09917d28e9355049998-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09917d28e9355049998-4">4</div><div class="crayon-num" data-line="crayon-55f09917d28e9355049998-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09917d28e9355049998-1"><span class="crayon-c">// ViewController.m</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28e9355049998-2">&nbsp;</div><div class="crayon-line" id="crayon-55f09917d28e9355049998-3"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-e ">tseriesDidFinishInitializing</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">PowaTSeries</span><span class="crayon-h"> </span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">tseries</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09917d28e9355049998-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-t">NSLog</span><span class="crayon-sy">(</span><span class="crayon-s">@"Finished Initialising"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09917d28e9355049998-5"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>Once this method is called you can be sure that the T-Series is initialised properly and ready to go. At this point you can update your UI and allow users to print receipts and use the other functionality of the T-Series.</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Barcode Scanning',
                'content' => '<p><b>1.0 – Write code to handle scanned barcodes</b></p>
<p>Receive the initialization event</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f099bb8cce5946418888" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">@Override&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;void&nbsp;onScannerInitialized(PowaPOSEnums.InitializedResult&nbsp;result)&nbsp;{&nbsp;

               if(result.equals(PowaPOSEnums.InitializedResult.SUCCESSFUL))&nbsp;{&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         //start&nbsp;to&nbsp;use&nbsp;the&nbsp;scanner
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;}else{&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        //some&nbsp;error&nbsp;occurred&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;
};</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-2">2</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-4">4</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-6">6</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-8">8</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-10">10</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8cce5946418888-12">12</div><div class="crayon-num" data-line="crayon-55f099bb8cce5946418888-13">13</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f099bb8cce5946418888-1"><span class="crayon-sy">@</span><span class="crayon-i">Override</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-m">public</span>&nbsp;<span class="crayon-t">void</span>&nbsp;<span class="crayon-e">onScannerInitialized</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-i">InitializedResult</span>&nbsp;<span class="crayon-v">result</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-3">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">result</span><span class="crayon-sy">.</span><span class="crayon-e">equals</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-v">InitializedResult</span><span class="crayon-sy">.</span><span class="crayon-v">SUCCESSFUL</span><span class="crayon-sy">)</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="crayon-c">//start&nbsp;to&nbsp;use&nbsp;the&nbsp;scanner</span></div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-8"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span class="crayon-sy">}</span><span class="crayon-st">else</span><span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-10"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-c">//some&nbsp;error&nbsp;occurred&nbsp;&nbsp;</span></div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8cce5946418888-12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span>&nbsp;</div><div class="crayon-line" id="crayon-55f099bb8cce5946418888-13"><span class="crayon-sy">}</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0017 seconds] -->
<p>&nbsp;</p>
<p>The following code illustrates how to handle events regarding the scanner barcode reads:</p>
<p>&nbsp;</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f099bb8ccf1334064071" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title">Initialize Barcode Scanner</span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPOSCallback&nbsp;powaPOSCallback&nbsp;=&nbsp;new&nbsp;PowaPOSCallback&nbsp;()&nbsp;{&nbsp;
@Override&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;void&nbsp;onScannerRead(String&nbsp;data)&nbsp;{&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//do&nbsp;something&nbsp;with&nbsp;the&nbsp;data&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f099bb8ccf1334064071-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8ccf1334064071-2">2</div><div class="crayon-num" data-line="crayon-55f099bb8ccf1334064071-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f099bb8ccf1334064071-4">4</div><div class="crayon-num" data-line="crayon-55f099bb8ccf1334064071-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f099bb8ccf1334064071-1"><span class="crayon-i">PowaPOSCallback</span>&nbsp;<span class="crayon-i">powaPOSCallback</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-r">new</span>&nbsp;<span class="crayon-i">PowaPOSCallback</span>&nbsp;<span class="crayon-sy">(</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8ccf1334064071-2"><span class="crayon-sy">@</span><span class="crayon-i">Override</span>&nbsp;</div><div class="crayon-line" id="crayon-55f099bb8ccf1334064071-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-m">public</span>&nbsp;<span class="crayon-t">void</span>&nbsp;<span class="crayon-e">onScannerRead</span><span class="crayon-sy">(</span><span class="crayon-t">String</span>&nbsp;<span class="crayon-v">data</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f099bb8ccf1334064071-4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-c">//do&nbsp;something&nbsp;with&nbsp;the&nbsp;data&nbsp;</span></div><div class="crayon-line" id="crayon-55f099bb8ccf1334064071-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0009 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'name' => 'Write code to Print',
                'content' => '<p><b>1.0 – Write code to print</b></p>
<p>The following code snippet allows printing a text string and an image to the T-Series Printer:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a0546db7840182884" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">powaPOS.printText(“Hello POWA!”);
powaPOS.printImage(image); //The image binary data expressed in a byte array</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a0546db7840182884-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a0546db7840182884-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a0546db7840182884-1"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">printText</span><span class="crayon-sy">(</span>“<span class="crayon-e">Hello </span><span class="crayon-v">POWA</span><span class="crayon-o">!</span>”<span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a0546db7840182884-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">printImage</span><span class="crayon-sy">(</span><span class="crayon-v">image</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">//The image binary data expressed in a byte array</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'name' => 'Initializing the PowaPOS T25',
                'content' => '<p>The following sections describe the step-by-step procedures on how to initialize the T25</p>
<p><b>Setting up the Project</b></p>
<p>The initial tasks to get the SDK integrated are those to create an Android project referencing the SDK libraries. This can be achieved in several ways, for example by moving the JAR files to the libs folder.</p>
<p><b>Creating a Simple Application</b></p>
<p>The following sections dive into the details of the Application design and how it makes use of the SDK functionality. The SDK requires some initial configuration to adapt it to the target project. The following sections show a step-by-step PowaPOS SDK implementation.</p>
<p><b>Add external libraries</b></p>
<p>These libraries must be copied in the libs folder or added into the Maven dependencies in the POM file:</p>
<ul>
<li>org.apache.commons:commons-io-vx.x.x</li>
<li>com.google.android:support-vxrxx</li>
<li>com.mpowa.android:powapos-sdk-android-vx.x.x</li>
<li>com.mpowa.android:powa-platform-android-vx.x.x</li>
</ul>
<p><b>Add Permissions</b></p>
<p>Adding permissions to allow Bluetooth connections is done on the Android manifest file by including the following statements:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009dd903549273" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title">Add Permissions</span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">&lt;uses-permission android:name="android.permission.BLUETOOTH"/&gt;
&lt;uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/&gt;
&lt;uses-feature android:name="android.hardware.usb.accessory" android:required="true"/&gt;
&lt;uses-feature android:name="android.hardware.usb.host" android:required="true"/&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009dd903549273-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009dd903549273-2">2</div><div class="crayon-num" data-line="crayon-55f09a84009dd903549273-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009dd903549273-4">4</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009dd903549273-1"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">permission </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.permission.BLUETOOTH"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009dd903549273-2"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">permission </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.permission.BLUETOOTH_ADMIN"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09a84009dd903549273-3"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">feature </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.accessory"</span><span class="crayon-h"> </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">required</span><span class="crayon-o">=</span><span class="crayon-s">"true"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009dd903549273-4"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">feature </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.host"</span><span class="crayon-h"> </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">required</span><span class="crayon-o">=</span><span class="crayon-s">"true"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0015 seconds] -->
<p>&nbsp;</p>
<p><b>Add Powa USB Filters</b></p>
<p>Allowing the android Application to automatically identify the Powa T-Series device requires the following actions:</p>
<p>(1) Copy the xml folder located in the sample app resources folder to your application’s project resources folder.</p>
<p>(2) Place the following filters in the manifest file within the activity:</p>
<p>&nbsp;</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009e9824348698" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">&lt;activity&gt; 
    &lt;intent-filter&gt;
                &lt;action android:name="android.hardware.usb.action.USB_ACCESSORY_ATTACHED" /&gt;
    &lt;/intent-filter&gt;
    &lt;meta-data android:name="android.hardware.usb.action.USB_ACCESSORY_ATTACHED"
                android:resource="@xml/accessory_filter" /&gt;
    &lt;intent-filter&gt;
                &lt;action android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED" /&gt;
    &lt;/intent-filter&gt;
    &lt;meta-data android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED"
                android:resource="@xml/device_filter" /&gt;
&lt;/activity&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-2">2</div><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-4">4</div><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-6">6</div><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-8">8</div><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-10">10</div><div class="crayon-num" data-line="crayon-55f09a84009e9824348698-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009e9824348698-12">12</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009e9824348698-1"><span class="crayon-e ">&lt;activity&gt;</span><span class="crayon-h"> </span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-2"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-v">intent</span><span class="crayon-o">-</span><span class="crayon-v">filter</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09a84009e9824348698-3"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-e">action </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.action.USB_ACCESSORY_ATTACHED"</span><span class="crayon-h"> </span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-o">/</span><span class="crayon-v">intent</span><span class="crayon-o">-</span><span class="crayon-v">filter</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09a84009e9824348698-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-v">meta</span><span class="crayon-o">-</span><span class="crayon-e">data </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.action.USB_ACCESSORY_ATTACHED"</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">resource</span><span class="crayon-o">=</span><span class="crayon-s">"@xml/accessory_filter"</span><span class="crayon-h"> </span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09a84009e9824348698-7"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-v">intent</span><span class="crayon-o">-</span><span class="crayon-v">filter</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-8"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-e">action </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.action.USB_DEVICE_ATTACHED"</span><span class="crayon-h"> </span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09a84009e9824348698-9"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-o">/</span><span class="crayon-v">intent</span><span class="crayon-o">-</span><span class="crayon-v">filter</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-10"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-o">&lt;</span><span class="crayon-v">meta</span><span class="crayon-o">-</span><span class="crayon-e">data </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.hardware.usb.action.USB_DEVICE_ATTACHED"</span></div><div class="crayon-line" id="crayon-55f09a84009e9824348698-11"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">resource</span><span class="crayon-o">=</span><span class="crayon-s">"@xml/device_filter"</span><span class="crayon-h"> </span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009e9824348698-12"><span class="crayon-o">&lt;</span><span class="crayon-o">/</span><span class="crayon-v">activity</span><span class="crayon-o">&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0022 seconds] -->
<p>(3) Call the PowaPos.onNewIntent static method on the Activity.onNewIntent(Intent intent) method.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009ee196769835" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">@Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        PowaPOS.onNewIntent(this.getApplicationContext(), intent);
    }</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009ee196769835-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009ee196769835-2">2</div><div class="crayon-num" data-line="crayon-55f09a84009ee196769835-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009ee196769835-4">4</div><div class="crayon-num" data-line="crayon-55f09a84009ee196769835-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009ee196769835-1"><span class="crayon-n">@Override</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009ee196769835-2"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-m">protected</span><span class="crayon-h"> </span><span class="crayon-t">void</span><span class="crayon-h"> </span><span class="crayon-e">onNewIntent</span><span class="crayon-sy">(</span><span class="crayon-e">Intent </span><span class="crayon-v">intent</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line" id="crayon-55f09a84009ee196769835-3"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-r">super</span><span class="crayon-sy">.</span><span class="crayon-e">onNewIntent</span><span class="crayon-sy">(</span><span class="crayon-v">intent</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009ee196769835-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-v">PowaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">onNewIntent</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">.</span><span class="crayon-e">getApplicationContext</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">intent</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09a84009ee196769835-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0009 seconds] -->
<p>&nbsp;</p>
<p><b>Initialize&nbsp;the PowaPOS</b></p>
<p>The first step is to obtain a reference for the PowaPOS object. This object is used to invoke the actions in the device. To create the object we simply execute:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009f3554220931" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPOS&nbsp;&nbsp;powaPOS&nbsp;=&nbsp;new&nbsp;PowaPOS(this, powaPOSCallback);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009f3554220931-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009f3554220931-1"><span class="crayon-i">PowaPOS</span>&nbsp;&nbsp;<span class="crayon-i">powaPOS</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-r">new</span>&nbsp;<span class="crayon-e">PowaPOS</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">powaPOSCallback</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>First&nbsp;parameter:&nbsp;An&nbsp;Android&nbsp;Activity&nbsp;object.</p>
<p>Second&nbsp;parameter:&nbsp;The&nbsp;callback&nbsp;to&nbsp;listen&nbsp;all&nbsp;the&nbsp;SDK&nbsp;events.</p>
<p><span style="text-decoration: underline;">Initializing the T-Series&nbsp;</span></p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009f8609383047" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaTSeries&nbsp;tseries&nbsp;=&nbsp;new&nbsp;PowaTSeries(this);&nbsp;
powaPOS.addPeripheral(tseries);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009f8609383047-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009f8609383047-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009f8609383047-1"><span class="crayon-i">PowaTSeries</span>&nbsp;<span class="crayon-i">tseries</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-r">new</span>&nbsp;<span class="crayon-e">PowaTSeries</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009f8609383047-2"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-e">addPeripheral</span><span class="crayon-sy">(</span><span class="crayon-v">tseries</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0005 seconds] -->
<p><span style="text-decoration: underline;">Receiving the initialization events&nbsp;</span></p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09a84009fc129155803" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPOSCallback&nbsp;powaPOSCallback&nbsp;=&nbsp;new&nbsp;PowaPOSCallback()&nbsp;{&nbsp;
@Override&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;void&nbsp;onMCUInitialized(PowaPOSEnums.InitializedResult&nbsp;result)&nbsp;{&nbsp;
&nbsp;            if(result.equals(PowaPOSEnums.InitializedResult.SUCCESSFUL))&nbsp;{&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//start&nbsp;to&nbsp;use&nbsp;the&nbsp;tseries&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}else{&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//some&nbsp;error&nbsp;occurred&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;};</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009fc129155803-2">2</div><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009fc129155803-4">4</div><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009fc129155803-6">6</div><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009fc129155803-8">8</div><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09a84009fc129155803-10">10</div><div class="crayon-num" data-line="crayon-55f09a84009fc129155803-11">11</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09a84009fc129155803-1"><span class="crayon-i">PowaPOSCallback</span>&nbsp;<span class="crayon-i">powaPOSCallback</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-r">new</span>&nbsp;<span class="crayon-e">PowaPOSCallback</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009fc129155803-2"><span class="crayon-sy">@</span><span class="crayon-i">Override</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09a84009fc129155803-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-m">public</span>&nbsp;<span class="crayon-t">void</span>&nbsp;<span class="crayon-e">onMCUInitialized</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-i">InitializedResult</span>&nbsp;<span class="crayon-v">result</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009fc129155803-4">&nbsp;<span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-st">if</span><span class="crayon-sy">(</span><span class="crayon-v">result</span><span class="crayon-sy">.</span><span class="crayon-e">equals</span><span class="crayon-sy">(</span><span class="crayon-v">PowaPOSEnums</span><span class="crayon-sy">.</span><span class="crayon-v">InitializedResult</span><span class="crayon-sy">.</span><span class="crayon-v">SUCCESSFUL</span><span class="crayon-sy">)</span><span class="crayon-sy">)</span>&nbsp;<span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09a84009fc129155803-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-c">//start&nbsp;to&nbsp;use&nbsp;the&nbsp;tseries&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009fc129155803-6">&nbsp;</div><div class="crayon-line" id="crayon-55f09a84009fc129155803-7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span><span class="crayon-st">else</span><span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009fc129155803-8">&nbsp;</div><div class="crayon-line" id="crayon-55f09a84009fc129155803-9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-c">//some&nbsp;error&nbsp;occurred&nbsp;&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09a84009fc129155803-10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09a84009fc129155803-11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="crayon-sy">}</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0019 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'name' => 'Creating a Native Wrapper Application for a Web-based POS App',
                'content' => '<p>As a web-based&nbsp;app works through a browser it does not have access to the hardware connection of the T-Series. In order to access the hardware connection, a “wrapper”&nbsp;application must be created that can access the hardware (using the PowaPOS T-Series SDK) with an API for calling the functions via javascript. The web app can then implement the javascript API.</p>
<h3>Web View</h3>
<p>The first step is to create a web view application. A web view is a frame that will load any given website (or some HTML code). Imagine Safari&nbsp;or Chrome&nbsp;without the address bar, back &amp; forward buttons and other features.</p>
<p>There are plenty of great tutorials to help getting started with web views including <a title="Tutorial on web views in iOS" href="http://conecode.com/news/2011/05/ios-tutorial-creating-a-web-view-uiwebview/" target="_blank">this one from&nbsp;Cone Code</a>.</p>
<p><a href="https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIWebView_Class/index.html" target="_blank">iOS&nbsp;
            <span id="crayon-55f09aa4a5634100032749" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">UIWebView</span></span></span>&nbsp;code reference.</a></p>
<p><a href="http://developer.android.com/reference/android/webkit/WebView.html" target="_blank">Android&nbsp;
            <span id="crayon-55f09aa4a5640677668821" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">WebView</span></span></span>&nbsp;code reference.</a></p>
<h3>Javascript API</h3>
<p>A javascript API will allow simulation of 2-way communication via the web view object in your native mobile application.</p>
<h4>» Sending Messages to a Web Application from the Native Application</h4>
<p>Sending messages to the web application can be achieved by sending javascript to be “evaluated” via the web view. It works in the same way as the&nbsp;
            <span id="crayon-55f09aa4a5645556090593" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-e">eval</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span></span></span>&nbsp;function in normal javascript. A javascript function can be called using this method.</p>
<p>iOS:&nbsp;
            <span id="crayon-55f09aa4a564a713245229" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-sy">[</span><span class="crayon-v">webView</span><span class="crayon-e "> stringByEvaluatingJavaScriptFromString</span><span class="crayon-o">:</span><span class="crayon-s">@"functionToCall()"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></span></span></p>
<p>Android:&nbsp;
            <span id="crayon-55f09aa4a564e157768550" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">webView</span><span class="crayon-sy">.</span><span class="crayon-e">evaluateJavascript</span><span class="crayon-sy">(</span><span class="crayon-s">"functionToCall()"</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></span></span></p>
<h4>» Sending Messages to a Native Mobile&nbsp;Application from the Web&nbsp;Application</h4>
<p>Sending messages from the web application isn’t so simple as there is no javascript functionality to call native mobile app code directly from javascript.</p>
<p>An alternative method is to create a custom URL scheme which can be intercepted by the native mobile application. Every time the web view tries to change it’s location (e.g. navigating to another page following a link click) it is possible to have the web view call back into the mobile application.</p>
<p>Once these callbacks have been registered it is possible to use javascript to send false location updates in order to send messages back to the mobile app. It is worth structuring a URL scheme to use (e.g. powa-javascript://action-to-call/attr1/attr2/attr3) in order to differentiate between legitimate location changes and API calls.</p>
<p>Here is some sample code for iOS which intercepts location requests and decides how to handle the request based on the URL scheme:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09aa4a5654698473810" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
   NSURL *URL = [request URL]; 
   if ([[URL scheme] isEqualToString:@"pows-javascript"]) {
       // parse the rest of the URL object and execute functions

       return NO; // the web view should not load this "false" request
   }
   return YES; // the web view should continue to load the request  
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09aa4a5654698473810-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09aa4a5654698473810-2">2</div><div class="crayon-num" data-line="crayon-55f09aa4a5654698473810-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09aa4a5654698473810-4">4</div><div class="crayon-num" data-line="crayon-55f09aa4a5654698473810-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09aa4a5654698473810-6">6</div><div class="crayon-num" data-line="crayon-55f09aa4a5654698473810-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09aa4a5654698473810-8">8</div><div class="crayon-num" data-line="crayon-55f09aa4a5654698473810-9">9</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09aa4a5654698473810-1"><span class="crayon-o">-</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-t">BOOL</span><span class="crayon-sy">)</span><span class="crayon-e ">webView</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">UIWebView</span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">webView</span><span class="crayon-e "> shouldStartLoadWithRequest</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">NSURLRequest</span><span class="crayon-t ">*</span><span class="crayon-sy">)</span><span class="crayon-v">request</span><span class="crayon-e "> navigationType</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-t">UIWebViewNavigationType</span><span class="crayon-sy">)</span><span class="crayon-v">navigationType</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09aa4a5654698473810-2"><span class="crayon-h">&nbsp;&nbsp; </span><span class="crayon-t">NSURL</span><span class="crayon-h"> </span><span class="crayon-v">*URL</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-e">request </span><span class="crayon-t">URL</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span><span class="crayon-h"> </span></div><div class="crayon-line" id="crayon-55f09aa4a5654698473810-3"><span class="crayon-h">&nbsp;&nbsp; </span><span class="crayon-st">if</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-t">URL</span><span class="crayon-h"> </span><span class="crayon-v">scheme</span><span class="crayon-sy">]</span><span class="crayon-e "> isEqualToString</span><span class="crayon-o">:</span><span class="crayon-s">@"pows-javascript"</span><span class="crayon-sy">]</span><span class="crayon-sy">)</span><span class="crayon-h"> </span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09aa4a5654698473810-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="crayon-c">// parse the rest of the URL object and execute functions</span></div><div class="crayon-line" id="crayon-55f09aa4a5654698473810-5">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09aa4a5654698473810-6"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="crayon-st">return</span><span class="crayon-h"> </span><span class="crayon-t">NO</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">// the web view should not load this "false" request</span></div><div class="crayon-line" id="crayon-55f09aa4a5654698473810-7"><span class="crayon-h">&nbsp;&nbsp; </span><span class="crayon-sy">}</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09aa4a5654698473810-8"><span class="crayon-h">&nbsp;&nbsp; </span><span class="crayon-st">return</span><span class="crayon-h"> </span><span class="crayon-t">YES</span><span class="crayon-sy">;</span><span class="crayon-h"> </span><span class="crayon-c">// the web view should continue to load the request&nbsp;&nbsp;</span></div><div class="crayon-line" id="crayon-55f09aa4a5654698473810-9"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0018 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '1',
            ],
            [
                'name' => 'Printing a Text Receipt',
                'content' => '<p>Printing a text receipt can be achieved&nbsp;using the&nbsp;
            <span id="crayon-55f09ac14fee4182969668" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">printText</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-e ">NSString *</span><span class="crayon-sy">)</span><span class="crayon-v">text</span></span></span>&nbsp; method on the&nbsp;
            <span id="crayon-55f09ac14feef147889932" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-v">printer</span></span></span>&nbsp; object.</p>
<p>Using this method you can print out a full receipt in one go, or line by line as necessary.</p>
<h3>Printing line by line</h3>
<p>To print line by line you must use the 
            <span id="crayon-55f09ac14fef4214947760" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">printText</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-e ">NSString *</span><span class="crayon-sy">)</span><span class="crayon-v">text</span></span></span>&nbsp; method to print&nbsp;
            <span id="crayon-55f09ac14fef9357682613" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">NSString</span></span></span>&nbsp; objects.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09ac14fefd277692491" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">[powaPOS.printer printText:@"Hello, World!"];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09ac14fefd277692491-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09ac14fefd277692491-1"><span class="crayon-sy">[</span><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-v">printer</span><span class="crayon-e "> printText</span><span class="crayon-o">:</span><span class="crayon-s">@"Hello, World!"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p></p>
<h3>Printing a full receipt</h3>
<p>To print a full receipt in one go you must first build the receipt in an NSString.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09ac14ff01555982641" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">NSMutableString *receipt = [NSMutableString stringWithString:@" \n"];
[receipt appendString:@" \n"];
[receipt appendString:@"        POWA Boutique \n"];
[receipt appendString:@"         123 Hialeah \n"];
[receipt appendString:@"        Miami, Florida \n"];
[receipt appendString:@" \n"];
[receipt appendString:@"Date: 06/05/2014 Time: 04:00 PM \n"];
[receipt appendString:@"------------------------------- \n"];
[receipt appendString:@"SALE \n"];
[receipt appendString:@"300678556  T SHIRT        10.99 \n"];
[receipt appendString:@"300678557  PANTS          21.99 \n"];
[receipt appendString:@" \n"];
[receipt appendString:@"Subtotal                  32.98 \n"];
[receipt appendString:@"Tax                        0.00 \n"];
[receipt appendString:@"------------------------------- \n"];
[receipt appendString:@"TOTAL                     32.98 \n"];
[receipt appendString:@"------------------------------- \n"];
[receipt appendString:@" \n\n"];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-2">2</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-4">4</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-6">6</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-8">8</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-10">10</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-11">11</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-12">12</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-13">13</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-14">14</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-15">15</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-16">16</div><div class="crayon-num" data-line="crayon-55f09ac14ff01555982641-17">17</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09ac14ff01555982641-18">18</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09ac14ff01555982641-1"><span class="crayon-t">NSMutableString</span><span class="crayon-h"> </span><span class="crayon-v">*receipt</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-sy">[</span><span class="crayon-t">NSMutableString</span><span class="crayon-e "> stringWithString</span><span class="crayon-o">:</span><span class="crayon-s">@" \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-2"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@" \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-3"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POWA Boutique \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-4"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 123 Hialeah \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-5"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miami, Florida \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-6"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@" \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-7"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"Date: 06/05/2014 Time: 04:00 PM \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-8"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"------------------------------- \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-9"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"SALE \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-10"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"300678556&nbsp;&nbsp;T SHIRT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.99 \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-11"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"300678557&nbsp;&nbsp;PANTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21.99 \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-12"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@" \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-13"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"Subtotal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;32.98 \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-14"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00 \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-15"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"------------------------------- \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-16"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 32.98 \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09ac14ff01555982641-17"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@"------------------------------- \n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09ac14ff01555982641-18"><span class="crayon-sy">[</span><span class="crayon-v">receipt</span><span class="crayon-e "> appendString</span><span class="crayon-o">:</span><span class="crayon-s">@" \n\n"</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0033 seconds] -->
<p>Then you must use the&nbsp;
            <span id="crayon-55f09ac14ff08740741728" class="crayon-syntax crayon-syntax-inline  crayon-theme-github crayon-theme-github-inline crayon-font-monaco" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important;"><span class="crayon-pre crayon-code" style="font-size: 12px !important; line-height: 15px !important;font-size: 12px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><span class="crayon-v">printText</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-e ">NSString *</span><span class="crayon-sy">)</span><span class="crayon-v">text</span></span></span>&nbsp;method again to print the full receipt.</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09ac14ff0c960973919" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Objective-C</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">[powaPOS.printer printText:receipt];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09ac14ff0c960973919-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09ac14ff0c960973919-1"><span class="crayon-sy">[</span><span class="crayon-v">powaPOS</span><span class="crayon-sy">.</span><span class="crayon-v">printer</span><span class="crayon-e "> printText</span><span class="crayon-v">:receipt</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0003 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '1',
                'platform_id' => '2',
            ],
            [
                'name' => 'Setting up the PowaPIN in iOS',
                'content' => '<p>1)&nbsp;Getting&nbsp;Started</p>
<p>The&nbsp;following&nbsp;sections&nbsp;describe&nbsp;the&nbsp;step‐by‐step&nbsp;procedures&nbsp;on&nbsp;how&nbsp;to&nbsp;create&nbsp;a&nbsp;simple&nbsp;iOS&nbsp;application&nbsp;to&nbsp;illustrate&nbsp;the&nbsp;SDK&nbsp;usage.</p>
<p><span id="more-456"></span></p>
<p>2)&nbsp;Setting&nbsp;up&nbsp;the&nbsp;Project</p>
<p>The&nbsp;initial&nbsp;tasks&nbsp;to&nbsp;get&nbsp;the&nbsp;SDK&nbsp;integrated&nbsp;are&nbsp;to&nbsp;create&nbsp;an&nbsp;iOS&nbsp;project&nbsp;and&nbsp;reference&nbsp;the&nbsp;SDK&nbsp;libraries:<br>
a)&nbsp;Create&nbsp;a&nbsp;new&nbsp;xcode&nbsp;project<br>
b)&nbsp;Import&nbsp;the&nbsp;SDK&nbsp;framework&nbsp;(PowaPINSDK.framework)&nbsp;into&nbsp;the&nbsp;frameworks&nbsp;folder<br>
c)&nbsp;Import&nbsp;the&nbsp;SDK&nbsp;framework&nbsp;(PowaPINDriver.framework)&nbsp;into&nbsp;the&nbsp;frameworks&nbsp;folder.</p>
<p>&nbsp;</p>
<p>3)&nbsp;Creating&nbsp;a&nbsp;Simple&nbsp;Application</p>
<p>The&nbsp;following&nbsp;sections&nbsp;dive&nbsp;into&nbsp;the&nbsp;details&nbsp;of&nbsp;the&nbsp;Application&nbsp;design&nbsp;and&nbsp;how&nbsp;it&nbsp;makes&nbsp;use&nbsp;of&nbsp;the&nbsp;SDK&nbsp;functionality.&nbsp;The&nbsp;SDK&nbsp;requires&nbsp;some&nbsp;initial&nbsp;configuration&nbsp;to&nbsp;adapt&nbsp;it&nbsp;to&nbsp;the&nbsp;target&nbsp;project.&nbsp;The&nbsp;following&nbsp;sections&nbsp;show&nbsp;a&nbsp;step‐by‐step&nbsp;implementation.</p>
<p>&nbsp;</p>
<p>4)&nbsp;Configure&nbsp;the&nbsp;Info.plist&nbsp;file</p>
<p>Add&nbsp;a&nbsp;row&nbsp;with&nbsp;the&nbsp;key:&nbsp;“Supported&nbsp;external&nbsp;accessory&nbsp;protocols”.&nbsp;Then&nbsp;add&nbsp;the&nbsp;key&nbsp;string&nbsp;pair&nbsp;under&nbsp;it&nbsp;with&nbsp;the&nbsp;value:&nbsp;“com.AmpedRFTech.Demo”.&nbsp;This&nbsp;action&nbsp;allows&nbsp;the&nbsp;iOS&nbsp;app&nbsp;to&nbsp;recognise&nbsp;the<br>
PowaPIN.</p>
<p>&nbsp;</p>
<p>5)&nbsp;Import&nbsp;the&nbsp;Library&nbsp;header&nbsp;file</p>
<p>This&nbsp;is&nbsp;achieved&nbsp;using&nbsp;the&nbsp;following&nbsp;code&nbsp;snippet&nbsp;in&nbsp;the&nbsp;class&nbsp;header&nbsp;files&nbsp;that&nbsp;will&nbsp;use&nbsp;the&nbsp;SDK:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c754157272060" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">#import&nbsp;&lt;PowaPINSDK/PowaPINSDK.h&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c754157272060-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c754157272060-1"><span class="crayon-p">#import&nbsp;&lt;PowaPINSDK/PowaPINSDK.h&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0002 seconds] -->
<p>&nbsp;</p>
<p>6)&nbsp;Import&nbsp;the&nbsp;Server&nbsp;simulation&nbsp;header&nbsp;file</p>
<p>In&nbsp;the&nbsp;implementation&nbsp;file,&nbsp;import&nbsp;the&nbsp;PPMockServerApp.h&nbsp;file&nbsp;included&nbsp;in&nbsp;the&nbsp;sample&nbsp;project:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c760019340843" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">#import&nbsp;“PPMockServerApp.h”</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c760019340843-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c760019340843-1"><span class="crayon-p">#import&nbsp;“PPMockServerApp.h”</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0001 seconds] -->
<p>&nbsp;</p>
<p>7)&nbsp;Initialize&nbsp;the&nbsp;PowaPIN</p>
<p>In&nbsp;the&nbsp;initialization&nbsp;method,&nbsp;initialise&nbsp;the&nbsp;PowaPIN:<br>
Fetch&nbsp;a&nbsp;connected&nbsp;instance&nbsp;of&nbsp;a&nbsp;PowaPIN&nbsp;using&nbsp;the&nbsp;PED&nbsp;manager:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c765719323074" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaDeviceInfo&nbsp;&nbsp;*ped&nbsp;=&nbsp;nil;&nbsp;
NSArray&nbsp;*pedDevices&nbsp;=&nbsp;[[PEDManager&nbsp;sharedManager]&nbsp;connectedDevices]&nbsp;getAvailablePEDs];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c765719323074-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c765719323074-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c765719323074-1"><span class="crayon-i">PowaDeviceInfo</span>&nbsp;&nbsp;<span class="crayon-o">*</span><span class="crayon-i">ped</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-v">nil</span><span class="crayon-sy">;</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c765719323074-2"><span class="crayon-i">NSArray</span>&nbsp;<span class="crayon-o">*</span><span class="crayon-i">pedDevices</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-i">PEDManager</span>&nbsp;<span class="crayon-v">sharedManager</span><span class="crayon-sy">]</span>&nbsp;<span class="crayon-v">connectedDevices</span><span class="crayon-sy">]</span>&nbsp;<span class="crayon-v">getAvailablePEDs</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0006 seconds] -->
<p>Check&nbsp;if&nbsp;there&nbsp;is&nbsp;any&nbsp;connected&nbsp;PED:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c76a775188873" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">If(pedDevices.count)&nbsp;
&nbsp;&nbsp;ped&nbsp;=&nbsp;[pedDevices&nbsp;objectAtIndex:0];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c76a775188873-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c76a775188873-2">2</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c76a775188873-1"><span class="crayon-st">If</span><span class="crayon-sy">(</span><span class="crayon-v">pedDevices</span><span class="crayon-sy">.</span><span class="crayon-v">count</span><span class="crayon-sy">)</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c76a775188873-2">&nbsp;&nbsp;<span class="crayon-i">ped</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-sy">[</span><span class="crayon-i">pedDevices</span>&nbsp;<span class="crayon-v">objectAtIndex</span><span class="crayon-o">:</span><span class="crayon-cn">0</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>8)&nbsp;Initialize&nbsp;the&nbsp;Simulation&nbsp;Server<br>
In&nbsp;the&nbsp;initialization&nbsp;method,&nbsp;initialize&nbsp;the&nbsp;simulation&nbsp;server&nbsp;to&nbsp;provide&nbsp;mocked&nbsp;responses&nbsp;to&nbsp;the<br>
PowaPIN:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c76e968987234" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PPMockServerApp&nbsp;*mockServerApp&nbsp;=&nbsp;[[[PPMockServerApp&nbsp;alloc]&nbsp;init]&nbsp;autorelease];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c76e968987234-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c76e968987234-1"><span class="crayon-i">PPMockServerApp</span>&nbsp;<span class="crayon-o">*</span><span class="crayon-i">mockServerApp</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-sy">[</span><span class="crayon-i">PPMockServerApp</span>&nbsp;<span class="crayon-v">alloc</span><span class="crayon-sy">]</span>&nbsp;<span class="crayon-v">init</span><span class="crayon-sy">]</span>&nbsp;<span class="crayon-v">autorelease</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0005 seconds] -->
<p>9)&nbsp;Initialize&nbsp;the&nbsp;SDK&nbsp;framework<br>
The&nbsp;following&nbsp;code&nbsp;snippet&nbsp;initializes&nbsp;the&nbsp;SDK&nbsp;with&nbsp;the&nbsp;PED&nbsp;and&nbsp;Server&nbsp;objects:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c773063793873" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPIN&nbsp;*powaPIN&nbsp;=&nbsp;[PowaPIN&nbsp;alloc]&nbsp;init];&nbsp;&nbsp;
[powaPIN&nbsp;selectPED:&nbsp;ped];&nbsp;
[powaPIN&nbsp;setServer:&nbsp;mockServerApp]&nbsp;
[powaPOS addObserver:self];</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c773063793873-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c773063793873-2">2</div><div class="crayon-num" data-line="crayon-55f09b483c773063793873-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c773063793873-4">4</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c773063793873-1"><span class="crayon-i">PowaPIN</span>&nbsp;<span class="crayon-o">*</span><span class="crayon-i">powaPIN</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-sy">[</span><span class="crayon-i">PowaPIN</span>&nbsp;<span class="crayon-v">alloc</span><span class="crayon-sy">]</span>&nbsp;<span class="crayon-v">init</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span>&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c773063793873-2"><span class="crayon-sy">[</span><span class="crayon-i">powaPIN</span>&nbsp;<span class="crayon-v">selectPED</span><span class="crayon-o">:</span>&nbsp;<span class="crayon-v">ped</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09b483c773063793873-3"><span class="crayon-sy">[</span><span class="crayon-i">powaPIN</span>&nbsp;<span class="crayon-v">setServer</span><span class="crayon-o">:</span>&nbsp;<span class="crayon-v">mockServerApp</span><span class="crayon-sy">]</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c773063793873-4"><span class="crayon-sy">[</span><span class="crayon-e">powaPOS </span><span class="crayon-v">addObserver</span><span class="crayon-o">:</span><span class="crayon-v">self</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0009 seconds] -->
<p>10)&nbsp;Write&nbsp;code&nbsp;to&nbsp;start&nbsp;a&nbsp;Sale&nbsp;transaction</p>
<p>The&nbsp;following&nbsp;code&nbsp;should&nbsp;be&nbsp;called&nbsp;when&nbsp;the&nbsp;control&nbsp;that&nbsp;starts&nbsp;the&nbsp;Sale&nbsp;transaction&nbsp;is&nbsp;activated&nbsp;(It&nbsp;can&nbsp;be&nbsp;a&nbsp;button&nbsp;or&nbsp;any&nbsp;other&nbsp;control&nbsp;element):</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c778719752924" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">int&nbsp;amount&nbsp;=&nbsp;100;&nbsp;&nbsp;
[powaPOS&nbsp;startTransactionWithOperationType:PEDOperationTypeSale&nbsp;
&nbsp; amount:amount&nbsp;
&nbsp; transDate:date&nbsp;
&nbsp; cvmMethod:cvmMethod];&nbsp;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c778719752924-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c778719752924-2">2</div><div class="crayon-num" data-line="crayon-55f09b483c778719752924-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c778719752924-4">4</div><div class="crayon-num" data-line="crayon-55f09b483c778719752924-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c778719752924-1"><span class="crayon-t">int</span>&nbsp;<span class="crayon-i">amount</span>&nbsp;<span class="crayon-o">=</span>&nbsp;<span class="crayon-cn">100</span><span class="crayon-sy">;</span>&nbsp;&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c778719752924-2"><span class="crayon-sy">[</span><span class="crayon-i">powaPOS</span>&nbsp;<span class="crayon-v">startTransactionWithOperationType</span><span class="crayon-o">:</span><span class="crayon-i">PEDOperationTypeSale</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09b483c778719752924-3">&nbsp;<span class="crayon-h"> </span><span class="crayon-v">amount</span><span class="crayon-o">:</span><span class="crayon-i">amount</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c778719752924-4">&nbsp;<span class="crayon-h"> </span><span class="crayon-v">transDate</span><span class="crayon-o">:</span><span class="crayon-i">date</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09b483c778719752924-5">&nbsp;<span class="crayon-h"> </span><span class="crayon-v">cvmMethod</span><span class="crayon-o">:</span><span class="crayon-v">cvmMethod</span><span class="crayon-sy">]</span><span class="crayon-sy">;</span>&nbsp;</div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0008 seconds] -->
<p>11)&nbsp;Handle&nbsp;the&nbsp;PowaPIN&nbsp;transaction&nbsp;finish&nbsp;event<br>
When&nbsp;the&nbsp;transaction&nbsp;is&nbsp;completed&nbsp;with&nbsp;the&nbsp;PowaPIN,&nbsp;a&nbsp;method&nbsp;is&nbsp;evoked&nbsp;from&nbsp;the&nbsp;SDK. &nbsp;&nbsp;Set&nbsp;the&nbsp;class&nbsp;to&nbsp;implement&nbsp;the&nbsp;PowaPINObserver&nbsp;protocol&nbsp;in&nbsp;the&nbsp;header&nbsp;file:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c77d191585784" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">@interface&nbsp;PPPowaPINSampleViewController&nbsp;:&nbsp;UIViewController&nbsp;&lt;PowaPINObserver&nbsp;&gt;&nbsp;
{&nbsp;
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c77d191585784-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c77d191585784-2">2</div><div class="crayon-num" data-line="crayon-55f09b483c77d191585784-3">3</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c77d191585784-1"><span class="crayon-sy">@</span><span class="crayon-t">interface</span>&nbsp;<span class="crayon-i">PPPowaPINSampleViewController</span>&nbsp;<span class="crayon-o">:</span>&nbsp;<span class="crayon-e">UIViewController</span>&nbsp;<span class="crayon-o">&lt;</span><span class="crayon-e">PowaPINObserver</span>&nbsp;<span class="crayon-o">&gt;</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c77d191585784-2"><span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09b483c77d191585784-3"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>Implement&nbsp;the&nbsp;protocol&nbsp;method&nbsp;that&nbsp;will&nbsp;be&nbsp;evoked&nbsp;when&nbsp;a&nbsp;transaction&nbsp;is&nbsp;finished&nbsp;by&nbsp;the&nbsp;SDK:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b483c781799516707" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">‐&nbsp;(void)powaPINFinishedTransaction:(PowaPIN&nbsp;*)powaPIN&nbsp;
{&nbsp;
//&nbsp;Do&nbsp;something&nbsp;
}</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b483c781799516707-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c781799516707-2">2</div><div class="crayon-num" data-line="crayon-55f09b483c781799516707-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b483c781799516707-4">4</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b483c781799516707-1">‐&nbsp;<span class="crayon-sy">(</span><span class="crayon-t">void</span><span class="crayon-sy">)</span><span class="crayon-v">powaPINFinishedTransaction</span><span class="crayon-o">:</span><span class="crayon-sy">(</span><span class="crayon-i">PowaPIN</span>&nbsp;<span class="crayon-o">*</span><span class="crayon-sy">)</span><span class="crayon-e">powaPIN</span>&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c781799516707-2"><span class="crayon-sy">{</span>&nbsp;</div><div class="crayon-line" id="crayon-55f09b483c781799516707-3"><span class="crayon-c">//&nbsp;Do&nbsp;something&nbsp;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b483c781799516707-4"><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0005 seconds] -->
<p>&nbsp;</p>',
                'product_id' => '3',
                'platform_id' => '2',
            ],
            [
                'name' => 'Setting up the PowaPIN in Android',
                'content' => '<p><b>1) Setting &nbsp;up &nbsp;the &nbsp;Project</b></p>
<p>The initial tasks to get the SDK integrated are those to create an Android project referencing the SDK libraries. This can be achieved in several ways, for example by moving the JAR files to the libs folder.</p>
<p><b>2) Creating &nbsp;a &nbsp;Simple &nbsp;Application</b></p>
<p>The following sections dive into the details of the Application design and how it makes use of the SDK functionality. The SDK requires some initial configuration to adapt it to the target project. The PowaPIN driver used in this sample application also requires some code to make it work namely to allow for device discovery over Bluetooth and device explicit binding to the SDK and mobile application. The following sections show a step-­‐by-­‐step PowaPOS SDK implementation.</p>
<p><b>2.1) Add &nbsp;Permissions</b></p>
<p>Adding permissions is done on the Android manifest file.</p>
<p>The following statements declare the User permissions to allow Bluetooth connections:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9cf0110616294" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">&lt;!-- The PowaPIN service requires Bluetooth permissions. --&gt;
&lt;uses-permission android:name="android.permission.BLUETOOTH"/&gt;
&lt;!-- Managing PowaPIN connections requires Bluetooth Admin permissions. --&gt;
&lt;uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9cf0110616294-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9cf0110616294-2">2</div><div class="crayon-num" data-line="crayon-55f09b5de9cf0110616294-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9cf0110616294-4">4</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9cf0110616294-1"><span class="crayon-o">&lt;</span><span class="crayon-o">!</span><span class="crayon-o">--</span><span class="crayon-h"> </span><span class="crayon-e">The </span><span class="crayon-e">PowaPIN </span><span class="crayon-e">service </span><span class="crayon-e">requires </span><span class="crayon-e">Bluetooth </span><span class="crayon-v">permissions</span><span class="crayon-sy">.</span><span class="crayon-h"> </span><span class="crayon-o">--</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9cf0110616294-2"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">permission </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.permission.BLUETOOTH"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09b5de9cf0110616294-3"><span class="crayon-o">&lt;</span><span class="crayon-o">!</span><span class="crayon-o">--</span><span class="crayon-h"> </span><span class="crayon-e">Managing </span><span class="crayon-e">PowaPIN </span><span class="crayon-e">connections </span><span class="crayon-e">requires </span><span class="crayon-e">Bluetooth </span><span class="crayon-e">Admin </span><span class="crayon-v">permissions</span><span class="crayon-sy">.</span><span class="crayon-h"> </span><span class="crayon-o">--</span><span class="crayon-o">&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9cf0110616294-4"><span class="crayon-o">&lt;</span><span class="crayon-v">uses</span><span class="crayon-o">-</span><span class="crayon-e">permission </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"android.permission.BLUETOOTH_ADMIN"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0013 seconds] -->
<p>&nbsp;</p>
<p>The following statements declare the PowaPIN service:</p>
<p>&nbsp;</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9cfb813232237" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">&lt;application&gt;
&lt;!-- Include the PowaPIN Service. --&gt;
&lt;service android:name="com.mpowa.android.powapin.PowaPinService"
android:exported="false"/&gt;
&lt;/application&gt;</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9cfb813232237-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9cfb813232237-2">2</div><div class="crayon-num" data-line="crayon-55f09b5de9cfb813232237-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9cfb813232237-4">4</div><div class="crayon-num" data-line="crayon-55f09b5de9cfb813232237-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9cfb813232237-1"><span class="crayon-e ">&lt;application&gt;</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9cfb813232237-2"><span class="crayon-o">&lt;</span><span class="crayon-o">!</span><span class="crayon-o">--</span><span class="crayon-h"> </span><span class="crayon-e">Include </span><span class="crayon-e">the </span><span class="crayon-e">PowaPIN </span><span class="crayon-v">Service</span><span class="crayon-sy">.</span><span class="crayon-h"> </span><span class="crayon-o">--</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09b5de9cfb813232237-3"><span class="crayon-o">&lt;</span><span class="crayon-e">service </span><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">name</span><span class="crayon-o">=</span><span class="crayon-s">"com.mpowa.android.powapin.PowaPinService"</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9cfb813232237-4"><span class="crayon-v">android</span><span class="crayon-o">:</span><span class="crayon-v">exported</span><span class="crayon-o">=</span><span class="crayon-s">"false"</span><span class="crayon-o">/</span><span class="crayon-o">&gt;</span></div><div class="crayon-line" id="crayon-55f09b5de9cfb813232237-5"><span class="crayon-o">&lt;</span><span class="crayon-o">/</span><span class="crayon-v">application</span><span class="crayon-o">&gt;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0008 seconds] -->
<p>&nbsp;</p>
<p><b>2.2) Hooking up the general SDK Events</b></p>
<p>During the processing of an operation, the SDK informs the application of specific events by firing event handlers that execute application code. This allows the mobile application to be asynchronously informed regarding the transaction progress. Regarding SDK Events a PowaSDKCallback object is required and therefore needs to be created. This object, as the PEDCallback and ServerCallback defined in the two next sections, are abstract classes. The advantage with this implementation is that the user only needs to override the methods that are going to be used. For example purposes, only the behaviour for the method onReady is shown below. For a more comprehensive list of available methods, please check the document PowaPIN SDK API Specification. The following code illustrates how the application defines the event handlers and hooks them in the SDK:</p>
<p>&nbsp;</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d01264104456" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaSDKCallback  sdkCallback  =  new  PowaSDKCallback  ()  {
@Override
        public  void  onReady()  {
       //the  sdk  is  ready  to  use  and  its  method  can  be  called  by  the  client 
        }</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d01264104456-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d01264104456-2">2</div><div class="crayon-num" data-line="crayon-55f09b5de9d01264104456-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d01264104456-4">4</div><div class="crayon-num" data-line="crayon-55f09b5de9d01264104456-5">5</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d01264104456-1"><span class="crayon-e">PowaSDKCallback&nbsp;&nbsp;</span><span class="crayon-v">sdkCallback</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-o">=</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-r">new</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-e">PowaSDKCallback</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d01264104456-2"><span class="crayon-n">@Override</span></div><div class="crayon-line" id="crayon-55f09b5de9d01264104456-3"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-m">public</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-t">void</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-e">onReady</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-sy">{</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d01264104456-4"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="crayon-c">//the&nbsp;&nbsp;sdk&nbsp;&nbsp;is&nbsp;&nbsp;ready&nbsp;&nbsp;to&nbsp;&nbsp;use&nbsp;&nbsp;and&nbsp;&nbsp;its&nbsp;&nbsp;method&nbsp;&nbsp;can&nbsp;&nbsp;be&nbsp;&nbsp;called&nbsp;&nbsp;by&nbsp;&nbsp;the&nbsp;&nbsp;client </span></div><div class="crayon-line" id="crayon-55f09b5de9d01264104456-5"><span class="crayon-h">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="crayon-sy">}</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0007 seconds] -->
<p>&nbsp;</p>
<p><b>2.3) Setting up the PED</b></p>
<p>PED is an interface defining the set of functionalities expected in a generic PED device. As of today, we are using the PowaPIN device (that is an implementation of the PED interface for the PowaPIN device). The initialisation of the PowaPinPED is as follows:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d05701258580" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPinPED  ped  =  new  PowaPinPED(this,  pedCallback);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d05701258580-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d05701258580-1"><span class="crayon-e">PowaPinPED&nbsp;&nbsp;</span><span class="crayon-v">ped</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-o">=</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-r">new</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-e">PowaPinPED</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h">&nbsp;&nbsp;</span><span class="crayon-v">pedCallback</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p><b>2.4) Setting up the Server</b></p>
<p>The SDK API as described in [1] provides access to the SDK functionalities without worrying about Server communications. Depending on the chosen integration model, the Server connection can be provided by Powa Technologies as a dedicated Server Abstraction component effectively liberating the developers to focus exclusively on the Mobile Application interface design and construction without worrying about connectivity.</p>
<p>If the integration model does not include a Powa dedicated server connection, the developer has to extend the provided SDK Server Abstraction Interface and write the connectivity code to reach a custom server.</p>
<p>The PowaSimulationServer class is an example of how to implement a server abstraction. For this setup we are going to use the PowaSimulationServer to achieve an End-­‐to-­‐End simulated transaction.</p>
<p>For this purpose we start by instantiating the PowaSimulationServer with the following code:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d0b931590933" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div><span class="crayon-language">Java</span></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaSimulationServer server = new PowaSimulationServer (this, serverCallback);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d0b931590933-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d0b931590933-1"><span class="crayon-e">PowaSimulationServer </span><span class="crayon-v">server</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaSimulationServer</span><span class="crayon-h"> </span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">serverCallback</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p></p>
<div></div>
<p>&nbsp;</p>
<p><b>2.5) Setting up the SDK</b></p>
<p>The first step is to obtain a reference for the PowaPosSDK object. This object is used to invoke the actions in the device, set it to use and setup the server. To create an object we simply execute:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d0f614252146" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">PowaPIN powaPIN = new PowaPIN(this, ped, server);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d0f614252146-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d0f614252146-1"><span class="crayon-e">PowaPIN </span><span class="crayon-v">powaPIN</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaPIN</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">ped</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">server</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0005 seconds] -->
<p>The object created above becomes useful when associated to a PED and a Server to be able to conduct an End-to-End payment transaction.</p>
<p><b>2.6) Merging all structures</b></p>
<p>At this stage all the necessary structures to use the PowaPIN object are build. The last action is to hook the PED, the Server and the callbacks with the PowaPIN. For this purpose we use the following code:</p>
<p>&nbsp;</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d14984637853" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">// Create PED and set callback.
PowaPinPED ped = new PowaPinPED(this, pedCallbacks);

// Create Server and set callback
PowaSimulationServer server = new PowaSimulationServer(this, serverCallbacks); 

//Create the SDK and initialize it with the PED and the Server
PowaPIN powaPIN = new PowaPIN(this, ped, server);

//Start the SDK and set callback
powaPIN.initializeSDK(sdkCallback);</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-1">1</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d14984637853-2">2</div><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-3">3</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d14984637853-4">4</div><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-5">5</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d14984637853-6">6</div><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-7">7</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d14984637853-8">8</div><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-9">9</div><div class="crayon-num crayon-striped-num" data-line="crayon-55f09b5de9d14984637853-10">10</div><div class="crayon-num" data-line="crayon-55f09b5de9d14984637853-11">11</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d14984637853-1"><span class="crayon-c">// Create PED and set callback.</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d14984637853-2"><span class="crayon-e">PowaPinPED </span><span class="crayon-v">ped</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaPinPED</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">pedCallbacks</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09b5de9d14984637853-3">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d14984637853-4"><span class="crayon-c">// Create Server and set callback</span></div><div class="crayon-line" id="crayon-55f09b5de9d14984637853-5"><span class="crayon-e">PowaSimulationServer </span><span class="crayon-v">server</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaSimulationServer</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">serverCallbacks</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span><span class="crayon-h"> </span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d14984637853-6">&nbsp;</div><div class="crayon-line" id="crayon-55f09b5de9d14984637853-7"><span class="crayon-c">//Create the SDK and initialize it with the PED and the Server</span></div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d14984637853-8"><span class="crayon-e">PowaPIN </span><span class="crayon-v">powaPIN</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-r">new</span><span class="crayon-h"> </span><span class="crayon-e">PowaPIN</span><span class="crayon-sy">(</span><span class="crayon-r">this</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">ped</span><span class="crayon-sy">,</span><span class="crayon-h"> </span><span class="crayon-v">server</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div><div class="crayon-line" id="crayon-55f09b5de9d14984637853-9">&nbsp;</div><div class="crayon-line crayon-striped-line" id="crayon-55f09b5de9d14984637853-10"><span class="crayon-c">//Start the SDK and set callback</span></div><div class="crayon-line" id="crayon-55f09b5de9d14984637853-11"><span class="crayon-v">powaPIN</span><span class="crayon-sy">.</span><span class="crayon-e">initializeSDK</span><span class="crayon-sy">(</span><span class="crayon-v">sdkCallback</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0014 seconds] -->
<p>&nbsp;</p>
<div></div>
<p>&nbsp;</p>
<p><b>2.7) Invoking the SDK API Methods</b></p>
<p>As described above, the SDK is controlled by method evocation to perform specific actions. In this example, the actions are triggered by controls present on the PowaPIN object. In the following example a search for devices is initiated:</p><!-- Crayon Syntax Highlighter v2.7.1 -->

        <div id="crayon-55f09b5de9d19229985758" class="crayon-syntax crayon-theme-github crayon-font-monaco crayon-os-pc print-yes notranslate" data-settings=" minimize scroll-mouseover" style=" margin-top: 12px; margin-bottom: 12px; font-size: 12px !important; line-height: 15px !important;">
        
            <div class="crayon-toolbar" data-settings=" mouseover overlay hide delay" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><span class="crayon-title"></span>
            <div class="crayon-tools" style="font-size: 12px !important;height: 18px !important; line-height: 18px !important;"><div class="crayon-button crayon-nums-button" title="Toggle Line Numbers"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-plain-button" title="Toggle Plain Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-wrap-button" title="Toggle Line Wrap"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-expand-button" title="Expand Code"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-copy-button" title="Copy"><div class="crayon-button-icon"></div></div><div class="crayon-button crayon-popup-button" title="Open Code In New Window"><div class="crayon-button-icon"></div></div></div></div>
            <div class="crayon-info" style="min-height: 16.8px !important; line-height: 16.8px !important;"></div>
            <div class="crayon-plain-wrap"><textarea wrap="soft" class="crayon-plain print-no" data-settings="dblclick" readonly="" style="-moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4; font-size: 12px !important; line-height: 15px !important;">List&lt;PowaDeviceInfo&gt; devices = powaPosSDK.getAvailablePEDs();</textarea></div>
            <div class="crayon-main" style="">
                <table class="crayon-table">
                    <tbody><tr class="crayon-row">
                <td class="crayon-nums " data-settings="show">
                    <div class="crayon-nums-content" style="font-size: 12px !important; line-height: 15px !important;"><div class="crayon-num" data-line="crayon-55f09b5de9d19229985758-1">1</div></div>
                </td>
                        <td class="crayon-code"><div class="crayon-pre" style="font-size: 12px !important; line-height: 15px !important; -moz-tab-size:4; -o-tab-size:4; -webkit-tab-size:4; tab-size:4;"><div class="crayon-line" id="crayon-55f09b5de9d19229985758-1"><span class="crayon-v">List</span><span class="crayon-o">&lt;</span><span class="crayon-v">PowaDeviceInfo</span><span class="crayon-o">&gt;</span><span class="crayon-h"> </span><span class="crayon-v">devices</span><span class="crayon-h"> </span><span class="crayon-o">=</span><span class="crayon-h"> </span><span class="crayon-v">powaPosSDK</span><span class="crayon-sy">.</span><span class="crayon-e">getAvailablePEDs</span><span class="crayon-sy">(</span><span class="crayon-sy">)</span><span class="crayon-sy">;</span></div></div></td>
                    </tr>
                </tbody></table>
            </div>
        </div>
<!-- [Format Time: 0.0004 seconds] -->
<p>The action returns all detected Bluetooth devices.</p>
<p>&nbsp;</p>
<p><b>3) Performing a Simple Sale Operation</b><b>Performing &nbsp;a &nbsp;Simple &nbsp;Sale &nbsp;Operation</b></p>
<p>After finishing the Payment application definition and SDK integration, a compile-ready application should be complete and fully usable. The following sections provide step-by-step guidance to perform a Sale operation using the PowaPIN device and the provided Sample Application.</p>
<p>&nbsp;</p>',
                'product_id' => '3',
                'platform_id' => '1',
            ],

        ];

        foreach ($tutorials as $tutorial) {
            $tutorial['slug'] = Str::slug($tutorial['name']);
            Tutorial::create($tutorial);
        }

    }

}