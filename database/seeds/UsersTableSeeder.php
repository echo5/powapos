<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use PPDevPortal\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
	{
        
		$users = [
            [
                'id'       => '1',
                'first_name' => 'Joshua',
                'last_name' => 'SuperAdmin',
                'email' => 'joshua@echo5webdesign.com',
                'password' => 'echo5p455',
                'country_id' => '233',
                'confirmed' => '1',
            ],
            [
                'id'       => '2',
                'first_name' => 'Joshua',
                'last_name' => 'Admin',
                'email' => 'joshuatf@gmail.com',
                'password' => 'echo5p455',
                'country_id' => '233',
                'confirmed' => '1',
            ],
            [
                'id'       => '3',
                'first_name' => 'Joshua',
                'last_name' => 'Developer',
                'email' => 'joshua@developer.com',
                'password' => 'echo5p455',
                'country_id' => '233',
                'confirmed' => '1',
            ],
            [
                'id'       => '4',
                'first_name' => 'Thomas',
                'last_name' => 'Clayson',
                'email' => 'thomasclayson@powa.com',
                'password' => 'thomasp455',
                'country_id' => '77',
                'confirmed' => '1',
            ],
            [
                'id'       => '5',
                'first_name' => 'Candace',
                'last_name' => 'McCaffery',
                'email' => 'candacemccaffery@powa.com',
                'password' => 'candacep455',
                'country_id' => '233',
                'confirmed' => '1',
            ],
        ];

        $roles = array();
        $roles[1] = '3';
        $roles[2] = '2';
        $roles[3] = '1';
        $roles[4] = '3';
        $roles[5] = '3';

        $n = 1;
        foreach ($users as $user) {
            $user = User::create($user);
            $user->attachRole($roles[$n]);
            $n++;
        }

	}

}