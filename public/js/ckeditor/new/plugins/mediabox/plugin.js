CKEDITOR.plugins.add( 'mediabox', {
    requires: 'widget',

    icons: 'mediabox',

    init: function( editor ) {
        editor.widgets.add( 'mediabox', {

            button: 'Create a media item',

            template:
                '<div class="mediabox">' +
                    '<div class="mediabox-img"><img src="/js/ckeditor/plugins/mediabox/icons/mediabox.png" /></div>' +
                    '<div class="mediabox-content"><p>Name</p></div>' +
                '</div>',

            editables: {
                image: {
                    selector: '.mediabox-img',
                },
                content: {
                    selector: '.mediabox-content',
                    allowedContent: 'p br ul ol li strong em'
                }
            },

            allowedContent:
                'div(!mediabox); div(!mediabox-content); div(!mediabox-img)',

            requiredContent: 'div(mediabox)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'mediabox' );
            }
        } );
    }
} );