CKEDITOR.plugins.add( 'templatebox', {
    requires: 'widget',

    icons: 'templatebox',

    init: function( editor ) {
        editor.widgets.add( 'templatebox', {

            button: 'Create a template or logo item',

            template:
                '<div class="templatebox">' +
                    '<div class="templatebox-img"><img src="/js/ckeditor/plugins/templatebox/icons/templatebox.png" /></div>' +
                    '<div class="templatebox-content"><p>Template Name</p></div>' +
                '</div>',

            editables: {
            	image: {
            	    selector: '.templatebox-img',
            	},
                content: {
                    selector: '.templatebox-content',
                    allowedContent: 'p br ul ol li strong em'
                }
            },

            allowedContent:
                'div(!templatebox); div(!templatebox-content); h2(!templatebox-img)',

            requiredContent: 'div(templatebox)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'templatebox' );
            }
        } );
    }
} );