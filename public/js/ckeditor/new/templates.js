// Register a template definition set named "default".
CKEDITOR.addTemplates( 'default',
{
	// The name of the subfolder that contains the preview images of the templates.
	imagesPath :'img/ckeditor/' ,
 
	// Template definitions.
	templates :
		[
			{
				title: 'Media Item',
				// image: 'template1.gif', // Goes to imagesPath/template1.gif
				description: 'Add image and text for Photography & Video page.',
				html:
					'<div class="media-item">' +
					'<img src=" ">' +
					'<p><img src="/logo.png" style="float:left" />Type media name here.</p>' +
					'</div>'
			},
		]
});