CKEDITOR.plugins.add( 'sectionbox', {
    requires: 'widget',

    icons: 'sectionbox',

    init: function( editor ) {
        editor.widgets.add( 'sectionbox', {

            button: 'Create a section with a background',

            template:
                '<div class="section">[section padding=150]' +
                    '<div class="section-bg"><img id="section-bg-img" src="/img/powapos-overview.jpg" /></div>' +
                    '<div class="section-content"><h1 style="color:red;">Your header here</h1><p>Tagline goes here</p></div>' +
                '[/section]</div>',

            editables: {
                image: {
                    selector: '.section-bg',
                },
                content: {
                    selector: '.section-content',
                    allowedContent: 'p br ul ol li strong em div h1 h2 h3 h4 h5 h6 span style'
                }
            },

            allowedContent:
                'div(!section); div(!section-content); div(!section-bg)',

            requiredContent: 'div(sectionbox)',

            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'sectionbox' );
            }
        } );
    }
} );