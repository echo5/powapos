/**
 * Accordion
 */

$(document).ready(function() {
    function close_accordion_section() {
        $('.accordion .accordion-section-title').removeClass('active');
        $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }
 
    $('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).attr('href');
 
        if($(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();
 
            // Add active class to section title
            $(this).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        }
 
        e.preventDefault();
    });
});

/**
 * Date picker
 */
$(document).ready(function() {
	$('.datepicker').datepicker({
	    format: 'mm/dd/yyyy',
	    // startDate: '-3d'
	});
});


/**
 * Filter products/sdks/tutorials form
 */

$(document).ready(function() {
	$( "#sort-form" ).submit(function( event ) {
		event.preventDefault();
		var path = $(this).attr('action');
		var selects = $(this).find('select');
		selects.each(function() {
			path += '/' + $(this).val();
		});
		window.location.href = path;
	});
});

/**
 * Filter FAQ list
 */
function filterFaq(element) {
	var value = $(element).val();

	$("#faq-accordion .accordion-section").each(function() {
		// if ($(this).text().search(value) > -1) {
		// 	$(this).show();
		// }
		if ($(this).text().toLowerCase().search(value.toLowerCase()) > -1) {
			$(this).show();
		}
		else {
			$(this).hide();
		}
	});
}

function clearFilter(element) {
	$("#faq-accordion .accordion-section").show();
	$(element).val('');
}

$(document).ready(function() {
	$('#faq-search').keyup(function(e) {
		filterFaq($(this));
	});
	$('#faq-clear-filter').click(function(e) {
		e.preventDefault();
		clearFilter('#faq-search');
	});
});


/**
 * Checklists
 */

$(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'fa fa-check-square-o'
                },
                off: {
                    icon: 'fa fa-square-o'
                }
            };
            
        $widget.css('cursor', 'pointer');
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.find('.checklist-item-name').prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});