@extends('layouts.frontend.minimal_brand')

@section('title') Account @stop

@section('content')

	<div class="alert alert-info" role="alert">Change your account details.</div>

	<div class="well">
		
		{!! Form::model($user, [
			
			'role' 		=> 'form',
			'url'		=> 'account',
			'method'	=> 'put'
			
		]) !!}
		
			<div class="form-group">
				{!! Form::label('first_name', 'First Name') !!}
				{!! Form::text('first_name', NULL, ['class' => 'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('last_name', 'Last Name') !!}
				{!! Form::text('last_name', NULL, ['class' => 'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('password', 'New Password') !!}
				{!! Form::password('password', ['class' => 'form-control']) !!}
				<p class="help-block">Leave blank for no change.</p>
			</div>
		
			<div class="form-group">
				{!! Form::label('password_confirmation', 'Password Confirmation') !!}
				{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::submit('Update Account Details', ['class' => 'btn btn-primary']) !!}
			</div>
				
		{!! Form::close() !!}
			
	</div>
	
<div class="row">
	<div class="col-xs-12">
		<a href="{{ url('/') }}">
			<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Return to developer portal
		</a>
	</div>
</div>

@stop