@extends('layouts.frontend.minimal_brand')

@section('title') Login @stop

@section('content')

@if(Session::get('extra'))
	<div class="alert alert-info" role="alert">

		{{-- User has tried to log in with an unconfirmed account --}}
		@if(Session::get('extra') == "need.confirmation")
			<p><strong>Account not confirmed</strong></p>
			<p>Please confirm your email address by following the instructions in the confirmation email.</p>
		
		{{-- User has completed registration and confirmation email has been sent --}}
		@elseif(Session::get('extra') == "sent.confirmation")
			<p><strong>Confirm email to continue</strong></p>
			<p>Please check your email and follow the verification link to complete the process.</p>
		@endif
		
		<p>Not received the confirmation email? <a href="{{ url('auth/confirmation/'.old('email')) }}">Click here to resend</a>
		</p>
	</div>
@endif
	    
<div class="well">

	<form role="form" method="POST" action="{{ url('/auth/login') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label class="control-label">E-Mail Address</label>
			<input type="email" class="form-control" name="email" value="{{ old('email') }}">
		</div>

		<div class="form-group">
			<label class="control-label">Password</label>
			<input type="password" class="form-control" name="password">
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="checkbox col-xs-8 col-md-7 col-lg-8">
		        	<label>
		            	{!! Form::checkbox('remember', 1, NULL) !!} Remember Me
		            </label>
		        </div>
		        <div class="col-xs-4 col-md-5 col-lg-4">
		        	{!! Form::submit('Login', ['class' => 'btn btn-primary btn-block']) !!}
		        </div>
			</div>
		</div>
	</form>
    
</div>

<div class="row">
	<div class="col-xs-12">
		<a href="{{ url('auth/register') }}">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Register
		</a>
		&nbsp;|&nbsp;
		<a href="{{ url('password/email') }}">
			Forgotten password?
		</a>
	</div>
</div>

@endsection
