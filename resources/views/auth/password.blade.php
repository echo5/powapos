@extends('layouts.frontend.minimal_brand')

@section('title') Register @stop

@section('content')

	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@else
		<div class="alert alert-info" role="alert">Reset your password.</div>
	@endif

	<div class="well">
		
		<form role="form" method="POST" action="{{ url('/password/email') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
			<div class="form-group">
				<label class="control-label">E-Mail Address</label>
				<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-xs-4 col-xs-offset-8 col-md-5 col-md-offset-7 col-lg-4 col-lg-offset-8">
			        	{!! Form::submit('Reset', ['class' => 'btn btn-primary btn-block']) !!}
			        </div>
				</div>
			</div>
		</form>
		
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<a href="{{ url('auth/login') }}">
				<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Login
			</a>
		</div>
	</div>
	
@endsection