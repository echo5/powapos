@extends('layouts.frontend.minimal_brand')

@section('title') Register @stop

@section('content')

<div class="alert alert-info" role="alert">Register for this site.</div>
	    
<div class="well">
	
    <form role="form" method="POST" action="{{ url('/auth/register') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label class="control-label">First Name</label>
			<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
		</div>

		<div class="form-group">
			<label class="control-label">Last Name</label>
			<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
		</div>

		<div class="form-group">
			<label class="control-label">E-Mail Address</label>
			<input type="email" class="form-control" name="email" value="{{ old('email') }}">
		</div>
		
		<hr />

		<div class="form-group">
			<label class="control-label">Password</label>
			<input type="password" class="form-control" name="password">
		</div>

		<div class="form-group">
			<label class="control-label">Confirm Password</label>
			<input type="password" class="form-control" name="password_confirmation">
		</div>
		
		<hr />

		<div class="form-group">
			<label class="control-label">Company</label>
			<input type="text" class="form-control" name="company" value="{{ old('company') }}">
		</div>

		<div class="form-group">
			<label class="control-label">Job Title</label>
			<input type="text" class="form-control" name="job_title" value="{{ old('job_title') }}">
		</div>

		<div class="form-group">
			<label class="control-label">Phone Number</label>
			<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
		</div>

		<div class="form-group">
			<label class="control-label">Country</label>
			{!! Form::select('country', $countries, NULL, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-6 col-md-offset-6">
		        	{!! Form::submit('Register', ['class' => 'btn btn-primary btn-block']) !!}
		        </div>
			</div>
		</div>
	</form>

</div>

<div class="row">
	<div class="col-xs-12">
		<a href="{{ url('auth/login') }}">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Login
		</a>
		&nbsp;|&nbsp;
		<a href="{{ url('password/email') }}">
			Forgotten password?
		</a>
	</div>
</div>
	
@endsection
