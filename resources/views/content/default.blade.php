@extends('layouts.frontend.fixed')

@section('title') {{ $page->title }} @endsection

@section('body_attributes') class="{{ $page->cssClasses }}" @endsection

@section('header')

	@include('partials.header', ['title'=> $page->title ])
	
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		{!! $page->content !!}
	</div>
</div>

@stop