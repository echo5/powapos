@extends('content.default')

@section('content')

<div class="row">
	<div class="col-md-12">
		{!! $page->content !!}
	</div>
	<div class="col-md-12">
		@include('modules.checklists')
	</div>
	</div><!-- end .row -->
	</div><!-- end .col-xs-12 -->
	</div><!-- end .row -->
	</div><!-- end .container -->
	<div class="container-checklist">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('modules.guides')
				</div>
			</div>		
		</div>
	</div>

</div>

@stop