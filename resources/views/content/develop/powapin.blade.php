@extends('content.default')

@section('content')
<div class="row">
	<div class="col-md-3">
		@include('partials.menu.menubar_side', [
			'menu_items' => [
				[
					'url' => '/sdk-downloads/powapin',
					'title' => 'SDK Downloads',
					'icon' => 'download'
				],
				[
					'url' => '/demo-apps/powapin',
					'title' => 'Demo Apps',
					'icon' => 'mobile'
				],
				[
					'url' => '/tutorials/powapin',
					'title' => 'Tutorials',
					'icon' => 'question'
				],
			],
		])
	</div>
	<div class="col-md-8">
		{!! $page->content !!}
	</div>
</div>

@stop