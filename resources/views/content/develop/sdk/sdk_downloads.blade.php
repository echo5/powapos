@extends('layouts.frontend.fixed')

@section('title') {{ $sdk->name }} @endsection

@section('header')

	@include('partials.header', ['title'=> $sdk->name ])
	
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		@include('modules.sdk_downloads')
	</div>
</div>

@stop