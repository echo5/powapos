@extends('content.default')

@section('content')
<div class="row">
	<div class="col-md-3">
		@include('partials.menu.menubar_side', [
			'menu_items' => [
				[
					'url' => '/sdk-downloads/t-series',
					'title' => 'SDK Downloads',
					'icon' => 'download'
				],
				[
					'url' => '/demo-apps/t-series',
					'title' => 'Demo Apps',
					'icon' => 'mobile'
				],
				[
					'url' => '/tutorials/t-series',
					'title' => 'Tutorials',
					'icon' => 'question'
				],
				[
					'url' => '/powapos-tools/t-series',
					'title' => 'PowaPOS Tools',
					'icon' => 'wrench'
				],
				[
					'url' => '/develop/t-series/supported-tablets',
					'title' => 'Supported Tablets',
					'icon' => 'tablet'
				],
				[
					'url' => '/develop/t-series/mfi',
					'title' => 'MFi',
					'icon' => 'apple'
				],
			],
		])
	</div>
	<div class="col-md-8">
		{!! $page->content !!}
	</div>
</div>

@stop