@extends('layouts.frontend.fixed')

@section('title') {{ $page->title }} @endsection

@section('header')

	@include('partials.header', ['title'=> $page->title ])
	
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		{!! $page->content !!}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		@if ($errors->has())
			<div class="alert alert-danger">
			    There were errors with your submission.
			</div>
		@endif
	</div>
</div>

	{!! Form::open(array('route' => 'mfi.submit', 'class'=>'mfi-form')) !!}
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('name')) has-error @endif">
					{!! Form::label('name', 'Application Name') !!}
			    	{!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('version')) has-error @endif">
					{!! Form::label('version', 'Version Number') !!}
			    	{!! Form::text('version', Input::old('version'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('version', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('release_date')) has-error @endif">
					{!! Form::label('release_date', 'Release Date') !!}
			    	{!! Form::text('release_date', Input::old('release_date'), ['class' => 'form-control datepicker']) !!}
			    	{!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('app_category')) has-error @endif">
					{!! Form::label('app_category', 'App Store Category') !!}
			    	{!! Form::text('app_category', Input::old('app_category'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('app_category', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('bundle_id')) has-error @endif">
					{!! Form::label('bundle_id', 'Bundle Identifier') !!}
			    	{!! Form::text('bundle_id', Input::old('bundle_id'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('bundle_id', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('developer_name')) has-error @endif">
					{!! Form::label('developer_name', 'Developer Name') !!}
			    	{!! Form::text('developer_name', Input::old('developer_name'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('developer_name', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="input-group @if ($errors->has('description')) has-error @endif">
					{!! Form::label('description', 'Application Functional Description') !!}
			    	{!! Form::textarea('description', Input::old('description'), ['class' => 'form-control']) !!}
			    	{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('products')) has-error @endif">
					{!! Form::label('products', 'Products') !!}<br/>
					@foreach ($products as $product)	
				    	{!! Form::checkbox('products[]', $product->name) !!} {{ $product->name }}<br/>
					@endforeach
				</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('existing')) has-error @endif">
					{!! Form::label('existing', 'New or Existing App') !!}<br/>
			    	{!! Form::radio('existing', '0') !!} The app is new on the app store. <br/>
			    	{!! Form::radio('existing', '1') !!} This is an update to an existing app. 
			    	{!! $errors->first('existing', '<p class="help-block">:message</p>') !!}
			   	</div>
			</div>
			<div class="col-lg-4">
				<div class="input-group @if ($errors->has('dependencies')) has-error @endif">
					{!! Form::label('dependencies', 'Dependence of T25') !!}<br/>
					{!! Form::radio('dependencies', 'The app only works with a T25.') !!} The app only works with a T25.<br/>
					{!! Form::radio('dependencies', 'The app works independently of a T25.') !!} The app works independently of a T25.
			    	{!! $errors->first('dependencies', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
			    {!! Form::submit('Submit') !!}
			</div>
		</div>
	{!! Form::close() !!}
</div>

@stop