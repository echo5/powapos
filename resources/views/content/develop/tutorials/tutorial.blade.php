@extends('layouts.frontend.fixed')

@section('title') {{ $tutorial->name }} @endsection

@section('header')

	@include('partials.header', ['title'=> $tutorial->name ])
	
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		@include('modules.tutorial')
	</div>
</div>

@stop