@extends('layouts.frontend.fixed')

@section('title') {{ $page->title }} @endsection

@section('body_attributes') class="{{ $page->cssClasses }}" @endsection


@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! $page->content !!}
		</div>
	</div>
@endsection