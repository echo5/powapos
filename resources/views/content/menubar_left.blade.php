@extends('content.default')

@section('content')

<div class="row">
	<div class="col-md-3">
		@include('partials.menu.menubar_side')
	</div>
	<div class="col-md-8">
		{!! $page->content !!}
	</div>
</div>

@stop