@extends('layouts.frontend.fixed')

@section('title') {{ $page->title }} @endsection

@section('body_attributes') class="{{ $page->cssClasses }}" @endsection

@section('header')

	@include('partials.header', ['title'=> $page->title ])
	
@stop

@section('content')

<div class="row">
	<div class="col-md-6 text-center content-icon">
		<span class="fa-stack icon-huge">
		  <i class="fa fa-circle fa-stack-2x icon-red"></i>
		  <i class="fa fa-ticket fa-stack-1x fa-inverse icon-white icon-red-shadow"></i>
		</span>
		<h3>Support Tickets</h3>
		<p>If you have a non-urgent issue you can get in touch with us via our support ticket platform.</p>
		<p>{!! link_to('support/tickets', 'Support Tickets') !!}</p>
	</div>
	<div class="col-md-6 text-center content-icon">
		<span class="fa-stack icon-huge">
		  <i class="fa fa-circle fa-stack-2x icon-purple"></i>
		  <i class="fa fa-phone fa-stack-1x fa-inverse icon-white icon-purple-shadow"></i>
		</span>
		<h3>Call Us</h3>
		<p>If you have an urgent enquiry please give us a call.</p>
		<p><strong>+1 678-348-6155‎, Option #2‏</strong></p>
	</div>
</div>

@stop