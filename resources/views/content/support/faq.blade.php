@extends('content.default')

@section('content')

<div class="row">
	<div class="col-md-12">
		{!! $page->content !!}
	</div>
	<div class="col-md-12">
		@include('modules.faq')
	</div>
</div>

@stop