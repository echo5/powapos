<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	{{-- We leave the head empty as gmail removes it completely --}}
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>
        <style></style>
    </head>
    <body style="background: rgb(241, 241, 241);">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background: rgb(241, 241, 241);">
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="5" cellspacing="0" width="600" id="emailContainer">
                    	{{-- Header --}}
                    	<tr>
                    		<td align="center" valign="top">
                    			<table border="0" cellpadding="15" cellspacing="0" width="100%" id="logoContainer">
                    				<tr>
                    					<td align="center" valign="top">
	                    					<a href="{{ url('/') }}" style="border:0;">{!! HTML::image('img/powapos_logo_login.png') !!}</a>
                    					</td>
                    				</tr>
                    			</table>
                    		</td>
                    	</tr>
                        <tr>
                            <td align="center" valign="top">
                            	<table border="0" cellpadding="20" cellspacing="0" width="100%" id="contentContainer" style="background: #fff;-webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);box-shadow: 0 1px 3px rgba(0,0,0,.13);border: 1px solid #e3e3e3;border-radius: 4px;font-family: helvetica, arial, sans-serif;font-size: 12px;">
                            		<tr>
                            			<td>
	                            			@yield('content')
                            			</td>
                            		</tr>
                            	</table>
                            </td>
                        </tr>
                        <tr>
                        	<td align="center" valign="top" style="font-family: helvetica, arial, sans-serif;font-size:12px;">
                        		<p><small>This message was sent on {{ date("l jS F Y h:i A") }}.</small></p>
                        	</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>