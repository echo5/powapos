@extends('layouts.frontend.minimal_brand')

@section('title') 401 Not Authorised @stop

@section('content')

<div class="well text-center">
	
	<p>Unfortunately you do not have the permissions to view this page.</p>
	
</div>

<div class="row">
	<div class="col-xs-12">
		<a href="{{ URL::previous() }}">
			<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back
		</a>
	</div>
</div>

@endsection