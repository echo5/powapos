@extends('layouts.frontend.minimal_brand')

@section('title') 404 Not Found @stop

@section('content')

<div class="well text-center">
	
	<p><strong>404 Not Found</strong></p>
	
	<p>The page you're looking for cannot be found.</p>
	
</div>

<div class="row">
	<div class="col-xs-12">
		<a href="{{ URL::previous() }}">
			<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back
		</a>
		&nbsp;|&nbsp;
		<a href="{{ url('/') }}">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home
		</a>
	</div>
</div>

@endsection