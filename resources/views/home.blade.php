@extends('layouts.frontend.fixed')

@section('title') Home @endsection

@section('header')

	<div class="home-pre-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h1>New SDKs released <small>Jan 2015</small></h1>
					<a href="#" class="btn btn-primary">Download Now</a>
				</div>
			</div>
		</div>
	</div>
	
@stop



@section('content')

<div class="row">
	<div class="col-md-4 text-center content-icon">
		<span class="fa-stack icon-huge">
		  <i class="fa fa-circle fa-stack-2x icon-blue"></i>
		  <i class="fa fa-rocket fa-stack-1x fa-inverse icon-white icon-blue-shadow"></i>
		</span>
		<h3>Get PowaPOS For Free</h3>
		<p>Join our Developer System Loaner Program to test drive PowaPOS, and you could earn a free T25, Scanner and Cash Drawer.</p>
		<p>{!! link_to('#', 'Find Out More') !!}</p>
	</div>

	@if(Auth::check())

		<div class="col-md-4 text-center content-icon">
			<span class="fa-stack icon-huge">
			  <i class="fa fa-circle fa-stack-2x icon-red"></i>
			  <i class="fa fa-home fa-stack-1x fa-inverse icon-white icon-red-shadow"></i>
			</span>
			<h3>Welcome Back</h3>
			<p>To change your account details or your password please navigate to your account page.</p>
			<p>{!! link_to('account', 'Edit my Account') !!}</p>
		</div>

	@else

		<div class="col-md-4 text-center content-icon">
			<span class="fa-stack icon-huge">
			  <i class="fa fa-circle fa-stack-2x icon-red"></i>
			  <i class="fa fa-user-plus fa-stack-1x fa-inverse icon-white icon-red-shadow"></i>
			</span>
			<h3>Get Started</h3>
			<p>Start developing straight away with our easy example apps and clear documentation. You can download SDKs and documentation in our "Downloads" area.</p>
			<p>{!! link_to('register', 'Sign Up') !!}</p>
		</div>
	
	@endif
	
	<div class="col-md-4 text-center content-icon">
		<span class="fa-stack icon-huge">
		  <i class="fa fa-circle fa-stack-2x icon-green"></i>
		  <i class="fa fa-heartbeat fa-stack-1x fa-inverse icon-white icon-green-shadow"></i>
		</span>
		<h3>Support</h3>
		<p>Visit the support site where you will be able to get community support from our extensive developer community or raise support tickets for direct help from our developers.</p>
		<p>{!! link_to('support', 'Support Site') !!}</p>
	</div>
</div>

@stop