<!doctype html>
<html lang="en" @yield('html_attributes')>
	<head>
	
		<meta charset="UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<title>@yield('title') | PowaPOS Partner Portal</title>
		
		{{-- Import styles --}}
		{!! HTML::style('css/style.css') !!}
		@yield('head_styles')
		
		{{-- Import scripts --}}
		<script src="{{ asset('js/codemirror/lib/codemirror.js') }}"></script>
		<script src="{{ asset('js/codemirror/mode/xml/xml.js') }}"></script>
		<script src="{{ asset('js/codemirror/mode/javascript/javascript.js') }}"></script>
		<script src="{{ asset('js/codemirror/mode/css/css.js') }}"></script>
		<script src="{{ asset('js/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
		<script src="{{ asset('js/codemirror/mode/clike/clike.js') }}"></script>
		@yield('head_scripts')
		
	</head>

	<body @yield('body_attributes')>
	
		@yield('page_content', 'No content')
		
		{{-- Import scripts --}}
		{!! HTML::script('js/jquery-1.11.2.min.js') !!}
		@yield('foot_scripts')
		
	</body>

</html>