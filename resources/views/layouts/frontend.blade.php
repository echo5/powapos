@extends('layouts.foundation')

@section('foot_scripts')
	{!! HTML::script('js/scripts.js') !!}
	{!! HTML::script('bootstrap/js/bootstrap.min.js') !!}
	{!! HTML::script('js/bootstrap-datepicker.js') !!}
	<script>
		jQuery(document).ready(function ($) {

		});
	</script>
	@if (App::environment('local'))
		<script type="text/javascript" src="http://localhost:48626/takana.js"></script>
		<script type="text/javascript">
		  takanaClient.run({
		    host: 'localhost:48626'
		  });
		</script>
	@endif
@stop

@section('page_content')

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
	  
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					{!! HTML::image('img/powapos_logo_white.png') !!}
				</a>
			</div>

	    	<!-- Collect the nav links, forms, and other content for toggling -->
	    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					
					@include('partials.menu.menu_item', [
						'url' 	=> '/', 
						'title'	=> 'Home',
					])
					
					@include('partials.menu.menu_item', [
						'url' 	=> 'develop', 
						'title'	=> 'Develop',
						'menu_items' => [
							[
								'url' => '/develop/t-series',
								'title' => 'T-Series'
							],
							[
								'url' => '/develop/powapin',
								'title' => 'PowaPIN'
							],
							[
								'url' => '/develop/guides-and-checklists',
								'title' => 'Guides & Checklists'
							]
						],
					])
					
					@include('partials.menu.menu_item', [
						'url' 	=> 'market', 
						'title'	=> 'Market',
						'menu_items' => [
							[
								'url' => '/market/logos-and-templates',
								'title' => 'Logos and Templates'
							],
							[
								'url' => '/market/photography-and-video',
								'title' => 'Photos and Video'
							],
							[
								'url' => '/market/collateral',
								'title' => 'Collateral'
							],
						],
					])
					
					@include('partials.menu.menu_item', [
						'url' 	=> 'engage', 
						'title'	=> 'Engage',
						'menu_items' => [
							[
								'url' => '/engage/partners',
								'title' => 'Partner Listings'
							],
						],
					])
					
					@include('partials.menu.menu_item', [
						'url' 	=> 'support', 
						'title'	=> 'Support',
						'menu_items' => [
							[
								'url' => '/support/faq',
								'title' => 'FAQs'
							],
							[
								'url' => '/support/proviews-videos',
								'title' => 'ProViews Videos'
							],
						],
					])
					
				</ul>
				<ul class="nav navbar-nav navbar-right navbar-account">
				
					@if(Auth::check())
					
						<li class="dropdown">
						
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> 
								<span class="fa fa-user" aria-hidden="true"></span>
								<span class="hidden-sm">{{ Auth::user()->getFullName() }}</span>
								<span class="caret"></span>
							</a>
							
							<ul class="dropdown-menu" role="menu">
								@if(Auth::user()->level() >= 10)
									<li>{!! link_to('admin', 'Administrator') !!}</li>
									<li class="divider"></li>
								@endif
								<li>{!! link_to('account', 'My Account') !!}</li>
								<li>{!! link_to('auth/logout', 'Logout') !!}</li>
							</ul>
							
						</li>
						
					@else
					
						<li class="hidden-sm">{!! link_to('auth/register', 'Register') !!}</li>
						<li>{!! link_to('auth/login', 'Login') !!}</li>
						
					@endif
					
				</ul>
			</div><!-- /.navbar-collapse -->
			
		</div><!-- /.container-fluid -->
	</nav>

	@yield('header')

	<div class="main">

		@yield('pre_main')
		
		@yield('main')
		
		@yield('post_main')

	</div>

	<footer>
		<div class="footer">
		
			<div class="container">
			
				<div class="row">
					<div class="col-sm-4 left-white">
						{!! HTML::image('img/logo.png') !!}
					</div>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-sm-6 col-xs-12 help-info">
								<p class="h4">Helpful information</p>
								<div class="row">
									<div class="col-xs-6">
										<ul>
											<li><a href="http://www.powa.com/about/">About Powa</a></li>
											<li><a href="http://blog.powa.com/" target="_blank">Blog</a></li>
											</ul></div><div class="col-xs-6"><ul><li><a href="http://www.powa.com/press/">Press</a></li>
											<li><a href="/contact/">Contact us</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12 social-info">
								<p class="h4">Follow us</p>
								<div class="row">
									<div class="col-xs-6">
										<ul>
											<li><a href="https://twitter.com/powatechltd" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
											<li><a href="https://www.facebook.com/PowaTechnologiesLtd" target="_blank"><i class="fa fa-facebook"></i>Facebook</a></li>
											<li><a href="https://www.youtube.com/user/getthepowa" target="_blank"><i class="fa fa-youtube"></i>Youtube</a></li>
											</ul></div><div class="col-xs-6"><ul><li><a href="https://www.linkedin.com/company/powa-technologies" target="_blank"><i class="fa fa-linkedin"></i>LinkedIn</a></li>
											<li><a href="https://plus.google.com/+PowaTechnologies/posts" target="_blank"><i class="fa fa-google-plus"></i>Google+</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-4 left-white small-letter">
						<span>©2015 Powa | <a href="/termsofuse/">Terms of use</a> | <a href="/privacy/">Privacy</a></span>
					</div>
				</div>

			</div>
		
		</div>
	</footer>

@stop