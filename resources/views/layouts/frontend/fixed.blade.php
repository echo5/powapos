@extends('layouts.frontend')

@section('main')

	<div class="container">
	  <div class="row">
	  	
	    <div class="col-xs-12">
	    
		    @if(Session::has('message'))
		    	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
		    @endif

			{{-- Main content for the section --}}
			@yield('content')
			
	    </div>
	    
	  </div>
	</div>
	
@stop