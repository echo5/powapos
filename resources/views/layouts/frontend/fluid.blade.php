@extends('layouts.frontend')

@section('main')
	
	<div class="container-fluid">
	  <div class="row">
	  
	    <div class="col-xs-12">
	    		    			
			{{-- Main content for the section --}}
			@yield('content')
			
	    </div>
	    
	  </div>
	</div>
	
@stop