@extends('layouts.minimal')

@section('main')

<div class="row main-form">
	<div class='col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-xs-10 col-xs-offset-1'>
		
		<p class="text-center logo">
			<a href="{{ url('/') }}">{!! HTML::image('img/powapos_logo_login.png') !!}</a>
		</p>
		
	    @if ($errors->has())
	    	<div class="alert alert-danger" role="alert">
	    		<strong>Fiddlesticks!</strong><br />
	    		<ul class="list-unstyled">
					@foreach ($errors->all() as $error)
					    <li>{{ $error }}</li>
					@endforeach
	    		</ul>
	    	</div>
	    @endif
	    
	    @if(Session::get('message'))
		    <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">{{ Session::get('message') }}</div>
	    @endif
	    
	    @yield('content')
	
	</div>
</div>

@stop