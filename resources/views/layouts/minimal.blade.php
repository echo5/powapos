@extends('layouts.foundation')

@section('head_styles')
	{!! HTML::style('bootstrap/css/bootstrap.css') !!}
	{!! HTML::style('css/minimal.css') !!}
@stop

@section('page_content')
	<div class="container">
		@yield('main')
	</div>
@stop
