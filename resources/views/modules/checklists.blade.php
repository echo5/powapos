<h2>Installation Checklists</h2>
<div class="checklists-table">
	@if (count($checklists))
		@foreach ($checklists as $checklist)
			<div class="checklist list-group checked-list-box">

				<div class="checklist-info">
					<div class="row">
						<div class="col-md-9">
							<h3 class="checklist-name">{{ $checklist->name }}</h3>
							<div class="checklist-description">{!! $checklist->description !!}</div>
						</div>
						<div class="col-md-3">
							<a class="btn btn-alt btn-download" href="{{ Config::get('assets.checklistsDirectory') . $checklist->pdf }}" target="_blank"><i class="fa fa-download"></i>Download as PDF</a>
						</div>
					</div>
				</div>
				
{{-- 				@if(count($checklist->items))
					@foreach ($checklist->items as $item)
						<div class="checklist-item list-group-item">
							<div class="col-md-3 checklist-item-name">{!! $item->name !!}</div>
							<div class="col-md-9">
								<div class="checklist-item-description">{!! $item->description !!}</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="alert alert-info">Sorry, there are no items in this checklist.</div>
				@endif
 --}}
			</div>
		@endforeach
	@else
		<div class="alert alert-info">Sorry, there are no checklists for this item.</div>
	@endif
</div>