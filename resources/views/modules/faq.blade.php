<div class="faq-search-container col-md-6">
	<h2 class="h3">FAQ Search</h2>
	<p>Begin typing to filter the FAQ results below.</p>
	<input type="text" class="form-input" id="faq-search">
	<a class="btn btn-default" id="faq-clear-filter">Clear</a>
</div>

<div class="clearfix"></div>

<div class="accordion faq-accordion" id="faq-accordion">
	@foreach ($faqCategories as $category)
		<h2 class="faq-category-name">{{ $category->name }}</h2>
		@if (count($category->faqs))
			@foreach ($category->faqs as $faq)
				<div class="accordion-section">
					<a href="#accordion-{{ $faq->id }}" class="accordion-section-title" class="h4">{{ $faq->question }}</a>
					<div id="accordion-{{ $faq->id }}" class="accordion-section-content">{!! $faq->answer !!}</div>
				</div>
			@endforeach
		@else
			<div class="alert alert-info">Sorry, there are no questions in this category.</div>
		@endif
	@endforeach
</div>