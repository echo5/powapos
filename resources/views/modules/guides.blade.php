<h2>Guides</h2>
<div class="guides-table row">
	@if (count($guides))
		@foreach ($guides as $guide)
		<div class="col-md-4">
			<div class="guide">
				<div class="guide-img">
					<a href="{{ Config::get('assets.guidesDirectory') . $guide->file }}" target="_blank">
						<img src="{!! Config::get('assets.guidesImageDirectory') . $guide->image !!}">
					</a>
				</div>
				<div class="guide-name h4">{!! $guide->name !!}</div>
				{{-- <div class="guide-description">{!! $guide->description !!}</div> --}}
			</div>
		</div>
		@endforeach
	@else
		<div class="alert alert-info">Sorry, there are no guides available.</div>
	@endif
</div>