<div class="row">
	@if (count($media))
		<?php $n = 1; ?>
		@foreach ($media as $item)
			<div class="{{ $col_span . ' ' . $class}}">
				<div class="media-item">
					<div class="media-item-img">
						<a href="{{ $item->getDownloadLink() }}">
							<img src="{{ Config::get('assets.mediaThumbsDirectory') . $image_size . '/' . $item->image }}">
						</a>
					</div> 
					<div class="media-item-name h4">{{ $item->name }}</div>
				</div>
			</div>
			<?php if ($n % $col == 0) { ?>
				<div class="clearfix"></div>
			<?php } ?>
			<?php $n++; ?>
		@endforeach
	@else
		<div class="alert alert-info">Sorry, there is no media in this category.</div>
	@endif
</div>