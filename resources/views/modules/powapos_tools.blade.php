<div class="row demoapps">
	<div class="col-md-4">
		@include('partials.filter', ['action' => URL::to('/powapos-tools/'), 'button' => 'Sort Demo Apps'] )
	</div>
	<div class="col-md-8">
		<div class="demoapps-table">
			<?php $count = 0; ?>
			@foreach ($products as $product)
				@foreach ($product->demoApps as $demoApp)
					<div class="demoapp">
						<div class="demoapp-header">
							<h3><a href="{{ $demoApp->link }}" target="_blank">{{ $demoApp->name }}</a></h3>
							@if ($demoApp->platform->slug)
								<div class="platform-icon"><i class="fa fa-{!! strtolower($demoApp->platform->slug) !!}"></i></div>			
							@endif
						</div>
						<div class="demoapp-description">{!! $demoApp->description !!}</div>
						<a class="btn btn-primary btn-download" href="{{ $demoApp->link }}" target="_blank"><i class="fa fa-download"></i>Download</a>
					</div>
					<?php $count ++; ?>
				@endforeach
			@endforeach
			@if (!$count)
				<div class="alert alert-info">Sorry, there are no PowaPOS tools matching your selection.</div>
			@endif
		</div>
	</div>
</div>