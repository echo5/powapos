<div class="row sdks">
	<div class="col-md-4">
		@include('partials.filter', ['action' => URL::to('/sdk-downloads/'), 'button' => 'Sort SDKs'] )
	</div>
	<div class="col-md-8">
		<div class="sdk-table">
			<?php $count = 0; ?>
			@foreach ($products as $product)
				@if (count($product->sdks))
					@foreach ($product->sdks as $sdk)
						<div class="sdk">
							<div class="sdk-header">
								<div class="sdk-label h5">{!! $sdk->label !!}</div>
								<h3><a href="{{ route('sdkdownloads', array('category' => $productCategory->slug, 'platform' => $sdk->platform['slug'], 'sdk' => $sdk->slug)) }}">{{ $sdk->name }}</a></h3>
								@if ($sdk->platform['slug'])
									<div class="platform-icon"><i class="fa fa-{!! strtolower($sdk->platform['slug']) !!}"></i></div>			
								@endif
							</div>
							<div class="sdk-description">{!! str_limit(strip_tags($sdk->description), 200, '...') !!}</div>
						</div>
						<?php $count ++; ?>
					@endforeach
				@else
					{{-- <div class="alert alert-info">Sorry, there are no SDKs available for this product.</div> --}}
				@endif
			@endforeach
			@if (!$count)
				<div class="alert alert-info">Sorry, there are no SDKs matching your selection.</div>
			@endif
		</div>
	</div>
</div>