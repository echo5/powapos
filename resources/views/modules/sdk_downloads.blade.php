<div class="row">
	<div class="sdk-downloads-table">
		@if (count($downloads))
			<?php $n = 1; ?>
			@foreach ($downloads as $download)
				<div class="col-md-4">
					<div class="sdk-download">
						<h3><a href="{{ route('download', array('folder' => 'sdks', 'filename' => $download->file)) }}">{!! $download->name !!}</a></h3>
						<div class="sdk-download-description">{!! $download->description !!}</div>
						<a class="btn btn-primary btn-download" href="{{ route('download', array('folder' => 'sdks', 'filename' => $download->file)) }}" target="_blank"><i class="fa fa-download"></i>Download</a>
					</div>
				</div>
				<?php if ($n % 3 == 0) { ?>
					<div class="clearfix"></div>
				<?php } ?>
				<?php $n++; ?>
			@endforeach
		@else
			<div class="alert alert-info">Sorry, there are no downloads available.</div>
		@endif
	</div>
	<div class="col-md-12">
		<div class="sdk-description">
			{!! $sdk->description !!}
		</div>
	</div>
</div>
