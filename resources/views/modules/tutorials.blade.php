<div class="row tutorials">
	<div class="col-md-4">
		@include('partials.filter', ['action' => URL::to('/tutorials/'), 'button' => 'Sort Tutorials'] )
	</div>
	<div class="col-md-8">
		<div class="tutorials-table">
			<?php $count = 0; ?>
			@foreach ($products as $product)
				@foreach ($product->tutorials as $tutorial)
					<div class="tutorial">
						<h3><a href="{{ route('tutorial', array('category' => $productCategory->slug, 'platform' => $tutorial->platform->slug, 'tutorial' => $tutorial->slug)) }}">{{ $tutorial->name }}</a></h3>
						<div>{!! str_limit(strip_tags($tutorial->content), 200, '...') !!}</div>
						@if ($tutorial->platform->slug)
							<div class="platform-icon"><i class="fa fa-{!! strtolower($tutorial->platform->slug) !!}"></i></div>
						@endif
					</div>
					<?php $count ++; ?>
				@endforeach
			@endforeach
			@if (!$count)
				<div class="alert alert-info">Sorry, there are no tutorials matching your selection.</div>
			@endif
		</div>
	</div>
</div>