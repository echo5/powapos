<div class="sort-box">
	<h4>Select product</h4>
	<form id="sort-form" name="sort-form" action="{{ $action }}">
		<select name="product" id="sort-product">] 
			@foreach ($productCategories as $category)
				<option value="{{ $category->slug }}" @if ($currentCategory == $category->slug) selected @endif>{{ $category->name }}</option>
			@endforeach
		</select>
		<h4>Select your platform</h4>
		<select name="platform" id="sort-platform">
			<option value="all">All Platforms</option>
			@foreach ($platforms as $platform)
				<option value="{{ $platform->slug }}" @if ($currentPlatform['slug'] == $platform->slug) selected @endif>{{ $platform->name }}</option>
			@endforeach
		</select>
		<div class="clear"></div>
		<input type="submit" href="#" class="btn btn-alt" value="{{ $button }}">
	</form>
</div>