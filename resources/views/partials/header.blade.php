<div class="main-header {{ $class or '' }}">
	<div class="container">
		<div class="row">
		  	<div class="col-xs-12">
		  		@if(isset($breadcrumbs))
		  			<div class="breadcrumbs">
			  			@foreach($breadcrumbs as $crumb => $url)
			  				{{ link_to($url, $crumb." &raquo; ") }}
			  			@endforeach
		  			</div>
		  		@endif
		  		<h1>{{ $title }} <small>{{ $sub_title or '' }}</small></h1>
		  	</div>
		</div>
	</div>
</div>