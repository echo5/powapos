<li class="@if(\Request::is($url)) active @endif @if(isset($menu_items)) dropdown @endif" >
	<a href="{{ url($url) }}" @if(isset($menu_items)) class="dropdown-toggle" @endif>
		{{ $title }}
		@if(\Request::is($url))
			<span class="sr-only">(current)</span>
		@endif
	</a>
	@if(isset($menu_items))
		<ul class="dropdown-menu">
			@foreach($menu_items as $menu_item)
				<li>
					<a href="{{ url($menu_item['url']) }}">
						{{ $menu_item['title'] }}
						@if(\Request::is($menu_item['url']))
							<span class="sr-only">(current)</span>
						@endif
					</a>
				</li>
			@endforeach
		</ul>
	@endif
</li>