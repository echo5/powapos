@if (isset($menu_items))
	<ul class="nav nav-pills nav-stacked">
		@foreach ($menu_items as $menu_item) 
			<li role="presentation">
				<a href="{{ $menu_item['url'] }}">
					@if (isset($menu_item['icon']))
						<i class="fa fa-{{ $menu_item['icon'] }}"></i>
					@endif
					{{ $menu_item['title'] }}
				</a>
			</li>
		@endforeach
	</ul>
@else
	This page doesn't have any menu items.
@endif