<p>
	<span class="fa-stack fa-sm">
	  <i class="fa fa-circle fa-stack-2x icon-{{ $color or 'blue' }}"></i>
	  <i class="fa fa-{{ $icon or 'circle' }} fa-stack-1x fa-inverse"></i>
	</span>
	
	<a href="{{ $url or '#' }}">{{ $title or '' }}</a>
</p>